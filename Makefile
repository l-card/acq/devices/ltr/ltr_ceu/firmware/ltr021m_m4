PROJECT_NAME := ltr021m
TARGET_CPU := lpc43xx

BUILD_GROUPS = M4 M4_EXTRA USB_DESCR M0



CHIP_DIR := ./lib/lpc_chip
include $(CHIP_DIR)/chip.mk
include ./lib/lusb/lusb.mk
include ./lib/lip/lip.mk
include ./lib/ltimer/ltimer.mk
include ./lib/lprintf/lprintf.mk

USE_BOOTROM ?= 1

LDSCRIPT_TEMPLATE := ./lpc4333.ld.S

DEFS            := NDEBUG
DEFS_M4         := STACK_SIZE=4096 $(CHIP_DEFS)
DEFS_M4_EXTRA   := $(DEFS_M4)
DEFS_USB_DESCR  := $(DEFS_M4)
DEFS_M0         := STACK_SIZE=1024 $(CHIP_DEFS_M0)

SRC_M4 := \
        $(CHIP_SRC) \
        $(CHIP_STARTUP_SRC) \
        $(LUSB_SRC) \
        $(LIP_SRC) \
        $(LTIMER_SRC) \
        ${LPRINTF_SRC}

SRC_USB_DESCR := $(LUSB_SRC_DESCR)

SRC_M4_EXTRA := \
       ./src/ledblink.c \
       ./src/conf.c \
       ./src/fwinfo.c \
       ./src/rtc.c \
       ./lib/crc/fast_crc.c \
       ./src/memtest.c \
       ./src/sdram_test.c \
       ./src/usb.c \
       ./src/usb_bulk.c \
       ./src/ctlfunc.c \
       ./src/cpld.c \
       ./src/aux_io.c \
       ./src/fpga_reg.c \
       ./src/data.c \
       ./src/dma.c \
       ./src/hostio.c \
       ./src/tcpserver_cmd.c \
       ./src/tcpserver_data.c \
       ./src/tcpserver.c \
       ./src/boot.c \
       ./src/main.c

SRC_M0 := \
    $(CHIP_STARTUP_SRC_M0) \
    ./src/m0_task.c

# Extra warnings
WARNOPTS_EXTRA := -Wframe-larger-than=400 -Wpadded -Wstrict-overflow=5 -Wcast-align -Waggregate-return

CFLAGS = $(OPT) -std=gnu99 -fomit-frame-pointer -fverbose-asm
CFLAGS_M4 = $(CHIP_CFLAGS)
CFLAGS_M4_EXTRA = $(CHIP_CFLAGS) $(WARNOPTS_EXTRA)
CFLAGS_M0 = $(CHIP_CFLAGS_M0)
CFLAGS_USB_DESCR = $(CFLAGS_M4) $(LUSB_SRC_DESCR_CPFLAGS)


LDFLAGS  = $(CHIP_LDFLAGS) -nostartfiles -Wl,--no-warn-mismatch,--section-start=.text=$(FLASH_START)

SYS_INC_DIRS := $(CHIP_INC_DIRS) \
        ./lib/lcspec/gcc \
        $(LPRINTF_INC_DIRS) \
        $(LUSB_INC_DIRS) \
        $(LIP_INC_DIRS) \
        $(LTIMER_INC_DIRS)

INC_DIRS := ./src ./lib/crc


# WARNING:
# Somehow it seems that the placement of functions in flash can affect performance
# when testing 500k samples/s in + 500k samples/s out (LTR34).
# UM10503.pdf says that Flash Accelerator has 32-byte aligned buffers; it also recommends
# putting .rodata and .text in different flash banks.
# For now, let's see if -falign-functions=32 does the trick.

# Define optimisation level here
OPT := -O2 -falign-functions=32

##############################################################################################

ifeq ($(USE_BOOTROM), 1)
    FLASH_START = 0x1A010100
else
    FLASH_START = 0x1A000000
    PROJECT_NAME := $(addsuffix -noboot, $(PROJECT_NAME))
endif

##############################################################################################



include ./lib/gnumake_prj/gcc_prj.mk

$(PROJECT_TARGET): $(PROJECT_BIN) $(PROJECT_HEX)

# *** EOF ***
