@echo off
if -%1 == - goto help

set HEX=build\ltr021m-noboot.hex
set COMPORT=%1

set FMPATH=C:\Program Files\Flash Magic
set OSCFREQ=12
set FMSCRIPT=fm.tmp

if not exist %HEX% goto nofile

echo COM(%COMPORT%, 38400) > %FMSCRIPT%
echo DEVICE(LPC4333, %OSCFREQ%, 0) >> %FMSCRIPT%
echo HARDWARE(BOOTEXEC, 50, 100) >> %FMSCRIPT%
echo HIGHSPEED(0, 115200) >> %FMSCRIPT%
echo ERASE(DEVICE, PROTECTISP) >> %FMSCRIPT%
echo HEXFILE(%HEX%, NOCHECKSUMS, NOFILL, PROTECTISP) >> %FMSCRIPT%
echo ACTIVATEFLASHBANK(0) >> %FMSCRIPT%
echo VERIFY(%HEX%, NOCHECKSUMS) >> %FMSCRIPT%

"%FMPATH%\fm" @%FMSCRIPT%

del %FMSCRIPT%

goto end

:help
echo Usage: %~n0 comport#
goto end
:nofile
echo File %HEX% not found
goto end
:fail
echo Operation failed, cannot continue
:end
