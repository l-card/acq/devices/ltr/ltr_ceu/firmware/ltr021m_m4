@echo off
rem �������� ������ LTR021M �� ���������� ETHERNET

rem ======== ��������� ========
rem �ᯮ��㥬� �ணࠬ�� � 䠩�� ����஥�
rem (����� ��室�����: � PATH, � ⥪�饬 ��⠫���, � ��⠫��� � .bat 䠩���)
set "LTRCFG=ltr021m-config.exe"
set "UNIXPING=unixping.exe"

rem IP ����, �ᯮ��㥬� �� ����ன��
set IPADDR=192.168.0.248
set IPPREFIX=24
rem ===========================

rem � PATH �६���� ���������� ⥪�騩 ��⠫�� � ��⠫��, �� ���ண� ����饭 .bat 䠩�
set "PATH=.;%PATH%;%~dp0"

rem �������� ������� ��������� ������
set MISSING_TOOLS=
for %%a in ("%LTRCFG%") do set FOUND_ON_PATH=%%~$PATH:a
if not defined FOUND_ON_PATH (
    echo �� ������� �ணࠬ�� "%LTRCFG%"
    set MISSING_TOOLS=1
)
for %%a in ("%UNIXPING%") do set FOUND_ON_PATH=%%~$PATH:a
if not defined FOUND_ON_PATH (
    echo �� ������� �ணࠬ�� "%UNIXPING%"
    set MISSING_TOOLS=1
)
if defined MISSING_TOOLS (
    echo.
    echo ��� ����� �������騥 �ணࠬ��, ����ᠭ� � 䠩�� INSTALL.
    echo �ணࠬ��, ����� ������ � ᮮ�饭�� ��� ���� ��� ��⠫���, ������ ����
    echo ����㯭� �१ PATH ��� ��室����� � ⥪�饬 ��⠫���.
    echo.
    goto quit
)

rem ---- �஢�ઠ ���䫨�� IP ���ᮢ
ping -n 1 %IPADDR% | find "TTL" > nul
if not errorlevel 1 (
    echo ������! � �� 㦥 ���� ��� � ���ᮬ %IPADDR%
    goto quit
)

rem ---- ����ன�� �� ࠡ��� �� ��
"%LTRCFG%" -i usb --set-iface=net --set-dhcp=0 --set-ip=%IPADDR%/%IPPREFIX% --reboot

if errorlevel 1 (
    echo ������! ���� �� �믮����.
    goto quit
)

echo ��������, �� ��������� Ethernet ��ᢥ⨫���.
pause

rem ---- �஢�ઠ �裡
:do_ping
"%UNIXPING%" -c 1000 -i 0 -s 1460 -q %IPADDR%
echo.
set TRYAGAIN=
set/p TRYAGAIN="������� ���? [y/N]"
if /i "%TRYAGAIN%"=="y" goto do_ping

rem ---- ����ன�� ���⭮ �� USB � ��� ��ࠬ��஢ ��
"%LTRCFG%" -i usb --set-iface=usb --set-dhcp=1 --set-ip=0.0.0.0/0 --reboot

if errorlevel 1 (
    echo ������! ���� �� �믮����.
    goto quit
)

goto :eof

:quit
pause
