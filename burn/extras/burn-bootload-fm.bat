@echo off
rem ======== ��������� ========
rem �ᯮ��㥬� �ணࠬ�� � 䠩�� ����஥�
set "FMPROG=C:\Program Files\Flash Magic\fm.exe"
set "FMPROG_ENV=C:\Program Files\Flash Magic\fm.env"

rem ����� ��訢�� (��᮫��� ���)
set "BOOTROM_HEX=Y:\EXCHANGE\Tech_Doc\Soft\bin\LTR\ltr021m\lboot_ltr021m.hex"
rem ===========================

rem ��稥 ��ࠬ����
set FMOSCFREQ=12
set "FMSCRIPT=%TEMP%\ltr021m-fm.tmp"
rem ===========================

rem �������� ������� ��������� ������
if not exist "%FMPROG%" (
    echo �� ��⠭������ �ணࠬ�� Flash Magic ^(%FMPROG%^)
    goto quit
)

if not exist "%BOOTROM_HEX%" (
    echo �� ������ 䠩� "%BOOTROM_HEX%"
    goto quit
)

rem ������ ����������
rem ��ࠬ���� ����� ���� ������:
rem 1) � 䠩��� .env �ଠ� NAME=VALUE
rem 2) � ��������� ��ப� .bat 䠩�� (NAME=VALUE)
rem 3) � ��६����� ���㦥���

if exist "%FMPROG_ENV%" (
    for /f "usebackq eol=; tokens=1,* delims== " %%a in ("%FMPROG_ENV%") do set %%a=%%b
)

:cmdline
if not -%2==- (
    set %1=%2
    shift
    shift
    goto cmdline
)

if defined LPC_ISP_COMPORT (
    set "COMPORT=%LPC_ISP_COMPORT%"
) else (
    set /p "COMPORT=������ ����� COM ����, � ���஬� ������祭 �ணࠬ����: "
    if not defined COMPORT goto :eof
)

for %%a in ("%BOOTROM_HEX%") do pushd "%%~dpa"
echo COM(%COMPORT%, 38400) > "%FMSCRIPT%"
echo DEVICE(LPC4333, %FMOSCFREQ%, 0) >> "%FMSCRIPT%"
echo HARDWARE(BOOTEXEC, 50, 100) >> "%FMSCRIPT%"
echo HIGHSPEED(0, 115200) >> "%FMSCRIPT%"
echo ERASE(DEVICE, PROTECTISP) >> "%FMSCRIPT%"
for %%a in ("%BOOTROM_HEX%") do echo HEXFILE(%%~nxa, NOCHECKSUMS, NOFILL, PROTECTISP) >> "%FMSCRIPT%"
echo ACTIVATEFLASHBANK(0) >> "%FMSCRIPT%"
for %%a in ("%BOOTROM_HEX%") do echo VERIFY(%%~nxa, NOCHECKSUMS) >> "%FMSCRIPT%"

"%FMPROG%" @"%FMSCRIPT%"
del "%FMSCRIPT%"
popd

:quit
pause
