@echo off
rem ========  ========
rem    OPENOCD   
set "OCDPATH=c:\openocd-0.7.0"
set "OCDPROG=%OCDPATH%\bin\openocd.exe"
set OCDPORT=14444
set "OCDCFG=%~dp0lpc43xx_flash.cfg"
set OCDIFACE=olimex-arm-usb-ocd
rem set OCDIFACE=jlink

rem  ŠŤë ŻŕŽč¨˘ŽŞ (ĄáŽŤîâ­ëĽ Żăâ¨)
set "BOOTROM_HEX=Y:\EXCHANGE\Tech_Doc\Soft\bin\LTR\ltr021m\lboot_ltr021m.hex"
rem ===========================

if not exist "%OCDPROG%" (
    echo Ľ ­ Š¤Ľ­  ŻŕŽŁŕ ŹŹ  "%OCDPROG%"
    goto quit
)

if not exist "%BOOTROM_HEX%" (
    echo Ľ ­ Š¤Ľ­ ä ŠŤ "%BOOTROM_HEX%"
    goto quit
)

"%OCDPROG%" -s "%OCDPATH%" -f interface/%OCDIFACE%.cfg -f "%OCDCFG%" ^
 -c "init" ^
 -c "reset init" ^
 -c "sleep 500" ^
 -c "halt" ^
 -c "wait_halt 100" ^
 -c "flash banks" ^
 -c "flash write_image erase {%BOOTROM_HEX%} 0 ihex" ^
 -c "core_reset" ^
 -c "sleep 10" ^
 -c "shutdown"

:quit
pause
