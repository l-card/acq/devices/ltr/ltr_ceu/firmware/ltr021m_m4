@echo off
rem ======== ��������� ========
rem �ᯮ��㥬� �ணࠬ�� � 䠩�� ����஥�
rem (����� ��室�����: � PATH, � ⥪�饬 ��⠫���, � ��⠫��� � .bat 䠩���)
set "LBOOT=lboot.exe"
set "LTRCFG=ltr021m-config.exe"

rem ����� ��訢�� (��᮫��� ���)
set "BINFILE=Y:\EXCHANGE\Tech_Doc\Soft\bin\LTR\ltr021m\ltr021m.bin"
rem ===========================

rem � PATH �६���� ���������� ⥪�騩 ��⠫�� � ��⠫��, �� ���ண� ����饭 .bat 䠩�
set "PATH=.;%PATH%;%~dp0"

rem ��������� ������
if -%1 == - (
    echo �ய�饭 ����室��� ��ࠬ���: ���� ��� LTR021M
    goto quit
)
set LTRHOST=%1

rem �������� ������� ��������� ������
set MISSING_TOOLS=
for %%a in ("%LBOOT%") do set FOUND_ON_PATH=%%~$PATH:a
if not defined FOUND_ON_PATH (
    echo �� ������� �ணࠬ�� "%LBOOT%"
    set MISSING_TOOLS=1
)
for %%a in ("%LTRCFG%") do set FOUND_ON_PATH=%%~$PATH:a
if not defined FOUND_ON_PATH (
    echo �� ������� �ணࠬ�� "%LTRCFG%"
    set MISSING_TOOLS=1
)
if defined MISSING_TOOLS (
    echo.
    echo ��� ����� �������騥 �ணࠬ��, ����ᠭ� � 䠩�� INSTALL.
    echo �ணࠬ��, ����� ������ � ᮮ�饭�� ��� ���� ��� ��⠫���, ������ ����
    echo ����㯭� �१ PATH ��� ��室����� � ⥪�饬 ��⠫���.
    echo.
    goto quit
)

if not exist "%BINFILE%" (
    echo �� ������ 䠩� "%BINFILE%"
    goto quit
)

"%LTRCFG%" -i net --bootload %LTRHOST%
if not errorlevel 1 (
    rem Delay
    ping -n 3 127.0.0.1 > nul
)

"%LBOOT%" tftp -v -r --recovery --hash --tftp-ip-addr=%LTRHOST% "%BINFILE%"

:quit
pause
