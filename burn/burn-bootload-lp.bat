@echo off
rem ======== ��������� ========
rem �ᯮ��㥬� �ணࠬ�� � 䠩�� ����஥�
rem (����� ��室�����: � PATH, � ⥪�饬 ��⠫���, � ��⠫��� � .bat 䠩���)
set "LPCPROG=lpcprog.exe"
set "LPCPROG_ENV=lpcprog.env"

rem ����� ��訢�� (��᮫��� ���)
set "BOOTROM_HEX=Y:\EXCHANGE\Tech_Doc\Soft\bin\LTR\ltr021m\lboot_ltr021m.hex"
rem ===========================

rem � PATH �६���� ���������� ⥪�騩 ��⠫�� � ��⠫��, �� ���ண� ����饭 .bat 䠩�
set "PATH=.;%PATH%;%~dp0"

rem �������� ������� ��������� ������
set MISSING_TOOLS=
for %%a in ("%LPCPROG%") do set FOUND_ON_PATH=%%~$PATH:a
if not defined FOUND_ON_PATH (
    echo �� ������� �ணࠬ�� "%LPCPROG%"
    set MISSING_TOOLS=1
)
if defined MISSING_TOOLS (
    echo.
    echo ��� ����� �������騥 �ணࠬ��, ����ᠭ� � 䠩�� INSTALL.
    echo �ணࠬ��, ����� ������ � ᮮ�饭�� ��� ���� ��� ��⠫���, ������ ����
    echo ����㯭� �१ PATH ��� ��室����� � ⥪�饬 ��⠫���.
    echo.
    goto quit
)

if not exist "%BOOTROM_HEX%" (
    echo �� ������ 䠩� "%BOOTROM_HEX%"
    goto quit
)

rem ������ ����������
rem ��ࠬ���� ����� ���� ������:
rem 1) � 䠩��� .env �ଠ� NAME=VALUE
rem 2) � ��������� ��ப� .bat 䠩�� (NAME=VALUE)
rem 3) � ��६����� ���㦥���

for %%a in ("%LPCPROG_ENV%") do set FOUND_ON_PATH=%%~$PATH:a
if defined FOUND_ON_PATH (
    for /f "usebackq eol=; tokens=1,* delims== " %%a in ("%FOUND_ON_PATH%") do set %%a=%%b
)

:cmdline
if not -%2==- (
    set %1=%2
    shift
    shift
    goto cmdline
)

if defined LPC_ISP_COMPORT (
    set "COMPORT=%LPC_ISP_COMPORT%"
) else (
    set /p "COMPORT=������ ����� COM ����, � ���஬� ������祭 �ணࠬ����: "
    if not defined COMPORT goto :eof
)

"%LPCPROG%" -wipe -control -try3 -hex "%BOOTROM_HEX%" \\.\com%COMPORT% 38400 12000
echo.
if errorlevel 1 (
    echo ������!
) else (
    echo ��������� �������.
)

:quit
pause
