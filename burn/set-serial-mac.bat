@echo off
rem ������ 䠡�筮� ���䨣��樨 (serial, MAC, ��஫�) � LTR021.
rem ������祭�� �� USB. ����᪠���� ��� ltrserver/ltrd.

rem ======== ��������� ========
rem �ᯮ��㥬� �ணࠬ�� � 䠩�� ����஥�
rem (����� ��室�����: � PATH, � ⥪�饬 ��⠫���, � ��⠫��� � .bat 䠩���)
set "LTRCFG=ltr021m-config.exe"
set "GETMAC=db-serial-mac.exe"

set PASSWD=ltr021m_2015

rem �६���� 䠩�
set "TMPFILE=%TEMP%\%~n0.tmp"
rem ===========================

rem � PATH �६���� ���������� ⥪�騩 ��⠫�� � ��⠫��, �� ���ண� ����饭 .bat 䠩�
set "PATH=.;%PATH%;%~dp0"

set LTR_SET_MAC=
if "%1" EQU "" (
    echo �訡��! �� ����� ⨯ �३� �室�� ��ࠬ��஬!
    goto quit  
) else if "%1" EQU "LTR-CEU" (
    echo �३� LTR-CEU. ������ �਩���� ����� � MAC-����.
    set LTR_SET_MAC=1
) else if "%1" EQU "LTR-CU" (
    echo �३� LTR-EU. ������ ⮫쪮 �਩���� �����.
) else (
    echo �訡��! ����� �������ন����� ⨯ �३�!
    goto quit
)



rem �������� ������� ��������� ������
set MISSING_TOOLS=
for %%a in ("%LTRCFG%") do set FOUND_ON_PATH=%%~$PATH:a
if not defined FOUND_ON_PATH (
    echo �� ������� �ணࠬ�� "%LTRCFG%"
    set MISSING_TOOLS=1
)



if defined LTR_SET_MAC (
    for %%a in ("%GETMAC%") do set FOUND_ON_PATH=%%~$PATH:a
    if not defined FOUND_ON_PATH (
        echo �� ������� �ணࠬ�� "%GETMAC%"
        set MISSING_TOOLS=1
    )
)

if defined MISSING_TOOLS (
    echo.
    echo ��� ����� �������騥 �ணࠬ��, ����ᠭ� � 䠩�� INSTALL.
    echo �ணࠬ��, ����� ������ � ᮮ�饭�� ��� ���� ��� ��⠫���, ������ ����
    echo ����㯭� �१ PATH ��� ��室����� � ⥪�饬 ��⠫���.
    echo.
    goto quit
)

set/p SERIAL="������ �਩�� ����� �३�: "
if not defined SERIAL goto :eof

if defined LTR_SET_MAC (
    "%GETMAC%" --serial="%SERIAL%" --add > %TMPFILE%
    if errorlevel 1 (
        echo ���������� �������� MAC �����!
        goto quit
    )

    for /f "usebackq" %%a in ("%TMPFILE%") do set MACADDR=%%a
    del "%TMPFILE%" > nul
)

if defined LTR_SET_MAC (    
    "%LTRCFG%" -i usb --set-serial="%SERIAL%" --set-fmac="%MACADDR%" --set-fpasswd="%PASSWD%" --reboot
) else (
    "%LTRCFG%" -i usb --set-serial="%SERIAL%" --set-fpasswd="%PASSWD%" --reboot
)


echo.
if errorlevel 1 (
    echo ������ �਩���� �믮����� � �訡���. 
    echo �������� 㦥 �뫠 ������ � �ॡ���� ��஫�
    echo ����ୠ� ����⪠ � ��஫��
    echo.    
    if defined LTR_SET_MAC (
        "%LTRCFG%" -i usb --set-serial="%SERIAL%" --set-fmac="%MACADDR%" --fpasswd="%PASSWD%" --reboot
    ) else (
        "%LTRCFG%" -i usb --set-serial="%SERIAL%" --fpasswd="%PASSWD%" --reboot
    )
    echo.
)

if errorlevel 1 (
    echo ������! 
) else (
    echo ������������ ��������.
)

:quit
pause
