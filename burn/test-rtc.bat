@echo off
rem �������� ������ ����� ��������� ������� LTR021M

rem ======== ��������� ========
rem �ᯮ��㥬� �ணࠬ�� � 䠩�� ����஥�
rem (����� ��室�����: � PATH, � ⥪�饬 ��⠫���, � ��⠫��� � .bat 䠩���)
set "LTRRTC=ltr021m-rtcutil.exe"

rem ===========================

rem � PATH �६���� ���������� ⥪�騩 ��⠫�� � ��⠫��, �� ���ண� ����饭 .bat 䠩�
set "PATH=.;%PATH%;%~dp0"

rem �������� ������� ��������� ������
set MISSING_TOOLS=
for %%a in ("%LTRRTC%") do set FOUND_ON_PATH=%%~$PATH:a
if not defined FOUND_ON_PATH (
    echo �� ������� �ணࠬ�� "%LTRRTC%"
    set MISSING_TOOLS=1
)
if defined MISSING_TOOLS (
    echo.
    echo ��� ����� �������騥 �ணࠬ��, ����ᠭ� � 䠩�� INSTALL.
    echo �ணࠬ��, ����� ������ � ᮮ�饭�� ��� ���� ��� ��⠫���, ������ ����
    echo ����㯭� �१ PATH ��� ��室����� � ⥪�饬 ��⠫���.
    echo.
    goto quit
)

rem ---- ��⠭���� �ᮢ
echo.
"%LTRRTC%" -i usb -s
echo.

if errorlevel 1 (
    echo ������! �� 㤠���� ��⠭����� ���.
    goto quit
)

echo ���� ��⠭������. �⪫��� ��⠭�� �३�-����஫���.
pause
ping -n 4 127.0.0.1 > nul
echo.
echo ������ ��⠭�� �३�-����஫���.
pause
ping -n 4 127.0.0.1 > nul

rem ---- �஢�ઠ �ᮢ
echo.
"%LTRRTC%" -i usb -c --max-error=1000
echo.

if errorlevel 1 (
    echo ������! ���� �� �������.
    goto quit
) else (
    echo ���� �������.
)

:quit
pause
