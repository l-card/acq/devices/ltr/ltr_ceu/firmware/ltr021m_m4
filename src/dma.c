#include "dma.h"

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void dma_init(void)
    {
    LPC_GPDMA->CONFIG = GPDMA_DMACConfig_E; /* enable */
    /* clear interrupts */
    LPC_GPDMA->INTTCCLEAR = 0xFF;
    LPC_GPDMA->INTERRCLR = 0xFF;

    /* clear all channels */
    for (int i = 0; i < 8; i++)
        {
        LPC_GPDMA->CH[i].CONFIG = 0;
        LPC_GPDMA->CH[i].LLI = 0;
        LPC_GPDMA->CH[i].CONTROL = 0;
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void dma_chan_setup(GPDMA_CH_T* pchan, const dma_lli_t* lli, uint32_t config)
    {
    pchan->CONFIG = config & ~GPDMA_DMACCxConfig_E;
    while (pchan->CONFIG & GPDMA_DMACCxConfig_E);
    pchan->SRCADDR = (uint32_t)lli->src_addr;
    pchan->DESTADDR = (uint32_t)lli->dest_addr;
    pchan->LLI = (uint32_t)lli->next;
    pchan->CONTROL = (uint32_t)lli->ctl_word;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
