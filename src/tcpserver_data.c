/*================================================================================================*
 * Данные по протоколу TCP/IP (lip sockets)
 *================================================================================================*/

#include "tcpserver.h"
#include "lip.h"
#include <string.h>
#include "ctlfunc.h"
#include "ledblink.h"
#include "conf.h"

/*================================================================================================*/
#define TX_BUF_SIZE             TCPSERVER_DATA_TX_BUF_SIZE
#define RX_BUF_SIZE             TCPSERVER_DATA_RX_BUF_SIZE
/*================================================================================================*/

/*================================================================================================*/
static t_lsock_id f_sock;
static int f_last_sock_state;

static TCPSERVER_DATA_BUF_DECL uint8_t f_rx_buf[RX_BUF_SIZE];
static TCPSERVER_DATA_BUF_DECL uint8_t f_tx_buf[TX_BUF_SIZE];
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static void f_sockstate_monitor(void)
    {
    int st = lsock_state(f_sock);
    if (st != f_last_sock_state)
        {
        f_last_sock_state = st;
        switch (st)
            {
            case LIP_TCP_SOCK_STATE_CLOSE_WAIT:
                lsock_shutdown(f_sock);
                break;
            case LIP_TCP_SOCK_STATE_CLOSED:
                /* data connection closed */
                break;
            case LIP_TCP_SOCK_STATE_ESTABLISHED:
                /* data connection open */
                ctlfunc_reset_fpga();
                break;
            }
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static int f_same_ip(const t_lip_ip_addr* a, const t_lip_ip_addr* b)
    {
    return (a->addr_len == b->addr_len) && (a->ip_type == b->ip_type)
        && !memcmp(a->addr, b->addr, a->addr_len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_init(void) {
    /* LIP must have been already initialized */
    
    /* init socket */
    f_sock = lsock_create();
    lsock_set_recv_fifo(f_sock, f_rx_buf, sizeof(f_rx_buf));
    lsock_set_send_fifo(f_sock, f_tx_buf, sizeof(f_tx_buf));
    if (g_conf_user.net_options & CONF_NETOPT_NODELAY_DTA) {
        int nodelay = 1;
        lsock_set_opt(f_sock, IPPROTO_TCP, TCP_NODELAY, &nodelay, sizeof(nodelay));
    }
    f_last_sock_state = lsock_state(f_sock);
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int tcpserver_data_sock_connected_to
    (
    t_lsock_id cmd_sock             /* сокет управляющего соединения, чей remote_ip сравнивается */
    )
    {
    t_lip_ip_addr remote_ip1, remote_ip2;
    if (lsock_state(f_sock) != LIP_TCP_SOCK_STATE_ESTABLISHED)
        return 0;
    return
        (LIP_ERR_SUCCESS == lsock_get_remote_ip(f_sock, &remote_ip1))
        && (LIP_ERR_SUCCESS == lsock_get_remote_ip(cmd_sock, &remote_ip2))
        && f_same_ip(&remote_ip1, &remote_ip2);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_sock_listen
    (
    t_lsock_id cmd_sock             /* сокет управляющего соединения, чей remote_ip слушать */
    )
    {
    t_lip_ip_addr remote_ip;
    if (lsock_state(f_sock) != LIP_TCP_SOCK_STATE_CLOSED)
        lsock_abort(f_sock);
    lsock_set_remote_ip(f_sock,
        (LIP_ERR_SUCCESS == lsock_get_remote_ip(cmd_sock, &remote_ip)) ? &remote_ip : NULL);
    lsock_listen(f_sock, TCPSERVER_DATA_PORT);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_hangup(void)
    {
    lsock_close(f_sock, TCPSERVER_HANGUP_TIMEOUT_MS);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_hangup_on_host
    (
    t_lsock_id cmd_sock             /* сокет управляющего соединения, чей remote_ip отключить */
    )
    {
    t_lip_ip_addr remote_ip1, remote_ip2;
    if (lsock_state(f_sock) == LIP_TCP_SOCK_STATE_CLOSED)
        return;
    if ((LIP_ERR_SUCCESS == lsock_get_remote_ip(f_sock, &remote_ip1))
        && (LIP_ERR_SUCCESS == lsock_get_remote_ip(cmd_sock, &remote_ip2))
        && f_same_ip(&remote_ip1, &remote_ip2))
        {
        lsock_close(f_sock, TCPSERVER_HANGUP_TIMEOUT_MS);
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_reset(void)
    {
    /* Called by ctlfunc_reset_fpga, probably nothing to do here */
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t tcpserver_data_send_avail(void) {
    int nbytes = 0;
    if (f_last_sock_state == LIP_TCP_SOCK_STATE_ESTABLISHED) {
        nbytes = lsock_send_rdy_size(f_sock);
        if (nbytes < 0)
            nbytes = 0;
#ifdef TCPSERVER_DATA_MAX_TX_BLOCK
        if (nbytes > TCPSERVER_DATA_MAX_TX_BLOCK)
            nbytes = TCPSERVER_DATA_MAX_TX_BLOCK;
#endif
    }
    return (size_t)nbytes;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t tcpserver_data_send(const uint8_t* data, size_t len, int push) {
    (void)push;
    int nbytes = lsock_send(f_sock, data, (int)len);
    return nbytes < 0 ? 0 : (size_t)nbytes;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t tcpserver_data_recv(uint8_t* data, size_t len) {
    int nbytes = 0;
    if (f_last_sock_state == LIP_TCP_SOCK_STATE_ESTABLISHED) {
#ifdef TCPSERVER_DATA_MAX_RX_BLOCK
        if (len > TCPSERVER_DATA_MAX_RX_BLOCK)
            len = TCPSERVER_DATA_MAX_RX_BLOCK;
#endif
        nbytes = lsock_recv(f_sock, data, (int)len);
        if (nbytes < 0)
            nbytes = 0;
    }
    return (size_t)nbytes;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_worker(void) {
    f_sockstate_monitor();
}
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
