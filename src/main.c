#include "global.h"
#include "settings.h"
#include "ltimer.h"
#include "dma.h"
#include "lip.h"
#include "usb.h"
#include "cpld.h"
#include "aux_io.h"
#include "ctlfunc.h"
#include "data.h"
#include "dbg_config.h"
#include "hostio.h"
#include "microtimer.h"
#include "ledblink.h"
#include "test.h"
#include "fwinfo.h"
#include "rtc.h"
#include "conf.h"
#include "boot.h"
#include "sdram_test.h"

/*================================================================================================*/
uint32_t SystemCoreClock;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static void f_hw_init(void)
    {
    SystemCoreClock = LPC_SYSCLK;
    lclock_init();
    lprintf_uart_init(115200, 8, LPRINTF_UART_PARITY_NONE);
    lprintf("app started!\n");
    microtimer_init();
    ledblink_init();
    dma_init();
    fwinfo_init();
    usb_init();
    aux_init();
    cpld_init();

    /* RTC используется только в модели LTR-CEU-1 */
    if (FWINFO_DEV_MODEL_CEU == fwinfo_get_dev_model())
        rtc_init();
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
__attribute__ ((__noreturn__))
static void f_indicate_fatal_error(void)
    {
    uint32_t t0 = microtimer_now();
    ledblink_setmode(LEDBLINK_MODE_STEADY(1));
    LPC_PIN_OUT(SET_PIN_USB_RED, 1);
    LPC_PIN_OUT(SET_PIN_USB_GREEN, 0);
    for (;;)
        {
        uint32_t t1 = microtimer_now();
        if ((uint32_t)(t1 - t0) >= MICROTIMER_uS(250000))
            {
            LPC_PIN_TOGGLE(SET_PIN_USB_RED);
            t0 = t1;
            }
        lpc_wdt_reset();
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int main(void)
    {
    int lip_started = 0;

    f_hw_init();

    /* Быстрый тест SDRAM */
    if (sdram_test_post() != SDRAM_TEST_RES_SUCCESS)
        f_indicate_fatal_error();

    conf_load();

    if (CONF_IFACE_NET == g_conf_user.iface)
        {
        const uint8_t* mac_addr = (g_conf_user.net_options & CONF_NETOPT_USERMAC)
            ? g_conf_user.mac_addr : g_conf_fact.mac_addr;
        lip_init();
        lip_eth_set_mac_addr(mac_addr);
        if (g_conf_user.net_options & CONF_NETOPT_DHCP)
            {
            lip_dhcpc_enable(1);
            }
        else
            {
            lip_ipv4_set_addr(g_conf_user.ip_addr);
            lip_ipv4_set_mask(g_conf_user.ip_netmask);
            lip_ipv4_set_gate(g_conf_user.ip_gateway);
            }
        lip_started = 1;
        }
    else
        {
#ifdef LIP_PHY_HARDWARE_RESET
        /* Отключить PHY */
        LIP_PHY_RESET_CFG();
        LIP_PHY_RESET_SET();
#endif
        }

    /* инициализация hostio ПОСЛЕ lip_init! */
    hostio_init(g_conf_user.iface);

    ledblink_setmode(LEDBLINK_MODE_BLINK_MS(500));

    /* запуск ПЛИС */
    data_restart();
    cpld_start();
    ctlfunc_startup(); /* после cpld_start! */

    for (;;)
        {
        lpc_wdt_reset();
        hostio_worker();

#if TEST_SEND_RESETS
        {
        static t_ltimer rst_tmr;
        static const uint32_t rst = 0x01018080;
        if (!rst_tmr.interval) ltimer_set(&rst_tmr, 1);
        if (ltimer_expired(&rst_tmr))
            {
            ltimer_reset(&rst_tmr);
            data_put(&rst, 1);
            }
        }
#endif

        usb_worker();
        if (lip_started) lip_pull();
        data_worker();
        cpld_worker();
        ledblink_worker();
        rtc_worker();
        boot_worker();
        }
    }
/*------------------------------------------------------------------------------------------------*/

#if 0
/*------------------------------------------------------------------------------------------------*/
/* Heap for sprintf (if needed) */
#include <sys/types.h>
extern char free_memory_start, free_memory_end; /* linker symbols */
caddr_t _sbrk(int incr)
    {
    static int heap_used = 0;
    int prev_heap_used = heap_used;
    if (heap_used + incr > &free_memory_end - &free_memory_start)
        return (caddr_t)NULL;
    heap_used += incr;
    return (caddr_t)(&free_memory_start + prev_heap_used);
    }
/*------------------------------------------------------------------------------------------------*/
#endif
/*================================================================================================*/
