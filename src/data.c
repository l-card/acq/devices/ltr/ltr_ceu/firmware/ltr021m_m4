/*================================================================================================*
 * Преобразование данных из формата ПЛИС (3 9-битных слова) в формат крейт-контроллера (4 байта)
 * и обратно с буферизацией.
 *================================================================================================*/

#include "data.h"
#include "cpld.h"
#include <string.h>

/*================================================================================================*/
#if DATA_USE_DMA_INTERRUPT && !CPLD_RX_DMA_IRQ_BITS
#error "data.c cannot use DMA interrupt if CPLD_RX_DMA_IRQ_BITS is not enabled in cpld.h"
#endif
/*================================================================================================*/

/*================================================================================================*/
/* прием */
static volatile uint8_t f_in_bytepos = 0;   /* позиция байта в 3-байтовой посылке ПЛИС */
static volatile uint32_t f_in_word_tmp;     /* собираемое 32-битное слово */
static volatile uint32_t f_in_mark_tmp;     /* собираемая синхрометка */
volatile DATA_IN_BUF_DECL data_host_in_t g_data_in_buf[DATA_IN_BUF_LEN];
volatile size_t g_data_in_head = 0, g_data_in_tail = 0;

/* передача */
DATA_OUT_BUF_DECL data_host_out_t g_data_out_buf[DATA_OUT_BUF_LEN];
size_t g_data_out_head = 0, g_data_out_tail = 0;

/* отладочные переменные */
data_diag_t g_data_diag;

#if DATA_DIAG_TRACE_LEN > 0
data_diag_trace_t g_data_diag_trace;

#define DIAG_TRACE(in_or_out, word) do { \
    unsigned int pos = g_data_diag_trace.in_or_out ## _pos; \
    g_data_diag_trace.in_or_out[pos++] = (uint16_t)(word); \
    if (pos >= ARRLEN(g_data_diag_trace.in_or_out)) pos = 0; \
    g_data_diag_trace.in_or_out ## _pos = (typeof(g_data_diag_trace.in_or_out ## _pos))pos; \
    } while (0)
#endif
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/* Преобразование считанных данных от ПЛИС в формат хоста */
__attribute__((__noinline__)) static void f_cpld_to_host(size_t nwords) {
    uint8_t bytepos = f_in_bytepos;
    uint32_t word_tmp = f_in_word_tmp;
    uint32_t mark_tmp = f_in_mark_tmp;
    size_t tail = g_data_in_tail;
    size_t head = g_data_in_head;
    size_t buf_free = head > tail ? head - tail - 1 : ARRLEN(g_data_in_buf) + head - tail - 1;
    /* признак, что нужно послать сообщение о переполнении. сохраняется между вызовами, т.к.
       скорее всего если буфер переполнен, то послать сам признак переполнения
       можно будет только когда буфер освободится */
    static int send_ov_signal_req = 0;

#define IN_BUF_PUT(word) do { \
        g_data_in_buf[tail] = (data_host_in_t)(word); tail = (tail + 1) % ARRLEN(g_data_in_buf); buf_free--; \
    } while (0)

    /* если был признак переполнения, то в первую очередь
       пытаемся послать соответствующее сообщение */
    if (send_ov_signal_req && (buf_free >= DATA_OVF_SIGNAL_LEN)) {
        IN_BUF_PUT(DATA_SIGNAL_PREFIX);
        IN_BUF_PUT(DATA_SIGNAL_BUFFER_OVERFLOW);
        send_ov_signal_req = 0;
    }

    __dmb(); /* Перед чтением памяти, куда писал контроллер DMA */

    /* Всегда обрабатываем все слова, а если буфер переполнен, записываем признак ошибки */
    g_data_diag.in_word_count += nwords;



    while (nwords--) {
        uint32_t data = cpld_recv_get_word();
#if DATA_DIAG_TRACE_LEN > 0
        DIAG_TRACE(in, data);
#endif
#if TEST_RAW_DATA_TO_HOST
        if (buf_free > 0)
            IN_BUF_PUT(data);
        else
            g_data_diag.in_ovf_count++;
#else
        int buf_ovf = send_ov_signal_req;

        //@@ пока метки времени только простые, не irig

        /* 1) обычная посылка
         Формат посылки ПЛИС: 0C.NNNNNNNN s0.DDDDDDDD t0.dddddddd
         Формат слова хоста:     DDDDDDDD dddddddd C0000000 NNNNNNNN
           2) метка времени без данных
         Формат посылки ПЛИС: 11.stXXXXXX
         Формат слова хоста:     00ts0000 00000000 11111111 00010000
         */
        
        if ((data & 0x300) != 0x300) {
            /* обычная посылка */
            if (data & 0x100) {
                /* слово 1/3 для команды: содержит биты [7:0] NNNNNNNN */
                word_tmp = 0x8000 | (data & 0xFF);
                mark_tmp = 0;
                bytepos = 1;
            } else switch (++bytepos) {
                case 1:     /* слово 1/3 для данных: содержит биты [7:0] NNNNNNNN */
                    word_tmp = data & 0xFF;
                    mark_tmp = 0;
                    break;
                case 2:     /* слово 2/3: содержит метку "старт" и биты [31:24] DDDDDDDD */
                    word_tmp |= data << 24;
                    mark_tmp |= (data & 0x200) << 19;   /* s << 28 */
                    break;
                case 3:     /* слово 3/3: содержит метку "секунда" и биты [23:16] dddddddd */
                    bytepos = 0;
                    word_tmp |= (data & 0xFF) << 16;
                    mark_tmp |= (data & 0x200) << 20;   /* t << 29 */
                    if (mark_tmp) {
                        if (!buf_ovf)
                            buf_ovf = (buf_free < DATA_OVF_SIGNAL_LEN + 1);
                        if (!buf_ovf)
                            IN_BUF_PUT(DATA_TIMESTAMP_WORD | mark_tmp);
                    }
                    if (!buf_ovf)
                        buf_ovf = (buf_free < DATA_OVF_SIGNAL_LEN + 1);
                    if (!buf_ovf)
                        IN_BUF_PUT(word_tmp);
                    break;
            }
        } else {
            /* синхрометка без данных */
            data &= 0xC0;   /* st000000 */
            data = ((data >> 1) | (data >> 3)) & 0x30; /* 00ts0000 */
            if (!buf_ovf)
                buf_ovf = (buf_free < DATA_OVF_SIGNAL_LEN + 1);
            if (!buf_ovf)
                IN_BUF_PUT(DATA_TIMESTAMP_WORD | (data << 24));
        }

        send_ov_signal_req = buf_ovf;
#endif
    } /* while */

    f_in_bytepos = bytepos;
    f_in_word_tmp = word_tmp;
    f_in_mark_tmp = mark_tmp;
    g_data_in_tail = tail;
#undef IN_BUF_PUT
    }
/*------------------------------------------------------------------------------------------------*/

#if DATA_USE_DMA_INTERRUPT
/*------------------------------------------------------------------------------------------------*/
void __attribute__ ((__interrupt__)) DMA_IRQHandler(void) {
    size_t nw = cpld_recv_words_avail();
    LPC_GPDMA->INTTCCLEAR = 0xFF;
    LPC_GPDMA->INTERRCLR = 0xFF;
    f_cpld_to_host(nw);
    g_data_diag.in_word_count_irq += nw;
    if (nw > g_data_diag.max_in_words_irq)
        g_data_diag.max_in_words_irq = nw;
}
/*------------------------------------------------------------------------------------------------*/
#endif

/*------------------------------------------------------------------------------------------------*/
void data_restart(void) {
#if DATA_USE_DMA_INTERRUPT
    NVIC_DisableIRQ(DMA_IRQn);
#endif
    f_in_bytepos = 0;
    g_data_in_head = g_data_in_tail;
    g_data_out_head = g_data_out_tail;

    /* обнуление отладочных переменных */
    memset(&g_data_diag, 0, sizeof(g_data_diag));
#if DATA_DIAG_TRACE_LEN > 0
    memset(&g_data_diag_trace, 0, sizeof(g_data_diag_trace));
#endif

#if DATA_USE_DMA_INTERRUPT
    NVIC_EnableIRQ(DMA_IRQn);
#endif
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void data_stop(void) {
#if DATA_USE_DMA_INTERRUPT
    NVIC_DisableIRQ(DMA_IRQn);
#endif
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void data_worker(void) {
    /* Преобразование данных от ПЛИС в формат хоста */
#if DATA_USE_DMA_INTERRUPT
    NVIC_DisableIRQ(DMA_IRQn);
#endif
    size_t nw_in = cpld_recv_words_avail();
    if (nw_in) {
        f_cpld_to_host(nw_in);
        if (nw_in > g_data_diag.max_in_words_bgnd)
            g_data_diag.max_in_words_bgnd = nw_in;
    }
#if DATA_USE_DMA_INTERRUPT
    NVIC_EnableIRQ(DMA_IRQn);
#endif

    /* Преобразование данных от хоста в формат ПЛИС и их посылка */
    size_t nw_out = 0;
    while (g_data_out_head != g_data_out_tail) {
        data_host_out_t host_word = g_data_out_buf[g_data_out_head];

#if TEST_RAW_DATA_FROM_HOST
        /* тест -- копирование сырых данных */
        if (!cpld_send_word(host_word)) break;
#if DATA_DIAG_TRACE_LEN > 0
        DIAG_TRACE(out, host_word);
#endif
        nw_out++;
#else
        /*
         Формат слова хоста:  DDDDDDDD dddddddd CYFTMMMM NNNNNNNN
         Формат посылки ПЛИС: C.NNNNNNNN 0.DDDDDDDD 0.dddddddd
         */
        /* В модуль передаются только слова, адресованные слоту 0:
           CYFTMMMM = x0xx0000 */
        if (0x0000 == (host_word & 0x4F00)) {
            uint16_t w0 = (uint16_t)((host_word & 0xFF) | ((host_word >> 7) & 0x100));
            uint16_t w1 = (uint16_t)((host_word >> 24) & 0xFF);
            uint16_t w2 = (uint16_t)((host_word >> 16) & 0xFF);
            if (!cpld_send_3words(w0, w1, w2))
                break;
#if DATA_DIAG_TRACE_LEN > 0
            DIAG_TRACE(out, w0);
            DIAG_TRACE(out, w1);
            DIAG_TRACE(out, w2);
#endif
            nw_out += 3;
        }
#endif
        g_data_out_head = (g_data_out_head + 1) % ARRLEN(g_data_out_buf);
    }
    g_data_diag.out_word_count += nw_out;
}
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
