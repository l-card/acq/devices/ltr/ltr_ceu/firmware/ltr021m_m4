#ifndef SETTINGS_H_
#define SETTINGS_H_

#include "global.h"

/*================================================================================================*/
#define CONCAT3(a, b, c)            CONCAT(CONCAT(a, b), c)
#define CONCAT4(a, b, c, d)         CONCAT(CONCAT(CONCAT(a, b), c), d)
/*================================================================================================*/

/* Включение модуля Real-time clock (пока экспериментальная реализация) */
#define SET_RTC_ENABLE              1

/* Использование второго ядра (Cortex-M0) для посылки данных в модуль */
/* В данной версии только 1, вариант кода без M0 убран (src/unused/cpld-without-m0core/) */
#define SET_USE_M0_CORE             1

/*================================================================================================*/
/* Распределение номеров периферийных устройств */
/*================================================================================================*/
/* Порты SSP */
#define SET_SSPn_CPLD_IN            0   /* порт чтения данных из ПЛИС */
#define SET_SSPn_CPLD_OUT           1   /* порт записи данных в ПЛИС */

/* Таймеры (TIMER0..3) */
#define SET_TIMERn_SSP_IN           2

/* USART */
#define SET_USARTn_CPLD_REG         0   /* USART для программирования регистров ПЛИС */

/* Каналы DMA */
#define SET_DMACH_SSP_IN_TX         0
#define SET_DMACH_SSP_IN_RX         1

/*================================================================================================*/
/* Назначение ножек контроллера и параметры интерфейсов */
/*================================================================================================*/

/* Порт чтения данных из ПЛИС */
#define SET_SSP_IN_BITRATE          50000000
#if (SET_SSPn_CPLD_IN == 0) && (SET_TIMERn_SSP_IN == 2)
#define SET_SSP_IN_TIMER_CAPn       0   /* номер входа CAPn таймера, на который подается DRDY */
#define SET_SSP_IN_TIMER_MATn       0   /* номер регистра MATn таймера, используемого для DMA RQ */
#define SET_PIN_SSP_IN_CLK          LPC_PIN_P3_3_SSP0_SCK
#define SET_PIN_SSP_IN_DR_M         LPC_PIN_P3_6_SSP0_MISO
#define SET_PIN_SSP_IN_FS           LPC_PIN_P3_8_SSP0_SSEL
#define SET_SSP_IN_DMARQ_RX         DMA_REQ9_SSP0RX
#define SET_SSP_IN_DMARQ_TX         DMA_REQ5_T2MAT0
#define SET_PIN_CPLD_DRDY           LPC_PIN_P6_1_T2_CAP0
#define SET_GIMACFG_CPLD_DRDY       (LPC_GIMA_MODE_DIRECT | LPC_GIMA_SEL_CAP2_0_T2CAP0)
#endif

/* Порт записи данных в ПЛИС */
#define SET_SSP_OUT_BITRATE         20000000
#define SET_SSP_OUT_TBYTE_SLOW_NS   14000   /* ns per byte @ low speed */
#define SET_SSP_OUT_TBYTE_FAST_NS   600     /* ns per byte @ high speed */
#if (SET_SSPn_CPLD_OUT == 1)
#define SET_PIN_SSP_OUT_SCK         LPC_PIN_PF_4_SSP1_SCK
#define SET_PIN_SSP_OUT_MOSI        LPC_PIN_PF_7_SSP1_MOSI
#define SET_PIN_SSP_OUT_SSEL        LPC_PIN_PF_5_SSP1_SSEL
#endif

/* Порт UART для программирования регистров ПЛИС (synchronous slave 5 MHz) */
#if SET_USARTn_CPLD_REG == 0
#define SET_PIN_USART_CPLDREG_TXD   LPC_PIN_PF_10_U0_TXD
#define SET_PIN_USART_CPLDREG_RXD   LPC_PIN_PF_11_U0_RXD
#define SET_PIN_USART_CPLDREG_CLK   LPC_PIN_PF_8_U0_UCLK
#endif

/* Синхрометки */
#define SET_AUX_CTOUT_OR_WITH_TIMER 0
#define SET_PIN_MARK_START          LPC_PIN_P7_0_CTOUT_14   /* CTOUT_14 can be ORed with T3MAT2 */
#define SET_AUX_MARK_START_CTOUTn   14
#define SET_PIN_MARK_1S             LPC_PIN_P4_2_CTOUT_0
#define SET_AUX_MARK_1S_CTOUTn      0
#define SET_PIN_IRIG_IN             LPC_PIN_P8_4_GPIO4_4    /* T0_CAP0 */

/* DIGIN, DIGOUT */
#define SET_PIN_DIGIN1_GPIO         LPC_PIN_P3_5_GPIO1_15
#define SET_PIN_DIGIN1_UART         LPC_PIN_P3_5_U1_RXD     /* альтернативная конфигурация */
#define SET_PIN_DIGOUT1_GPIO        LPC_PIN_P3_4_GPIO1_14
#define SET_PIN_DIGOUT1_UART        LPC_PIN_P3_4_U1_TXD     /* альтернативная конфигурация */
#define SET_PIN_DIGIN2_GPIO         LPC_PIN_P7_2_GPIO3_10
#define SET_PIN_DIGIN2_UART         LPC_PIN_P7_2_U2_RXD     /* альтернативная конфигурация */
#define SET_PIN_DIGOUT2_GPIO        LPC_PIN_P7_1_GPIO3_9
#define SET_PIN_DIGOUT2_UART        LPC_PIN_P7_1_U2_TXD     /* альтернативная конфигурация */
#define SET_PIN_DTR1_GPIO           LPC_PIN_PC_12_GPIO6_11
#define SET_PIN_DTR1_UART           LPC_PIN_PC_12_U1_DTR    /* альтернативная конфигурация */

/* GPIO: Присутствие модуля, in, активный 1 */
#define SET_PIN_MODULE_DETECT       LPC_PIN_P3_7_GPIO5_10
#define SET_MODULE_DETECT_BOUNCE_uS 100000

/* GPIO: Сброс ПЛИС, out, активный 1 */
#define SET_PIN_CPLD_RESET          LPC_PIN_P6_7_GPIO5_15

/* GPIO: Сброс PHY, out, активный 0 */
#define SET_PIN_ETH_PHY_nRESET      LPC_PIN_P9_0_GPIO4_12

/* GPIO: Вариант изделия: 0 = LTR-CU-1 (USB), 1 = LTR-CEU-1 (USB/Ethernet) */
#define SET_PIN_DEV_MODEL           LPC_PIN_P4_1_GPIO2_1
#define SET_PINVAL_DEV_MODEL_CU     0
#define SET_PINVAL_DEV_MODEL_CEU    1

/* GPIO: Светодиоды, out, активный 1 */
#define SET_PIN_LED_DEBUG           LPC_PIN_P1_4_GPIO0_11
#define SET_PIN_USB_GREEN           LPC_PIN_P8_5_GPIO4_5
#define SET_PIN_USB_RED             LPC_PIN_P8_7_GPIO4_7

/* GPIO: Сигнал длительного нажатия кнопки RESET, in (1 = нормальный, 0 = более 10 с) */
#define SET_PIN_ALTERN_BOOT_L       LPC_PIN_PB_6_GPIO5_26
/*================================================================================================*/

#endif /* SETTINGS_H_ */
