#include "boot.h"
#include "global.h"
#include "ltimer.h"
#include "hostio.h"
#include "lusb.h"
#include "lip.h"
#include "fast_crc.h"
#include "cpld.h"
#include "tcpserver.h"

#define LBOOT_USE_TFTP_SERVER
#define LBOOT_USE_USB
#define LBOOT_CHIP_FLASH_START_ADDR 0x1A000000UL
#include "lboot_req.h"

/*================================================================================================*/
#define USE_CURRENT_LIP_ADDR    0

#if USE_CURRENT_LIP_ADDR
#include "lip_private.h" /* FIXME: ugly hack used to get current MAC addr */
#endif
/*================================================================================================*/

/*================================================================================================*/
/* Структура с информацией о прошивке для bootloader'а */
__attribute__ ((used, section(".appl_info")))
static const struct {
    t_app_info app_info; /* обязательно в начале структуры */
    char fw_ver[16]; /* чтобы версию было видно при визуальном просмотре бинарника */
    }
    boot_id = {
        .app_info = {
            .size = sizeof(t_app_info),
            .flags = LBOOT_APP_FLAGS_STABLE,
            .devname = FWINFO_DEVICE_NAME_BOOTLD
            },
        .fw_ver = FWINFO_FW_VERSION_STR
    };

static boot_rst_t f_reboot_req = BOOT_RST_NONE;
static t_ltimer f_reboot_timer;
static char f_stage;

static t_lboot_params rq = {
    .hdr = {
        .flags = LBOOT_REQ_FLAGS_RECOVERY_WR | LBOOT_REQ_FLAGS_ENABLE_NO_SIGN,
        .timeout = BOOT_BOOTLD_TIMEOUT_MS,
        .reserv = { 0, 0 },
        .devinfo = {
            .devname = FWINFO_DEVICE_NAME_BOOTLD,
            .soft_ver = FWINFO_FW_VERSION_STR,
            .brd_revision = STRINGIZE(FWINFO_HW_REVISION),
            .serial = "",
            .brd_impl = "",
            .spec_info = ""
            }
        }
    };
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static void f_reset_clkgen(void)    /* Сброс CGU (clock generation unit) */
    {
    /* Согласно datasheet сбрасывать CGU через RGU нельзя, поэтому процедура программная */

    /* First of all, select IRC as system clock */
    LPC_CGU->BASE_CLK[CLK_BASE_MX] = LPC_BASECFG(LPC_CLKSEL_IRC, 0);

    LPC_CGU->XTAL_OSC_CTRL |= LPC_CGU_XTAL_OSC_CTRL_PD;     /* power down XTAL OSC */
    LPC_CGU->PLL[CGU_USB_PLL].PLL_CTRL |= LPC_CGU_PLL0_CTRL_PD;
    LPC_CGU->PLL[CGU_AUDIO_PLL].PLL_CTRL |= LPC_CGU_PLL0_CTRL_PD;
    LPC_CGU->PLL1_CTRL |= LPC_CGU_PLL1_CTRL_PD;

    LPC_CGU->IDIV_CTRL[CLK_IDIV_A] = LPC_CLKCFG(LPC_CLKSEL_IRC, 1, 0);
    LPC_CGU->IDIV_CTRL[CLK_IDIV_B] = LPC_CLKCFG(LPC_CLKSEL_IRC, 1, 0);
    LPC_CGU->IDIV_CTRL[CLK_IDIV_C] = LPC_CLKCFG(LPC_CLKSEL_IRC, 1, 0);
    LPC_CGU->IDIV_CTRL[CLK_IDIV_D] = LPC_CLKCFG(LPC_CLKSEL_IRC, 1, 0);
    LPC_CGU->IDIV_CTRL[CLK_IDIV_E] = LPC_CLKCFG(LPC_CLKSEL_IRC, 1, 0);

    LPC_CGU->BASE_CLK[CLK_BASE_USB0] =     LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_PERIPH] =   LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_USB1] =     LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_SPIFI] =    LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_SPI] =      LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_PHY_RX] =   LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_PHY_TX] =   LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_APB1] =     LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_APB3] =     LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_LCD] =      LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_ADCHS] =    LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_SDIO] =     LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_SSP0] =     LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_SSP1] =     LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_UART0] =    LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_UART1] =    LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_UART2] =    LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_UART3] =    LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_OUT] =      LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_APLL] =     LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_CGU_OUT0] = LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    LPC_CGU->BASE_CLK[CLK_BASE_CGU_OUT1] = LPC_BASECFG(LPC_CLKSEL_IRC, 0);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_make_lboot_request(void)  /* Заполнить запрос для lboot */
    {
    strncpy(rq.hdr.devinfo.serial, g_conf_fact.serial, sizeof(rq.hdr.devinfo.serial) - 1);
    strncpy(rq.hdr.devinfo.brd_impl, fwinfo_get_dev_name(), sizeof(rq.hdr.devinfo.brd_impl) - 1);

    switch (f_reboot_req)
        {
        case BOOT_RST_BOOTLDR_NET:
            rq.hdr.size = LBOOT_REQ_SIZE(tftp);
            rq.hdr.bootmode = LBOOT_BOOTMODE_TFTP_SERVER;
            rq.tftp.flags = 0;
#if !USE_CURRENT_LIP_ADDR 
            /* Настройки сети берем не активные, а те, что записаны в EEPROM, кроме случая DHCP,
               потому что lboot не поддерживает DHCP */
            memcpy(rq.tftp.mac,
                ((g_conf_user.net_options & CONF_NETOPT_USERMAC)
                    ? g_conf_user.mac_addr : g_conf_fact.mac_addr),
                sizeof(rq.tftp.mac));

            if (g_conf_user.net_options & CONF_NETOPT_DHCP)
                {
                /* Если выбрана конфигурация по DHCP, а интерфейс неактивен, то непонятно, как
                   вызывать lboot. В норме этого не должно быть, т.к. предполагается, что
                   команда прошиваться по сети должна передаваться только по сети (f_reboot_req
                   должен соответствовать интерфейсу, по которому пришел запрос на перезагрузку) */
                memcpy(rq.tftp.l_ip, lip_ipv4_cur_addr(), sizeof(rq.tftp.l_ip));
                memcpy(rq.tftp.mask, lip_ipv4_cur_mask(), sizeof(rq.tftp.mask));
                memcpy(rq.tftp.gate, lip_ipv4_cur_gate(), sizeof(rq.tftp.gate));
                }
            else
                {
                memcpy(rq.tftp.l_ip, g_conf_user.ip_addr, sizeof(rq.tftp.l_ip));
                memcpy(rq.tftp.mask, g_conf_user.ip_netmask, sizeof(rq.tftp.mask));
                memcpy(rq.tftp.gate, g_conf_user.ip_gateway, sizeof(rq.tftp.gate));
                }
#else
            extern t_lip g_lip_st;
            /* Настройки сети всегда активные (lip должен быть инициализирован!) */
            memcpy(rq.tftp.mac, g_lip_st.eth.addr, sizeof(rq.tftp.mac));
            memcpy(rq.tftp.l_ip, lip_ipv4_cur_addr(), sizeof(rq.tftp.l_ip));
            memcpy(rq.tftp.mask, lip_ipv4_cur_mask(), sizeof(rq.tftp.mask));
            memcpy(rq.tftp.gate, lip_ipv4_cur_gate(), sizeof(rq.tftp.gate));
#endif
            rq.tftp.crc = CRC16_Block8(0, (const uint8_t*)&rq, offsetof(t_lboot_params, tftp.crc));
            break;

        case BOOT_RST_BOOTLDR_USB:
        default:
            rq.hdr.size = LBOOT_REQ_SIZE(usb);
            rq.hdr.bootmode = LBOOT_BOOTMODE_USB;
            rq.usb.flags = 0;
            rq.usb.crc = CRC16_Block8(0, (const uint8_t*)&rq, offsetof(t_lboot_params, usb.crc));
            break;
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
__attribute__ ((__noreturn__))
static void f_jump_to_lboot(void)   /* Переход в bootloader с передачей запроса */
    {
    uint32_t dummy;

    lpc_wdt_reset();

    /* Запретить и очистить все прерывания */
    NVIC->ICER[0] = NVIC->ICER[0]; /* при чтении "1" в позициях разрешенных прерываний */
    NVIC->ICER[1] = NVIC->ICER[1];
    NVIC->ICPR[0] = NVIC->ICPR[0]; /* при чтении "1" в позициях прерываний в состоянии pending */
    NVIC->ICPR[1] = NVIC->ICPR[1];
    SysTick->CTRL = 0;

    /* Сброс генератороа клоков */
    f_reset_clkgen();

    /* Сбросить периферию (кроме WDT и CREG, которые программно сбросить нельзя, и FLASH). */

#define RST0(x) (1UL << RGU_ ## x ## _RST)
#define RST1(x) (1UL << (RGU_ ## x ## _RST - 32))

    LPC_RGU->RESET_CTRL0 =
        RST0(SCU) | RST0(M0SUB) | RST0(LCD) | RST0(USB0) | RST0(USB1) | RST0(DMA) |
        RST0(SDIO) | RST0(EMC) | RST0(ETHERNET) | RST0(EEPROM) | RST0(GPIO);
    LPC_RGU->RESET_CTRL1 = 
        RST1(TIMER0) | RST1(TIMER1) | RST1(TIMER2) | RST1(TIMER3) | RST1(RITIMER) | RST1(SCT) |
        RST1(MOTOCONPWM) | RST1(QEI) | RST1(ADC0) | RST1(ADC1) | RST1(DAC) |
        RST1(UART0) | RST1(UART1) | RST1(UART2) | RST1(UART3) | RST1(I2C0) | RST1(I2C1) |
        RST1(SSP0) | RST1(SSP1) | RST1(I2S) | RST1(SPIFI) | RST1(CAN1) | RST1(CAN0) |
        RST1(M0APP) | RST1(SGPIO) | RST1(SPI) | RST1(ADCHS);

    /* Wait until reset ends (1 IRC clock) */
    while (!(LPC_RGU->RESET_ACTIVE_STATUS1 & RST1(TIMER0)));

#undef RST1
#undef RST0

    /* Скопировать запрос в начало памяти и перейти на загрузчик.
     * Примечание: адрес LBOOT_REQ_ADDR, куда копируется запрос, находится в самом начале RAM.
     * Это может быть стек или область данных программы. В частности, структура rq может
     * пересекаться с нею. Однако поскольку копирование производится от начала к концу без
     * использования стека, а адрес источника >= адресу приемника, это не страшно,
     * поэтому rq может располагаться где угодно.
     */
    asm volatile
        (
        "   ldr sp, [%[boot_intvec]]        \n"     /* стек на всякий случай меняется сразу */
        "   cbz %[len], 1f                  \n"
        "0:"                                        /* копирование по байту (скороть не важна) */
        "   ldrb %[tmp], [%[src]], #1       \n"
        "   strb %[tmp], [%[dest]], #1      \n"
        "   subs %[len], %[len], #1         \n"
        "   bhi 0b                          \n"
        "1:"
        "   ldr pc, [%[boot_intvec], #4]"           /* переход на bootloader */
        : [tmp] "=&r" (dummy) /* "&" = не использовать регистр со входными операндами */
        : [dest] "r" (LBOOT_REQ_ADDR),
          [src] "r" (&rq),
          [len] "l" (rq.hdr.size),
          [boot_intvec] "r" (LBOOT_CHIP_FLASH_START_ADDR)
        : "cc", "memory"
        );

#if 0
    /* Системный сброс */
    LPC_RGU->RESET_CTRL0 = ~LPC_RGU->RESET_ACTIVE_STATUS0 | (1UL << RGU_CORE_RST);
#endif
    
    for (;;); /* avoid "noreturn function does return" error */
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
uint16_t boot_loader_version(void)
    {
    const t_lboot_info* lbinfo = (const t_lboot_info*)LBOOT_INFO_ADDR;
    if (LBOOT_INFO_SIGNATURE != lbinfo->sign)
        return 0;
    return lbinfo->ver;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void boot_schedule_reset(boot_rst_t rst_type)
    {
    f_reboot_req = rst_type;
    f_stage = 0;
    ltimer_set(&f_reboot_timer, LTIMER_MS_TO_CLOCK_TICKS(BOOT_RESET_DELAY1_MS));
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void boot_worker(void)
    {
    if ((f_reboot_req != BOOT_RST_NONE) && ltimer_expired(&f_reboot_timer))
        { 
        if (!f_stage)
            { /* Таймаут 1 (на отправку ответа), закрыть соединение */
            hostio_close();
            /* Если подключение по IP, hostio_close() инициирует закрытие сокетов, на которое нужно
               еще какое-то время */
#if BOOT_RESET_DELAY2_MS <= TCPSERVER_HANGUP_TIMEOUT_MS
#error "BOOT_RESET_DELAY2_MS must be greater than TCPSERVER_HANGUP_TIMEOUT_MS"
#endif
            f_stage = 1;
            ltimer_set(&f_reboot_timer, LTIMER_MS_TO_CLOCK_TICKS(BOOT_RESET_DELAY2_MS));
            }
        else
            { /* Таймаут 2 (на закрытие интерфейса), перезагрузить процессор */
            int to_lboot = (f_reboot_req != BOOT_RST_NORMAL) && (boot_loader_version() != 0);
            if (to_lboot)
                f_make_lboot_request(); /* заполнить запрос, пока не закрыты интерфейсы */
            lip_close();
            lusb_close();
            cpld_stop(); /* остановить все операции по DMA */
            if (to_lboot)
                { /* reboot to bootloader */
                f_jump_to_lboot();
                }
            else
                { /* reboot normally */
                LPC_RGU->RESET_CTRL0 = ~LPC_RGU->RESET_ACTIVE_STATUS0 | (1UL << RGU_CORE_RST);
                }
            }
        }
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
