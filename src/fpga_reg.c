#include "fpga_reg.h"
#include "cpld.h"
#include "aux_io.h"
#include <string.h>

/*================================================================================================*/
static uint8_t f_reg_emu[FPGA_REG_COUNT_EMU];      
static uint8_t f_reg_native[FPGA_REG_COUNT_NATIVE];
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static int f_write_native(unsigned int idx, uint8_t val)
    {
    if (idx >= FPGA_REG_COUNT_NATIVE) return 0;
    f_reg_native[idx] = val;
    return cpld_write_regs(idx, &f_reg_native[idx], 1);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static int f_set_digout_emu(unsigned int emu_reg, uint8_t emu_val)
    {
    unsigned int native_reg, digout_n;
    uint8_t native_val;

    if (FPGA_REGIDX_EMU_DIGOUT1 == emu_reg)
        {
        native_reg = FPGA_REGIDX_NAT_DIGOUT1; digout_n = AUX_DIGOUT1;
        }
    else
        {
        native_reg = FPGA_REGIDX_NAT_DIGOUT2; digout_n = AUX_DIGOUT2;
        }

    switch (emu_val)
        {
        case FPGA_REGVAL_EMU_DIGOUTx_DIGIN1:
            native_val = FPGA_REGVAL_NAT_DIGOUTx_DIGIN1;
            break;
        case FPGA_REGVAL_EMU_DIGOUTx_DIGIN2:
            native_val = FPGA_REGVAL_NAT_DIGOUTx_DIGIN2;
            break;
        case FPGA_REGVAL_EMU_DIGOUTx_START:
            native_val = FPGA_REGVAL_NAT_DIGOUTx_START;
            break;
        case FPGA_REGVAL_EMU_DIGOUTx_1S:
            native_val = FPGA_REGVAL_NAT_DIGOUTx_1S;
            break;
        case FPGA_REGVAL_EMU_DIGOUTx_UARTTX:
            native_val = FPGA_REGVAL_NAT_DIGOUTx_UARTTX;
            aux_digout_set_uarttx(digout_n);
            break;
        case FPGA_REGVAL_EMU_DIGOUTx_PG13:
            if (AUX_DIGOUT2 == digout_n)
                {
                native_val = FPGA_REGVAL_NAT_DIGOUT2_UARTDTR;
                }
            else
                { /* Для DIGOUT1 обрабатывается как CONST0 */
                aux_digout_set_gpio(digout_n, 0);
                native_val = FPGA_REGVAL_NAT_DIGOUTx_UARTTX;
                }
            break;
        case FPGA_REGVAL_EMU_DIGOUTx_CONST1:
            native_val = FPGA_REGVAL_NAT_DIGOUTx_UARTTX;
            aux_digout_set_gpio(digout_n, 1);
            break;
        case FPGA_REGVAL_EMU_DIGOUTx_CONST0:
        case FPGA_REGVAL_EMU_DIGOUTx_IRIG:  //@@ not handled yet
        default:
            emu_val = FPGA_REGVAL_EMU_DIGOUTx_CONST0;
            native_val = FPGA_REGVAL_NAT_DIGOUTx_UARTTX;
            aux_digout_set_gpio(digout_n, 0);
            break;
        }
    f_reg_emu[emu_reg] = emu_val;
    return f_write_native(native_reg, native_val);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static int f_set_digout_native(unsigned int native_reg, uint8_t native_val)
    {
    unsigned int emu_reg =
        (FPGA_REGIDX_NAT_DIGOUT1 == native_reg) ? FPGA_REGIDX_EMU_DIGOUT1 : FPGA_REGIDX_EMU_DIGOUT2;
    uint8_t emu_val;

    switch (native_val)
        {
        case FPGA_REGVAL_NAT_DIGOUTx_DIGIN1:
            emu_val = FPGA_REGVAL_EMU_DIGOUTx_DIGIN1;
            break;
        case FPGA_REGVAL_NAT_DIGOUTx_DIGIN2:
            emu_val = FPGA_REGVAL_EMU_DIGOUTx_DIGIN2;
            break;
        case FPGA_REGVAL_NAT_DIGOUTx_START:
            emu_val = FPGA_REGVAL_EMU_DIGOUTx_START;
            break;
        case FPGA_REGVAL_NAT_DIGOUTx_1S:
            emu_val = FPGA_REGVAL_EMU_DIGOUTx_1S;
            break;
        case FPGA_REGVAL_NAT_DIGOUTx_UARTTX:
            emu_val = FPGA_REGVAL_EMU_DIGOUTx_UARTTX;
            break;
        case FPGA_REGVAL_NAT_DIGOUT2_UARTDTR:
            emu_val = FPGA_REGVAL_EMU_DIGOUTx_PG13;
            break;
        default:
            emu_val = FPGA_REGVAL_EMU_DIGOUTx_CONST0;
            break;
        }
    return f_set_digout_emu(emu_reg, emu_val);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static int f_set_start_emu(uint8_t emu_val)
    {
    const unsigned int reg = FPGA_REGIDX_NAT_STARTCTL;
    int res;
    aux_mark_start_off(); /* Если был включен генератор меток, выключить */
    switch (emu_val)
        {
        case FPGA_REGVAL_EMU_STARTCTL_DIGIN1R:
            res = f_write_native(reg, FPGA_REGVAL_NAT_STARTCTL_DIGIN1R);
            break;
        case FPGA_REGVAL_EMU_STARTCTL_DIGIN1F:
            res = f_write_native(reg, FPGA_REGVAL_NAT_STARTCTL_DIGIN1F);
            break;
        case FPGA_REGVAL_EMU_STARTCTL_DIGIN2R:
            res = f_write_native(reg, FPGA_REGVAL_NAT_STARTCTL_DIGIN2R);
            break;
        case FPGA_REGVAL_EMU_STARTCTL_DIGIN2F:
            res = f_write_native(reg, FPGA_REGVAL_NAT_STARTCTL_DIGIN2F);
            break;
        case FPGA_REGVAL_EMU_STARTCTL_NOW:
            res = f_write_native(reg, FPGA_REGVAL_NAT_STARTCTL_TIMER);
            aux_mark_start_go();   /* подать иипульс */
            break;
        case FPGA_REGVAL_EMU_STARTCTL_OFF:
        default:
            emu_val = FPGA_REGVAL_EMU_STARTCTL_OFF;
            res = f_write_native(reg, FPGA_REGVAL_NAT_STARTCTL_OFF);
            break;
        }
    f_reg_emu[FPGA_REGIDX_EMU_STARTCTL] = emu_val;
    return res;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static int f_set_1s_emu(uint8_t emu_val)
    {
    const unsigned int reg = FPGA_REGIDX_NAT_1SCTL;
    int res;
    aux_mark_1s_off(); /* Если был включен генератор меток, выключить */
    switch (emu_val)
        {
        case FPGA_REGVAL_EMU_1SCTL_DIGIN1R:
            res = f_write_native(reg, FPGA_REGVAL_NAT_1SCTL_DIGIN1R);
            break;
        case FPGA_REGVAL_EMU_1SCTL_DIGIN1F:
            res = f_write_native(reg, FPGA_REGVAL_NAT_1SCTL_DIGIN1F);
            break;
        case FPGA_REGVAL_EMU_1SCTL_DIGIN2R:
            res = f_write_native(reg, FPGA_REGVAL_NAT_1SCTL_DIGIN2R);
            break;
        case FPGA_REGVAL_EMU_1SCTL_DIGIN2F:
            res = f_write_native(reg, FPGA_REGVAL_NAT_1SCTL_DIGIN2F);
            break;
        case FPGA_REGVAL_EMU_1SCTL_NOW:
            res = f_write_native(reg, FPGA_REGVAL_NAT_1SCTL_TIMER);
            aux_mark_1s_go_single();   /* подать иипульс */
            break;
        case FPGA_REGVAL_EMU_1SCTL_1S:
            res = f_write_native(reg, FPGA_REGVAL_NAT_1SCTL_TIMER);
            aux_mark_1s_go();           /* включить импульсы */
            break;
        case FPGA_REGVAL_EMU_1SCTL_OFF:
        default:
            emu_val = FPGA_REGVAL_EMU_1SCTL_OFF;
            res = f_write_native(reg, FPGA_REGVAL_NAT_1SCTL_OFF);
            break;
        }
    f_reg_emu[FPGA_REGIDX_EMU_1SCTL] = emu_val;
    return res;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void fpga_reg_reset(void)
    {
    /* Установка регистров в начальное состояние */
    memset(f_reg_emu, 0, sizeof(f_reg_emu));
    memset(f_reg_native, 0, sizeof(f_reg_native));

    /* Установка регистров, имеющих ненулевое значение или побочный эффект записи */
    fpga_reg_set_emu(FPGA_REGIDX_EMU_TEST1, 0);
    fpga_reg_set_emu(FPGA_REGIDX_EMU_TEST2, 1);
    fpga_reg_set_emu(FPGA_REGIDX_EMU_DIGOUTEN, 0);
    fpga_reg_set_emu(FPGA_REGIDX_EMU_DIGOUT1, FPGA_REGVAL_EMU_DIGOUTx_CONST0);
    fpga_reg_set_emu(FPGA_REGIDX_EMU_DIGOUT2, FPGA_REGVAL_EMU_DIGOUTx_CONST0);
    fpga_reg_set_emu(FPGA_REGIDX_EMU_STARTCTL, FPGA_REGVAL_EMU_STARTCTL_OFF);
    fpga_reg_set_emu(FPGA_REGIDX_EMU_1SCTL, FPGA_REGVAL_EMU_1SCTL_OFF);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
uint8_t fpga_reg_get_emu(unsigned int idx)
    {
    return (idx < ARRLEN(f_reg_emu)) ? f_reg_emu[idx] : 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
uint8_t fpga_reg_get_native(unsigned int idx)
    {
    return (idx < ARRLEN(f_reg_native)) ? f_reg_native[idx] : 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void fpga_reg_set_emu(unsigned int idx, uint8_t val)
    {
    switch (idx)
        {
        case FPGA_REGIDX_EMU_TEST1:
            val &= FPGA_REGVAL_EMU_TEST1_RINGMODE;
            /* В регистр f_reg_emu[idx] не пишем, при чтении всегда 0 */
            f_write_native(FPGA_REGIDX_NAT_TEST, (val) ? FPGA_REGVAL_NAT_TEST_RINGMODE : 0);
            break;

        case FPGA_REGIDX_EMU_TEST2:
            f_reg_emu[idx] = val;  /* тестовый регистр, записывается любое значение */
            break;

        case FPGA_REGIDX_EMU_DIGOUTEN:
            val &= FPGA_REGVAL_EMU_DIGOUTEN_ENA;
            f_reg_emu[idx] = val;
            f_write_native(FPGA_REGIDX_NAT_DIGOUTEN, (val) ? FPGA_REGVAL_NAT_DIGOUTEN_ENA : 0);
            break;

        case FPGA_REGIDX_EMU_DIGOUT1:
        case FPGA_REGIDX_EMU_DIGOUT2:
            f_set_digout_emu(idx, val);
            break;

        case FPGA_REGIDX_EMU_STARTCTL:
            f_set_start_emu(val);
            break;

        case FPGA_REGIDX_EMU_1SCTL:
            f_set_1s_emu(val);
            break;
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void fpga_reg_set_native(unsigned int idx, uint8_t val)
    {
    switch (idx)
        {
        case FPGA_REGIDX_NAT_DIGOUTEN:
            val &= FPGA_REGVAL_NAT_DIGOUTEN_ENA;
            f_reg_emu[FPGA_REGIDX_EMU_DIGOUTEN] = (val) ? FPGA_REGVAL_EMU_DIGOUTEN_ENA : 0;
            f_write_native(idx, val);
            break;

        case FPGA_REGIDX_NAT_DIGOUT1:
        case FPGA_REGIDX_NAT_DIGOUT2:
            f_set_digout_native(idx, val);
            break;

        case FPGA_REGIDX_NAT_STARTCTL:
            switch (val)
                {
                case FPGA_REGVAL_NAT_STARTCTL_DIGIN1R: val = FPGA_REGVAL_EMU_STARTCTL_DIGIN1R; break;
                case FPGA_REGVAL_NAT_STARTCTL_DIGIN1F: val = FPGA_REGVAL_EMU_STARTCTL_DIGIN1F; break;
                case FPGA_REGVAL_NAT_STARTCTL_DIGIN2R: val = FPGA_REGVAL_EMU_STARTCTL_DIGIN2R; break;
                case FPGA_REGVAL_NAT_STARTCTL_DIGIN2F: val = FPGA_REGVAL_EMU_STARTCTL_DIGIN2F; break;
                case FPGA_REGVAL_NAT_STARTCTL_TIMER  : val = FPGA_REGVAL_EMU_STARTCTL_NOW; break;
                default:
                case FPGA_REGVAL_NAT_STARTCTL_OFF    : val = FPGA_REGVAL_EMU_STARTCTL_OFF; break;
                }
            f_set_start_emu(val);
            break;

        case FPGA_REGIDX_NAT_1SCTL:
            switch (val)
                {
                case FPGA_REGVAL_NAT_1SCTL_DIGIN1R: val = FPGA_REGVAL_EMU_1SCTL_DIGIN1R; break;
                case FPGA_REGVAL_NAT_1SCTL_DIGIN1F: val = FPGA_REGVAL_EMU_1SCTL_DIGIN1F; break;
                case FPGA_REGVAL_NAT_1SCTL_DIGIN2R: val = FPGA_REGVAL_EMU_1SCTL_DIGIN2R; break;
                case FPGA_REGVAL_NAT_1SCTL_DIGIN2F: val = FPGA_REGVAL_EMU_1SCTL_DIGIN2F; break;
                case FPGA_REGVAL_NAT_1SCTL_TIMER  : val = FPGA_REGVAL_EMU_1SCTL_1S; break;
                default:
                case FPGA_REGVAL_NAT_1SCTL_OFF    : val = FPGA_REGVAL_EMU_1SCTL_OFF; break;
                }
            f_set_1s_emu(val);
            break;

        case FPGA_REGIDX_NAT_TEST:
            val &= FPGA_REGVAL_NAT_TEST_RINGMODE;
            f_write_native(idx, val);
            break;
        }
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
