#include "conf.h"
#include "fast_crc.h"
#include "fwinfo.h"
#include <string.h>

/*================================================================================================*/
#define EEPROM_CLK          1500000UL
#define EEPROM_RPHASE1_NS   35
#define EEPROM_RPHASE2_NS   70
#define EEPROM_PHASE1_NS    20
#define EEPROM_PHASE2_NS    40
#define EEPROM_PHASE3_NS    10

typedef uint32_t ee_word_t;
#define EE_ADDR(ofs)        ((uintptr_t)&_eeprom_start + (ofs))
#define EE_WORD_AT(addr)    (*(volatile ee_word_t*)(addr))

#define CONF_FACT_EEOFS     0x0000      /* Смещение в EEPROM блока фабричных данных */
#define CONF_USER_EEOFS     0x0100      /* Смещение в EEPROM блока пользовательских данных */

#define CONF_FACT_EEADDR    EE_ADDR(CONF_FACT_EEOFS)
#define CONF_USER_EEADDR    EE_ADDR(CONF_USER_EEOFS)

/* Значения по умолчанию */
#define DFLT_SERIAL_NO      "NO_SERIAL"
#define DFLT_MAC_ADDR       "\x00\x00\xDE\xAD\xBE\xEF"
#define DFLT_IFACE          CONF_IFACE_USB
#define DFLT_NETOPT         CONF_NETOPT_DHCP
/*================================================================================================*/

/*================================================================================================*/
/* Размер хранилища структуры с заголовком и CRC */
#define EEPROM_STORE_SIZE(type) (sizeof(type) + sizeof(uint32_t))
/*================================================================================================*/

/*================================================================================================*/
/* Начало EEPROM, определенное линкером */
extern uint32_t _eeprom_start;

/* Рабочая копия конфигурации в RAM */
conf_fact_t g_conf_fact_w;
conf_user_t g_conf_user_w;

static char f_init_done = 0;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static void f_eeprom_init(void)
    {
    /* Check that EEPROM regions don't overlap */
    ASSERT(
    (CONF_FACT_EEOFS + EEPROM_STORE_SIZE(conf_fact_t) <= CONF_USER_EEOFS) ||
    (CONF_USER_EEOFS + EEPROM_STORE_SIZE(conf_user_t) <= CONF_FACT_EEOFS)
    );
    /* EEPROM structure sizes SHOULD be word aligned (but don't have to be) */
    ASSERT(!(EEPROM_STORE_SIZE(conf_fact_t) % sizeof(ee_word_t)));
    ASSERT(!(EEPROM_STORE_SIZE(conf_user_t) % sizeof(ee_word_t)));

    if (LPC_EEPROM->PWRDWN)
        {
        LPC_EEPROM->PWRDWN = 0;
        lpc_delay_clk(LPC_SYSCLK / 10000); /* 100 us */
        }
    LPC_EEPROM->CLKDIV = (LPC_CLK_EEPROM + EEPROM_CLK / 2) / EEPROM_CLK - 1;
    LPC_EEPROM->RWSTATE = 
        (((CYCLES_NS_NLESS(LPC_CLK_EEPROM, EEPROM_RPHASE1_NS) - 1) & 0xFF) << 8) |
        ((CYCLES_NS_NLESS(LPC_CLK_EEPROM, EEPROM_RPHASE2_NS) - 1) & 0xFF);
    LPC_EEPROM->WSTATE = 
        (((CYCLES_NS_NLESS(LPC_CLK_EEPROM, EEPROM_PHASE1_NS) - 1) & 0xFF) << 16) |
        (((CYCLES_NS_NLESS(LPC_CLK_EEPROM, EEPROM_PHASE2_NS) - 1) & 0xFF) << 8) |
        ((CYCLES_NS_NLESS(LPC_CLK_EEPROM, EEPROM_PHASE3_NS) - 1) & 0xFF);
    f_init_done = 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static uintptr_t f_read_eeprom(uintptr_t ee_addr, uint8_t* buf, size_t len)
    {
    /* Read EEPROM, converting byte access to 32-bit access */
    union { ee_word_t w; uint8_t b[sizeof(ee_word_t)]; } t;
    uintptr_t byte_ofs = ee_addr % sizeof(ee_word_t);
    t.w = EE_WORD_AT(ee_addr - byte_ofs);
    while (len--)
        {
        *buf++ = t.b[byte_ofs];
        byte_ofs = (++ee_addr) % sizeof(ee_word_t);
        if (!byte_ofs && len)
            t.w = EE_WORD_AT(ee_addr);
        }
    return ee_addr;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static int f_verify_eeprom(uintptr_t ee_addr, const uint8_t* buf, size_t len)
    {
    /* Read EEPROM, converting byte access to 32-bit access */
    union { ee_word_t w; uint8_t b[sizeof(ee_word_t)]; } t;
    uintptr_t byte_ofs = ee_addr % sizeof(ee_word_t);
    t.w = EE_WORD_AT(ee_addr - byte_ofs);
    while (len--)
        {
        if (*buf++ != t.b[byte_ofs])
            return 0;
        byte_ofs = (++ee_addr) % sizeof(ee_word_t);
        if (!byte_ofs && len)
            t.w = EE_WORD_AT(ee_addr);
        }
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static uintptr_t f_write_eeprom(uintptr_t ee_addr, const uint8_t* buf, size_t len)
    {
    /* Write EEPROM, converting byte access to 32-bit access */
    union { ee_word_t w; uint8_t b[sizeof(ee_word_t)]; } t;
    uintptr_t byte_ofs = ee_addr % sizeof(ee_word_t);
    uintptr_t ret_val = ee_addr + len;

    LPC_EEPROM->AUTOPROG = EEPROM_AUTOPROG_OFF;
    LPC_EEPROM->INTENCLR = EEPROM_INT_ENDOFPROG;

    ee_addr -= byte_ofs;
    t.w = EE_WORD_AT(ee_addr);
    while (len--)
        {
        t.b[byte_ofs++] = *buf++;
        byte_ofs %= sizeof(ee_word_t);
        if (!byte_ofs || !len)
            {
            EE_WORD_AT(ee_addr) = t.w;
            ee_addr += sizeof(ee_word_t);
            if (!len || !(ee_addr % EEPROM_PAGE_SIZE))
                { /* в конце страницы или после записи последнего слова выполнить программирование */
                LPC_EEPROM->INTSTATCLR = EEPROM_INT_ENDOFPROG;
                LPC_EEPROM->CMD = EEPROM_CMD_ERASE_PRG_PAGE;
                while (!(LPC_EEPROM->INTSTAT & EEPROM_INT_ENDOFPROG));
                }
            if (len && (len < sizeof(ee_word_t)))
                t.w = EE_WORD_AT(ee_addr);
            }
        }
    return ret_val;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static int f_load_conf(uintptr_t ee_addr, uint8_t* buf, size_t len)
    {
    uint32_t crc;
    f_read_eeprom(ee_addr, buf, len);
    f_read_eeprom(ee_addr + len, (uint8_t*)&crc, sizeof(crc));
    return (CRC32_Block8(0, buf, len) == crc);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_save_conf(uintptr_t ee_addr, const uint8_t* buf, size_t len)
    {
    uint32_t crc = CRC32_Block8(0, buf, len);
    if (!f_verify_eeprom(ee_addr, buf, len) ||
        !f_verify_eeprom(ee_addr + len, (uint8_t*)&crc, sizeof(crc)))
        {
        f_write_eeprom(ee_addr, buf, len);
        f_write_eeprom(ee_addr + len, (uint8_t*)&crc, sizeof(crc));
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int conf_load(void)                 /* Чтение конфигурации из EEPROM */
    {
    int fact_ok, user_ok;
    if (!f_init_done) f_eeprom_init();

    fact_ok = f_load_conf(CONF_FACT_EEADDR, (uint8_t*)&g_conf_fact_w, sizeof(g_conf_fact_w))
        && (CONF_FACT_SIG == g_conf_fact_w.signature)
        && (sizeof(g_conf_fact_w) == g_conf_fact_w.size);

    if (!fact_ok)
        {
        memset(&g_conf_fact_w, 0, sizeof(g_conf_fact_w));
        g_conf_fact_w.signature = CONF_FACT_SIG;
        g_conf_fact_w.size = sizeof(g_conf_fact_w);
#ifdef DFLT_SERIAL_NO
        strncpy(g_conf_fact_w.serial, DFLT_SERIAL_NO, sizeof(g_conf_fact_w.serial));
#endif
#ifdef DFLT_MAC_ADDR
        memcpy(g_conf_fact_w.mac_addr, DFLT_MAC_ADDR, sizeof(g_conf_fact_w.mac_addr));
#endif
        }

    user_ok = f_load_conf(CONF_USER_EEADDR, (uint8_t*)&g_conf_user_w, sizeof(g_conf_user_w))
        && (CONF_USER_SIG == g_conf_user_w.signature)
        && (sizeof(g_conf_user_w) == g_conf_user_w.size);

    if (!user_ok)
        {
        memset(&g_conf_user_w, 0, sizeof(g_conf_user_w));
        g_conf_user_w.signature = CONF_USER_SIG;
        g_conf_user_w.size = sizeof(g_conf_user_w);
        g_conf_user_w.iface = DFLT_IFACE;
        g_conf_user_w.net_options = DFLT_NETOPT;
        }
    
    if (CONF_IFACE_INVALID == g_conf_user_w.iface)
        g_conf_user_w.iface = DFLT_IFACE;
    
    /* На LTR-CU-1 принудительно включить режим USB (отсутствует Ethernet) */
    if (FWINFO_DEV_MODEL_CU == fwinfo_get_dev_model())
        g_conf_user_w.iface = CONF_IFACE_USB;

    return fact_ok && user_ok;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int conf_save_fact                  /* Запись фабричной конфигурации */
    (
    const conf_fact_t* conf         /* записываемая структура */
    )
    {
    if (!conf) return 0;

    /* Проверить сигнатуру и длину */
    if ((CONF_FACT_SIG != conf->signature) || (sizeof(conf_fact_t) != conf->size))
        return 0;

    if (&g_conf_fact_w != conf)
        g_conf_fact_w = *conf;

    if (!f_init_done) f_eeprom_init();
    f_save_conf(CONF_FACT_EEADDR, (uint8_t*)&g_conf_fact_w, sizeof(g_conf_fact));
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int conf_save_user                  /* Запись пользовательской конфигурации */
    (
    const conf_user_t* conf         /* записываемая структура */
    )
    {
    if (!conf) return 0;

    /* Проверить сигнатуру и длину */
    if ((CONF_USER_SIG != conf->signature) || (sizeof(conf_user_t) != conf->size))
        return 0;

    /* Проверка совместимости параметров с устройством */
    if (FWINFO_DEV_MODEL_CU == fwinfo_get_dev_model())
        {
        if (conf->iface != CONF_IFACE_USB) return 0;
        }

    if (&g_conf_user_w != conf)
        g_conf_user_w = *conf;

    if (!f_init_done) f_eeprom_init();
    f_save_conf(CONF_USER_EEADDR, (uint8_t*)&g_conf_user_w, sizeof(g_conf_user));
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
