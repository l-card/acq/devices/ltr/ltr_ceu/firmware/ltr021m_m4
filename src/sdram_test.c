/*================================================================================================*
 * Обертка вокруг memtest для защиты от конфликтов
 * (с отключением всего, что может иметь буферы в SDRAM)
 *================================================================================================*/

#include "sdram_test.h"
#include "memtest.h"
#include "data.h"
#include "usb_bulk.h"
#include "lip.h"

/*================================================================================================*/
#if (SDRAM_TEST_MEMSIZE % SDRAM_TEST_BLKSIZE) || (SDRAM_TEST_BLKSIZE % 4)
#error "SDRAM_TEST_BLKSIZE must be 32-bit aligned and a divisor of SDRAM_TEST_MEMSIZE"
#endif
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static void f_wdt_reset(void)
    {
    lpc_wdt_reset();
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/* Quick power-on self test (before any access to SDRAM) */
uintptr_t sdram_test_post(void)
    {
    return (uintptr_t)memtest_quick_32bit(
        SDRAM_TEST_MEMPTR, SDRAM_TEST_BLKSIZE, SDRAM_TEST_MEMSIZE / SDRAM_TEST_BLKSIZE, f_wdt_reset);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/* Full runtime memory test (long) */
uintptr_t sdram_test_runtime(void)
    {
    uintptr_t ret_val;

    /* Cannot run with TCP/IP stack enabled (it may use SDRAM) */
    if (lip_state() != LIP_STATE_OFF)
        return SDRAM_TEST_RES_NOT_STARTED;

    /* Disable and reset everything that uses SDRAM (we are going to destroy all content) */
    data_stop();
    usbbulk_reset(); /* if we don't call hostio_worker, USB bulk will not use SDRAM */

    ret_val = (uintptr_t)memtest_march_c_16bit(
        SDRAM_TEST_MEMPTR, SDRAM_TEST_BLKSIZE, SDRAM_TEST_MEMSIZE / SDRAM_TEST_BLKSIZE, f_wdt_reset);

    /* Restart disabled functions */
    data_restart();

    return ret_val;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
