/*================================================================================================*
 * ��������� �������� ��������� � ����������� lboot
 *================================================================================================*/

#include "lbsupp.h"
#include "fwinfo.h"
//@@#include "fast_crc.h"
#include <string.h>

/*================================================================================================*/
/* ��������� � ����������� � �������� ��� bootloader'� */
__attribute__ ((used, section(".appl_info")))
static const t_app_info app_info =
    {
    .size = sizeof(t_app_info),
    .flags = LBOOT_APP_FLAGS_STABLE,
    .devname = FWINFO_DEVICE_NAME
    };

lbsupp_lboot_rq_t g_lbsupp_lboot_rq =
    {
    .hdr =
        {
        .size = offsetof(lbsupp_lboot_rq_t, crc) + sizeof(uint16_t),
        .bootmode = LBOOT_BOOTMODE_USB,
        .flags = LBOOT_REQ_FLAGS_RECOVERY_WR | LBOOT_REQ_FLAGS_ENABLE_NO_SIGN,
        .timeout = LBSUPP_TIMEOUT_MS,
        .reserv = { 0, 0 },
        .devinfo =
            {
            .devname = FWINFO_DEVICE_NAME,
            .soft_ver = FWINFO_FW_VERSION,
            .brd_revision = "",
            .serial = "NO_BOOTROM",   /* <-- ���� ���������� �������� ����� ��� lbsupp_init() */
            .brd_impl = "",
            .spec_info = ""
            }
        },
    .flags = 0
    };
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void lbsupp_init                    /* ��������� ��������� ������ (���� ���, �� ������ ������) */
    (void)
    {
#if 0 //@@
    if (LBOOT_INFO_SIGNATURE == LBSUPP_LBOOT_INFO.sign)
        {
        size_t len = LBSUPP_LBOOT_INFO.size - sizeof(uint16_t);
        const uint8_t* info_ptr = (uint8_t*)&LBSUPP_LBOOT_INFO;
        uint16_t crc = (uint16_t)(info_ptr[len] | ((uint16_t)info_ptr[len + 1] << 8));
        if (CRC16_Block8(0, info_ptr, len) == crc)
            {
            #define SERIAL_VAR g_lbsupp_lboot_rq.hdr.devinfo.serial
            strncpy(SERIAL_VAR, LBSUPP_LBOOT_INFO.serial, sizeof(SERIAL_VAR) - 1);
            SERIAL_VAR[sizeof(SERIAL_VAR) - 1] = '\0';
            #undef SERIAL_VAR
            }
        }
    g_lbsupp_lboot_rq.crc =
        CRC16_Block8(0, (uint8_t*)&g_lbsupp_lboot_rq, offsetof(lbsupp_lboot_rq_t, crc));
#endif
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
__attribute__ ((__noreturn__))
void lbsupp_jump_to_bootloader      /* ������� � bootloader � ��������� ������� */
    (void)
    {
#if 0 //@@
    #error "lboot jump has to be rewritten from LPC17xx to LPC43xx"

    const lbsupp_lboot_rq_t* request = &g_lbsupp_lboot_rq;
    uint32_t tmp;

    /* ��������� � �������� ��� ���������� */
    NVIC->ICER[0] = NVIC->ICER[0]; /* ��� ������ "1" � �������� ����������� ���������� */
    NVIC->ICER[1] = NVIC->ICER[1];
    NVIC->ICPR[0] = NVIC->ICPR[0]; /* ��� ������ "1" � �������� ���������� � ��������� pending */
    NVIC->ICPR[1] = NVIC->ICPR[1];
    SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;
    SCB->ICSR = SCB_ICSR_PENDSVCLR_Msk | SCB_ICSR_PENDSTCLR_Msk;

    /* �������� ��� ������ � ��������� ��������� */
    LPC_PINCON->PINSEL0 = 0; LPC_PINCON->PINSEL1 = 0; LPC_PINCON->PINSEL2 = 0;
    LPC_PINCON->PINSEL3 = 0; LPC_PINCON->PINSEL4 = 0; LPC_PINCON->PINSEL5 = 0;
    LPC_PINCON->PINSEL6 = 0; LPC_PINCON->PINSEL7 = 0; LPC_PINCON->PINSEL8 = 0;
    LPC_PINCON->PINSEL9 = 0; LPC_PINCON->PINSEL10 = 0;
    LPC_GPIO(0)->FIODIR = 0; LPC_GPIO(1)->FIODIR = 0; LPC_GPIO(2)->FIODIR = 0;
    LPC_GPIO(3)->FIODIR = 0; LPC_GPIO(4)->FIODIR = 0;

    /* ��������� ���������, ����� GPIO */
    LPC_SC->PCONP = LPC_SC_PCONP_PCGPIO_Msk;

    /* ��������� PLL � ������� ���������� �� ��������� */
    lpc_stop_pll0();
    lpc_stop_pll1();
    LPC_SC->CCLKCFG = 0; /* ��������� �������� 1 */
    LPC_SC->CLKSRCSEL = 0; /* ������� IRC ���������� */

    /* ���������� ������� �������� ���������� �� ������� ���������� */
    SCB->VTOR = 0;

    /* ��������� ���� � ��������� ���������� �� ������� ���������� ����������.
     * ���� ������� ������, �� ����������� ��� � ������ ������.
     *
     * ����������: ����� LBOOT_REQ_ADDR, ���� ���������� ������, ��������� � ����� ������ RAM.
     * ��� ����� ���� ���� ��� ������� ������ ���������. � ���������, ��������� *request �����
     * ������������ � ���. ������, ��������� ����������� ������������ �� ������ � ����� ���
     * ������������� �����, � ����� ��������� >= ������ ���������, ��� �� �������,
     * ������� request ����� ������������� ��� ������.
     */
    asm volatile
        (
        "   ldr sp, [%[boot_intvec]]        \n"     /* ���� �� ������ ������ �������� ����� */
        "   cbz %[len], 1f                  \n"
        "0:"                                        /* ����������� �� ����� (������� �� �����) */
        "   ldrb %[tmp], [%[src]], #1       \n"
        "   strb %[tmp], [%[dest]], #1      \n"
        "   subs %[len], %[len], #1         \n"
        "   bhi 0b                          \n"
        "1:"
        "   ldr pc, [%[boot_intvec], #4]"           /* ������� �� bootloader */
        : [tmp] "=&r" (tmp) /* "&" = �� ������������ ������� �� �������� ���������� */
        : [dest] "r" (LBOOT_REQ_ADDR), [src] "r" (request),
          [len] "l" ((request) ? request->hdr.size : 0),
          [boot_intvec] "r" (0)
        : "cc", "memory"
        );
#endif
    for (;;); /* avoid "noreturn function does return" error */
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
