/*================================================================================================*
 * Данные по протоколу TCP/IP (lip sockets)
 *================================================================================================*/

#include "tcpserver.h"
#include "lip.h"
#include <string.h>
#include "ctlfunc.h"

/*================================================================================================*/
#define TX_BLOCK_LEN            TCPSERVER_DATA_TX_BLOCK_LEN
#define TX_BUF_NBLK             TCPSERVER_DATA_TX_NBLK
#define TX_BUF_SIZE             (TX_BLOCK_LEN * TX_BUF_NBLK)

#define RX_BUF_SIZE             TCPSERVER_DATA_RX_BUF_SIZE
/*================================================================================================*/

/*================================================================================================*/
static int f_sock;
static int f_last_sock_state;

/* Примечания по алгоритму передачи - в usb_bulk.c, этот код почти полностью повторяет его. */

typedef uint8_t* bufptr_t;

static TCPSERVER_DATA_BUF_DECL uint8_t f_rx_buf[RX_BUF_SIZE];
static TCPSERVER_DATA_BUF_DECL uint8_t f_tx_buf[TX_BUF_SIZE];

/* переменные состояния передатчика */
static bufptr_t f_tx_rptr = f_tx_buf;   /* позиция, откуда читаются данные для отправки */
static bufptr_t f_tx_wptr = f_tx_buf;   /* позиция, куда пишутся данные в tx_buf */
static size_t f_tx_bytes_used;          /* количество занятых байт в буфере */
static size_t f_tx_bytes_committed; /* кол-во байт всего, поставленных на отправку */
static volatile size_t f_tx_bytes_finished; /* кол-во байт всего, посылка которых закончена */
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static inline bufptr_t f_ring_add(bufptr_t x, size_t delta_x, bufptr_t buf, size_t buf_len)
    {
    x += delta_x;
    if (x >= buf + buf_len) x -= buf_len;
    return x;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/* callback по завершению передачи */
#ifndef LIP_TCP_CB_DD_SEND_DONE
#error "LIP send callback must be enabled!"
#endif
void lsock_cb_dd_send_done(int sock_id, int dd_num, t_lip_tcp_dd* pdd)
    {
    if (LIP_TCP_DD_STATUS_DONE == pdd->status)
        f_tx_bytes_finished += TX_BLOCK_LEN;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_start_tx(int push)
    {
    /* Отправка заполненных блоков, а если push != 0 - и неполного */
#if 1
    /* отправить полные блоки */
    while (f_tx_bytes_used >= TX_BLOCK_LEN)
        {
        if (lsock_send_start(f_sock, f_tx_rptr, TX_BLOCK_LEN) < 0)
            return;
        f_tx_bytes_committed += TX_BLOCK_LEN;
        f_tx_rptr = f_ring_add(f_tx_rptr, TX_BLOCK_LEN, f_tx_buf, TX_BUF_SIZE);
        f_tx_bytes_used -= TX_BLOCK_LEN;
        }

    if (push && (f_tx_bytes_used > 0))
        { /* отправить и последний неполный блок */
        if (lsock_send_start(f_sock, f_tx_rptr, (int)f_tx_bytes_used) < 0)
            return;
        /* rp и bytes_committed всегда увеличивается на полный блок */
        f_tx_bytes_committed += TX_BLOCK_LEN;
        f_tx_rptr = f_ring_add(f_tx_rptr, TX_BLOCK_LEN, f_tx_buf, TX_BUF_SIZE);
        f_tx_bytes_used = 0;
        /* сдвинуть указатель wp, чтобы он не оказался "левее" rp */
        f_tx_wptr = f_tx_rptr;
        }
#else
    while (f_tx_bytes_used > 0)
        {
        size_t len = TX_BLOCK_LEN;
        if (f_tx_bytes_used < len)
            { /* неполный блок отправляется только если push != 0 */
            if (!push)
                break;
            len = f_tx_bytes_used;
            }
        if (lsock_send_start(f_sock, f_tx_rptr, (int)len) < 0)
            break;
        /* rp и bytes_committed всегда увеличивается на полный блок */
        f_tx_bytes_committed += TX_BLOCK_LEN;
        f_tx_rptr = f_ring_add(f_tx_rptr, TX_BLOCK_LEN, f_tx_buf, TX_BUF_SIZE);
        f_tx_bytes_used -= len;
        /* если блок был неполный (и тогда он последний), то сдвинуть указатель wp, 
           чтобы он не оказался "левее" rp */
        if (len < TX_BLOCK_LEN)
            f_tx_wptr = f_tx_rptr;
        }
#endif
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static int f_sockstate_monitor(void)
    {
    int st = lsock_state(f_sock);
    if (st != f_last_sock_state)
        {
        f_last_sock_state = st;
        switch (st)
            {
            case LIP_TCP_STATE_CLOSE_WAIT:
                lsock_shutdown(f_sock);
                break;
            case LIP_TCP_STATE_CLOSED:
                /* data connection closed */
                break;
            case LIP_TCP_STATE_ESTABLISHED:
                /* data connection open */
                ctlfunc_reset_fpga();
                break;
            }
        }
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_init(void)
    {
    /* LIP must have been already initialized */
    
    /* init socket */
    f_sock = lsock_init();
    lsock_set_recv_fifo(f_sock, f_rx_buf, sizeof(f_rx_buf));
    f_last_sock_state = lsock_state(f_sock);

    /* init tx */
    f_tx_rptr = f_tx_wptr = f_tx_buf;
    f_tx_bytes_used = 0;
    f_tx_bytes_finished = f_tx_bytes_committed = 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_sock_listen(void)
    {
    if (lsock_state(f_sock) != LIP_TCP_STATE_CLOSED)
        {
        lsock_abort(f_sock);
        tcpserver_data_init();
        }
    lsock_listen(f_sock, TCPSERVER_DATA_PORT);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_sock_shutdown(void)
    {
    lsock_shutdown(f_sock);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_reset(void)
    {
    /* Called by ctlfunc_reset_fpga, probably nothing to do here */
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t tcpserver_data_send_avail(void)
    {
    if (f_last_sock_state != LIP_TCP_STATE_ESTABLISHED) return 0;
    size_t unsent_bytes = f_tx_bytes_committed - f_tx_bytes_finished;
    return TX_BUF_SIZE - f_tx_bytes_used - unsent_bytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t tcpserver_data_send(const uint8_t* data, size_t len, int push)
    {
    if (len > tcpserver_data_send_avail())
        return 0;

    bufptr_t new_wptr = f_ring_add(f_tx_wptr, len, f_tx_buf, TX_BUF_SIZE);

    if (new_wptr >= f_tx_wptr)
        {
        memcpy((uint8_t*)f_tx_wptr, data, len);
        }
    else
        {
        size_t len1 = (size_t)(f_tx_buf + TX_BUF_SIZE - f_tx_wptr);
        memcpy((uint8_t*)f_tx_wptr, data, len1);
        memcpy((uint8_t*)f_tx_buf, data + len1, len - len1);
        }

    f_tx_wptr = new_wptr;
    f_tx_bytes_used += len;
    f_start_tx(push);
    return len;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t tcpserver_data_recv(uint8_t* data, size_t len)
    {
    int nbytes = lsock_recv(f_sock, data, len);
    if (nbytes < 0) nbytes = 0;
    return (size_t)nbytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_worker(void)
    {
    f_sockstate_monitor();

    if (LIP_TCP_STATE_ESTABLISHED == f_last_sock_state)
        {
        /* Отправка данных. Неполный блок посылается, если очередь пуста. */
        f_start_tx(0 == lsock_send_req_cnt(f_sock));
        }
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
