#include "tests.h"
#include "chip.h"
#include "fpga.h"
#include "fpga_sram_buf.h"
#include "lprintf.h"

#include <stdlib.h>



static uint16_t f_adc_cntr;
static uint16_t f_dac_cntr;
static uint16_t f_dac_rdy;
static uint16_t f_wr_val;
static uint16_t f_rd_val;

static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state);

const t_test_descr test_descr_sram_buf_ring = {
    E502_TEST_SRAM_BUF_RING,
    f_test_start,
    f_test_proc,
    NULL
};



static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state) {

    fpga_reg_write(E502_REGS_ARM_DMA, E502_REGBIT_ARM_DMA_ADC_BUF_CLR_Msk |
                   E502_REGBIT_ARM_DMA_DAC_BUF_CLR_Msk);


    fpga_reg_write(E502_REGS_ARM_DMA, E502_REGBIT_ARM_DMA_RING_MODE_Msk);


    f_adc_cntr = *FPGA_SRAM_DMA_ADC_CNTR;
    f_dac_cntr = *FPGA_SRAM_DMA_DAC_CNTR;

    f_dac_rdy = FPGA_SRAM_DMA_BUF_RING_SIZE;

    f_wr_val = f_rd_val = 0;
    return 0;
}



static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state) {
    for (int i=0; i < 1024; i++) {

        if (f_dac_rdy) {
            *FPGA_SRAM_DMA_DAC_BUF = f_wr_val&0xFFFF;
            f_wr_val++;
            *FPGA_SRAM_DMA_DAC_BUF = f_wr_val&0xFFFF;// (s_wr_val>>16) & 0xFFFF;
            f_wr_val++;
            f_dac_rdy--;
        }

        volatile uint16_t cur_adc_cntr;
        cur_adc_cntr = *FPGA_SRAM_DMA_ADC_CNTR;


        if (f_adc_cntr!=cur_adc_cntr) {
            uint16_t adc_val_l = *FPGA_SRAM_DMA_ADC_BUF;
            uint16_t adc_val_h = *FPGA_SRAM_DMA_ADC_BUF;
            f_adc_cntr = (f_adc_cntr+1) & 0xFFF;
            f_dac_rdy++;

            uint16_t adc_exp_val_h = f_rd_val++;
            uint16_t adc_exp_val_l = f_rd_val++;

            if ((adc_exp_val_h != adc_val_h) || (adc_exp_val_l!=adc_val_l)) {
                lprintf("adc rd error: read 0x%04X 0x%04X, exp 0x%04X 0x%04X\n", adc_val_h, adc_val_l, adc_exp_val_h, adc_exp_val_l);
                state->last_rd = (adc_val_h<<16) | adc_val_l;
                state->last_wr = (adc_exp_val_h<<16) | adc_exp_val_l;
            }

            if (!(f_adc_cntr)) {
                state->cntr++;
                lprintf(".");
            }
        }
    }
    return 0;
}
