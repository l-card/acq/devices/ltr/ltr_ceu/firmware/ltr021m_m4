/* Кольцевой тест USB. Модуль принимает данные по USB в буфер и по возможности
 * ставит их обратно на передачу, не анализируя их (подразумевается, что
 * проверка данных идет на стороне ПК */

#include "tests.h"
#include "lusb.h"
#include "e502_cm4_defs.h"
#include "e502_proc.h"

static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state);

static uint32_t f_rx_pos;
static uint32_t f_tx_pos;
static uint32_t f_wrds_tx_rdy;
static uint32_t f_rx_dd_in_progr;


static uint32_t f_buf_cnt;
static uint32_t f_buf_size;




const t_test_descr test_descr_usb_ring = {
    E502_TEST_USB_RING,
    f_test_start,
    f_test_proc,
    f_test_stop
};


static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    f_buf_cnt = LUSB_DMA_EP_DESCR_CNT;
    f_buf_size = STREAM_IN_BUF_SIZE/f_buf_cnt;
    if (f_buf_size > LUSB_DD_DATA_SIZE_MAX/sizeof(in_buf[0]))
        f_buf_size = LUSB_DD_DATA_SIZE_MAX/sizeof(in_buf[0]);
    f_rx_pos = 0;
    f_tx_pos = 0;

    f_wrds_tx_rdy = 0;
    f_rx_dd_in_progr = 0;


    return err;

}

static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    uint32_t tx_dds =  lusb_ep_get_dd_in_progress(LUSB_TX_EP_IND);
    uint32_t rx_dds = lusb_ep_get_dd_in_progress(LUSB_RX_EP_IND);
    uint32_t rx_buf_rdy;



    while (rx_dds < f_rx_dd_in_progr) {
        f_wrds_tx_rdy+=f_buf_size;
        f_rx_dd_in_progr--;
    }

    rx_buf_rdy = f_buf_cnt - rx_dds- tx_dds - f_wrds_tx_rdy/f_buf_size;


    while (rx_buf_rdy > 0) {
        int usb_err = lusb_ep_add_dd(LUSB_RX_EP_IND, &in_buf[f_rx_pos], f_buf_size*sizeof(in_buf[0]), 0);
        if (usb_err == LUSB_ERR_SUCCESS) {
            f_rx_pos+=f_buf_size;
            if (f_rx_pos == f_buf_cnt*f_buf_size)
                f_rx_pos = 0;
            f_rx_dd_in_progr++;
            rx_buf_rdy--;
        }
    }



    if ((lusb_ep_get_dd_in_progress(LUSB_TX_EP_IND) < LUSB_DMA_EP_DESCR_CNT)
           && (f_wrds_tx_rdy >= f_buf_size)) {

        int usb_err = lusb_ep_add_dd(LUSB_TX_EP_IND, &in_buf[f_tx_pos], f_buf_size*sizeof(in_buf[0]), 0);
        if (usb_err == LUSB_ERR_SUCCESS) {
            f_wrds_tx_rdy-=f_buf_size;

            f_tx_pos+=f_buf_size;
            if (f_tx_pos == f_buf_cnt*f_buf_size)
                f_tx_pos = 0;
        }
    }

    return err;
}

static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;

    lusb_ep_clear(LUSB_TX_EP_IND);
    lusb_ep_clear(LUSB_RX_EP_IND);

    return err;
}








