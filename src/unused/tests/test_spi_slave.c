/* Тест SPI интерфейса в slave-режиме. Используется только со специальной версией
    прошивки ПЛИС */

#include "tests.h"
#include "e502_cm4_defs.h"
#include "chip.h"
#include "lprintf.h"
#include "timer.h"


#define PIN_FPGA_SPI_MST_MODE    LPC_PIN_PB_1_GPIO5_21

#define TEST_CNTR_MODULE   16384


static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state);


const t_test_descr test_descr_spi_slave = {
    E502_TEST_SPI_SLAVE,
    f_test_start,
    f_test_proc,
    f_test_stop
};


static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    uint16_t f_test_cntr;
    uint32_t f_err_cnt;
    t_timer f_tmr;

    LPC_PIN_CONFIG(LPC_PIN_P3_3_SSP0_SCK,  1, LPC_PIN_FAST | LPC_PIN_NOPULL);
    //LPC_PIN_CONFIG(LPC_PIN_P3_6_SSP0_MISO, 1, LPC_PIN_FAST | LPC_PIN_NOPULL);
    LPC_PIN_CONFIG(LPC_PIN_P3_7_SSP0_MOSI, 1, LPC_PIN_FAST | LPC_PIN_NOPULL);
    LPC_PIN_CONFIG(LPC_PIN_P3_8_SSP0_SSEL, 1, LPC_PIN_FAST | LPC_PIN_NOPULL);


    f_test_cntr = 1;



    timer_set(&f_tmr, CLOCK_CONF_SECOND);

    LPC_SSP0->CR1 = 0;
    LPC_SSP0->CR0 = SSP_CR0_DSS(14) | SSP_CR0_FRF_TI;
    LPC_SSP0->CR1 = SSP_CR1_SSP_EN | SSP_CR1_SLAVE_EN | SSP_CR1_SO_DISABLE;

    LPC_PIN_CONFIG(PIN_FPGA_SPI_MST_MODE, 0, LPC_PIN_PULLUP);
    LPC_PIN_DIR_OUT(PIN_FPGA_SPI_MST_MODE);
    LPC_PIN_OUT(PIN_FPGA_SPI_MST_MODE, 0);



    for (;;) {
        if (LPC_SSP0->SR & SSP_STAT_RNE) {
            uint16_t cntr = LPC_SSP0->DR;
            if (cntr != f_test_cntr) {
                lprintf("cntr error: exp=0x%04X rcv=0x%04X\n",
                        f_test_cntr, cntr);
                f_test_cntr = cntr;
                f_err_cnt++;
            }

            if (++f_test_cntr==TEST_CNTR_MODULE) {
                lprintf(".");
                f_test_cntr = 0;
            }
        }
    }

    return err;

}

static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state) {
   return 0;
}

static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state) {
    LPC_PIN_OUT(PIN_FPGA_SPI_MST_MODE, 1);

    return 0;
}








