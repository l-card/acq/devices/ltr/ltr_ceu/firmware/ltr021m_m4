/* Тест DMA на передачу из устройства в ПК. Модуль генерирует 32-битный
   счетчик в буфере и ставит его на передачу с максимально возможной скоростью.
   Подразумевается, что проверка счетчика осуществляется в ПК */
#include "tests.h"
#include "lusb.h"
#include "e502_cm4_defs.h"
#include "e502_proc.h"

static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state);

static uint32_t f_put_pos;
static uint32_t f_put_cntr;
static uint32_t f_buf_cnt;
static uint32_t f_buf_size;




const t_test_descr test_descr_usb_tx_cntr = {
    E502_TEST_USB_TX_CNTR,
    f_test_start,
    f_test_proc,
    f_test_stop
};


static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    f_buf_cnt = LUSB_DMA_EP_DESCR_CNT;
    f_buf_size = STREAM_IN_BUF_SIZE/f_buf_cnt;
    if (f_buf_size > LUSB_DD_DATA_SIZE_MAX/sizeof(in_buf[0]))
        f_buf_size = LUSB_DD_DATA_SIZE_MAX/sizeof(in_buf[0]);
    f_put_pos = 0;
    f_put_cntr = 0;

    return err;

}

static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;

    if (lusb_ep_get_dd_in_progress(LUSB_TX_EP_IND) < LUSB_DMA_EP_DESCR_CNT) {
        uint32_t cur_pos = f_put_pos;
        int usb_err;

        for (uint32_t i =0 ; i < f_buf_size; i++) {
            in_buf[cur_pos++] = f_put_cntr++;
        }

        usb_err = lusb_ep_add_dd(LUSB_TX_EP_IND, &in_buf[f_put_pos], f_buf_size*sizeof(in_buf[0]), 0);
        if (usb_err == LUSB_ERR_SUCCESS) {
            f_put_pos = cur_pos == f_buf_cnt*f_buf_size ? 0 : cur_pos;
        }
    }

    return err;
}

static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;

    lusb_ep_clear(LUSB_TX_EP_IND);

    return err;
}







