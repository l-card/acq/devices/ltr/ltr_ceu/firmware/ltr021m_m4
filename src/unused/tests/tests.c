#include "tests.h"
#include <stdlib.h>
#include <string.h>
#include "lprintf.h"

extern t_test_descr test_descr_sram_buf_ring;
extern t_test_descr test_descr_sram_sdram_ring_dma;
extern t_test_descr test_descr_usb_tx_cntr;
extern t_test_descr test_descr_usb_ring;
extern t_test_descr test_descr_spi_slave;

static const t_test_descr* f_test_descrs[] = {
    &test_descr_sram_buf_ring,
    &test_descr_sram_sdram_ring_dma,
    &test_descr_usb_tx_cntr,
    &test_descr_usb_ring,
    &test_descr_spi_slave
};

static t_e502_cm4_test_state f_state;
static unsigned f_last_flags;
static const t_test_descr* f_cur_test_descr = NULL;




t_e502_cm4_errs test_start(t_test_number test, unsigned flags) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    f_cur_test_descr = NULL;



    if (test_is_running())
        err = E502_CM4_ERR_TEST_ALREADY_RUNNING;

    if (!err) {
        for (size_t i=0; (f_cur_test_descr==NULL) &&
             (i < sizeof(f_test_descrs)/sizeof(f_test_descrs[0])); i++) {
            if (f_test_descrs[i]->num==test) {
                f_cur_test_descr = f_test_descrs[i];
            }
        }

        if (f_cur_test_descr==0)
            err = E502_CM4_ERR_TEST_INVALID_NUM;
    }

    if (!err) {
        f_last_flags = flags;

        memset(&f_state, 0, sizeof(f_state));
        f_state.test = test;
        err = f_cur_test_descr->init(&f_state);
        if (!err) {
            f_state.run = 1;

        }

        lprintf("test started %d (res = %d)\n", test, err);
    }

    return err;

}


t_test_number test_is_running(void) {
    return f_state.run ? f_state.test : E502_TEST_NONE;
}


t_e502_cm4_errs test_stop() {
    t_e502_cm4_errs err = 0;
    if (!test_is_running()) {
        err = E502_CM4_ERR_TEST_NOT_RUNNING;
    } else {
        f_state.run = 0;
        if (f_cur_test_descr->stop!=NULL) {
            err = f_cur_test_descr->stop(&f_state);
            lprintf("test stopped (res = %d)\n", err);
        }
    }

    return err;
}


t_e502_cm4_errs test_restart_last() {
    return test_start(f_cur_test_descr->num, f_last_flags);
}


t_e502_cm4_errs test_pull() {
    return f_cur_test_descr->progr(&f_state);
}


t_e502_cm4_test_state *test_state() {
    return &f_state;
}
