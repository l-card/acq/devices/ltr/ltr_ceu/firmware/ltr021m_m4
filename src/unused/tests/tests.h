#ifndef TESTS_H
#define TESTS_H

#include <stdint.h>
#include "e502_cm4_defs.h"

typedef void (*t_proc_func)(void);
typedef t_e502_cm4_errs (*t_test_init)(t_e502_cm4_test_state *state);
typedef t_e502_cm4_errs (*t_test_progr)(t_e502_cm4_test_state *state);
typedef t_e502_cm4_errs (*t_test_stop)(t_e502_cm4_test_state *state);

typedef struct {
    t_test_number num;
    t_test_init  init;
    t_test_progr progr;
    t_test_stop  stop;
} t_test_descr;


t_e502_cm4_errs test_start(t_test_number test, unsigned flags);
t_e502_cm4_errs test_stop(void);
t_e502_cm4_errs test_restart_last(void);
t_e502_cm4_errs test_pull(void);
t_test_number test_is_running(void);
t_e502_cm4_test_state *test_state(void);










#endif // TESTS_H
