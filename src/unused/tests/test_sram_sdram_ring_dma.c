#include "tests.h"
#include "chip.h"
#include "fpga.h"
#include "fpga_sram_buf.h"
#include "lprintf.h"

#include <stdlib.h>

#include "e502_proc.h"

//#define SPI_TEST_WR
//#define SPI_TEST_RD

//#define TEST_DONT_CHECK_ADC_CNTR

static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state);

static uint16_t f_dac_rdy_size;
static uint16_t f_in_cntr;
static uint32_t f_in_buf_pos;
static uint32_t f_out_buf_pos;
static uint32_t f_put_val;
static uint32_t f_get_val;
static uint32_t f_in_proc_pos;
static uint32_t f_in_total_cnt;


const t_test_descr test_descr_sram_sdram_ring_dma = {
    E502_TEST_SRAM_SDRAM_RING_DMA,
    f_test_start,
    f_test_proc,
    f_test_stop
};


#define DMA_TRANSF_MAX_SIZE 2048

#define TEST_INC_VAL  1
#define TEST_INIT_VAL 0 //0x55AA55AA

#define PROC_IN_DMA_CHANNEL   6
#define PROC_OUT_DMA_CHANNEL  7


void stream_out_wr_start(const uint32_t *buf, uint32_t buf_size, uint32_t* ppos, uint16_t size) {
    static DMA_TransferDescriptor_t lli;
    GPDMA_CH_T *out_ch = &LPC_GPDMA->CH[PROC_OUT_DMA_CHANNEL];
    uint32_t put_size = size;
    uint32_t cur_pos = ppos == NULL ? 0 : *ppos;


    if (buf_size && ((cur_pos + put_size) > buf_size)) {
        put_size = buf_size - cur_pos;
    }

    if (put_size > DMA_TRANSF_MAX_SIZE/2)
        put_size = DMA_TRANSF_MAX_SIZE/2;

    out_ch->SRCADDR = (uint32_t)&buf[cur_pos];
    out_ch->DESTADDR = (uint32_t)FPGA_SRAM_DMA_DAC_BUF;
    out_ch->CONTROL = GPDMA_DMACCxControl_TransferSize(put_size*2) |
        GPDMA_DMACCxControl_SWidth(GPDMA_WIDTH_HALFWORD) |
        GPDMA_DMACCxControl_DWidth(GPDMA_WIDTH_HALFWORD) |
        GPDMA_DMACCxControl_SI;

    if (put_size == size) {
        out_ch->LLI = 0;
    } else {
        out_ch->LLI = (uint32_t)&lli;
        cur_pos += put_size;
        if (cur_pos==buf_size)
            cur_pos = 0;
        put_size = size-put_size;

        lli.src = (uint32_t)&buf[cur_pos];
        lli.dst = (uint32_t)FPGA_SRAM_DMA_DAC_BUF;
        lli.ctrl = GPDMA_DMACCxControl_TransferSize(put_size*2) |
                GPDMA_DMACCxControl_SWidth(GPDMA_WIDTH_HALFWORD) |
                GPDMA_DMACCxControl_DWidth(GPDMA_WIDTH_HALFWORD) |
                GPDMA_DMACCxControl_SI;
        lli.lli = 0;
    }

    if (ppos!=NULL) {
        cur_pos += put_size;
        if (cur_pos==buf_size)
            cur_pos = 0;
        *ppos = cur_pos;
    }

    out_ch->CONFIG = GPDMA_DMACCxConfig_TransferType(GPDMA_TRANSFERTYPE_M2M_CONTROLLER_DMA);
    out_ch->CONFIG |= GPDMA_DMACCxConfig_E;
}

void stream_in_rd_start(const uint32_t *buf, uint32_t buf_size, uint32_t* ppos, uint16_t size) {
    static DMA_TransferDescriptor_t lli;
    uint32_t cur_size = size;
    uint32_t cur_pos = ppos == NULL ? 0 : *ppos;


    if (buf_size && ((cur_pos + cur_size) > buf_size)) {
        cur_size = buf_size - cur_pos;
    }

    if (cur_size > DMA_TRANSF_MAX_SIZE/2)
        cur_size = DMA_TRANSF_MAX_SIZE/2;


    GPDMA_CH_T *in_ch = &LPC_GPDMA->CH[PROC_IN_DMA_CHANNEL];
    in_ch->SRCADDR = (uint32_t)FPGA_SRAM_DMA_ADC_BUF;
    in_ch->DESTADDR = (uint32_t)&buf[cur_pos];


    in_ch->CONTROL = GPDMA_DMACCxControl_TransferSize(cur_size*2) |
            GPDMA_DMACCxControl_SWidth(GPDMA_WIDTH_HALFWORD) |
            GPDMA_DMACCxControl_DWidth(GPDMA_WIDTH_HALFWORD) |
            GPDMA_DMACCxControl_DI;

    if (cur_size == size) {
        in_ch->LLI = 0;
    } else {
        in_ch->LLI = (uint32_t)&lli;
        cur_pos += cur_size;
        if (cur_pos==buf_size)
            cur_pos = 0;
        cur_size = size-cur_size;

        lli.src = (uint32_t)FPGA_SRAM_DMA_ADC_BUF;
        lli.dst = (uint32_t)&buf[cur_pos];
        lli.ctrl = GPDMA_DMACCxControl_TransferSize(cur_size*2) |
                GPDMA_DMACCxControl_SWidth(GPDMA_WIDTH_HALFWORD) |
                GPDMA_DMACCxControl_DWidth(GPDMA_WIDTH_HALFWORD) |
                GPDMA_DMACCxControl_DI;
        lli.lli = 0;
    }

    if (ppos!=NULL) {
        cur_pos += cur_size;
        if (cur_pos==buf_size)
            cur_pos = 0;
        *ppos = cur_pos;
    }

    in_ch->CONFIG = GPDMA_DMACCxConfig_TransferType(GPDMA_TRANSFERTYPE_M2M_CONTROLLER_DMA);
    in_ch->CONFIG |= GPDMA_DMACCxConfig_E;
}



int stream_out_active(void) {
    return LPC_GPDMA->CH[PROC_OUT_DMA_CHANNEL].CONFIG & GPDMA_DMACCxConfig_E;
}

int stream_in_active(void) {
    return LPC_GPDMA->CH[PROC_IN_DMA_CHANNEL].CONFIG & GPDMA_DMACCxConfig_E;
}

void stream_out_disable(void) {
    LPC_GPDMA->CH[PROC_OUT_DMA_CHANNEL].CONFIG &= ~GPDMA_DMACCxConfig_E;
}

void stream_in_disable(void) {
    LPC_GPDMA->CH[PROC_IN_DMA_CHANNEL].CONFIG &= ~GPDMA_DMACCxConfig_E;
}


static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state) {

    fpga_reg_write(E502_REGS_ARM_DMA, E502_REGBIT_ARM_DMA_ADC_BUF_CLR_Msk |
                   E502_REGBIT_ARM_DMA_DAC_BUF_CLR_Msk);
#if defined SPI_TEST_WR || defined SPI_TEST_RD
    fpga_reg_write(E502_REGS_ARM_DMA, 0);
#else
    fpga_reg_write(E502_REGS_ARM_DMA, E502_REGBIT_ARM_DMA_RING_MODE_Msk);
#endif

    f_dac_rdy_size = FPGA_SRAM_DMA_BUF_RING_SIZE;
    f_in_cntr = 0;
    f_in_buf_pos = f_in_proc_pos = f_out_buf_pos = 0;
    f_put_val = f_get_val = TEST_INIT_VAL;
    f_in_total_cnt=0;

    return E502_CM4_ERR_OK;
}

static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state) {
    stream_out_disable();
    stream_in_disable();
    return E502_CM4_ERR_OK;
}


static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state) {

#ifdef SPI_TEST_WR
    while (f_dac_rdy_size) {
        uint32_t val = ((f_put_val>>16) & 0xFFFF) |
                ((f_put_val&0xFFFF)<<16);
        if (fpga_reg_write(0x101, val)==0) {
            f_put_val+=TEST_INC_VAL;
            f_dac_rdy_size--;
        } else {
            break;
        }
    }
#endif


#ifndef SPI_TEST_WR
    if (f_dac_rdy_size && !stream_out_active()) {
        uint16_t snd_size = f_dac_rdy_size;
        uint32_t pos = f_out_buf_pos;


        for (uint32_t i=0; i < snd_size; i++) {
            out_buf[pos++] = f_put_val++;
            if (pos==STREAM_OUT_BUF_SIZE) {
                pos=0;                
            }
        }

        //lprintf("start write %d\n", snd_size);
        stream_out_wr_start(out_buf, STREAM_OUT_BUF_SIZE, &f_out_buf_pos, snd_size);
        f_dac_rdy_size -= snd_size;
    }
#endif

#ifdef SPI_TEST_RD
    if (!f_dac_rdy_size && !stream_out_active()) {
        for (uint32_t i=0; i < FPGA_SRAM_DMA_BUF_RING_SIZE; i++) {
            uint32_t val;
            if (fpga_reg_read(0x101, &val)==0) {
                if (val != f_get_val) {
                    lprintf("!invalid value: wr 0x%08X, rd 0x%08X\n", f_get_val, val);
                    //for (;;) {}
                    //break;
                }
                if ((f_get_val&0xFFFF)==0xFFFF)
                    lprintf(".");
                    //lprintf("ok! rd 0x%08X, wr 0x%08X\n", val , f_get_val);

                f_get_val++;
                f_dac_rdy_size++;
            } else {
                break;
            }
        }
    }
#endif

#ifndef SPI_TEST_RD
    if (!stream_in_active()) {
        uint16_t cur_in_cntr;

        if (f_in_proc_pos!=f_in_buf_pos) {
            while(f_in_proc_pos!=f_in_buf_pos) {
                uint32_t tst_val;
                uint32_t val = in_buf[f_in_proc_pos++];
                val = ((val >> 16) & 0xFFFF) | ((val&0xFFFF) << 16);

                f_in_total_cnt++;

#ifdef TEST_CNTR_16BIT
                tst_val = (f_get_val++&0xFFFF)<<16;
                tst_val |= f_get_val++ & 0xFFFF;

#else
                tst_val = f_get_val;
                f_get_val+=TEST_INC_VAL;
#endif

                if (val != tst_val) {
                    lprintf("\n!invalid value: wr 0x%08X, rd 0x%08X (%d, %d), total 0x%08X\n", tst_val, val,
                            (int)!((val&0xFFFF0000) == (tst_val & 0xFFFF0000)),
                            (int)!((val&0xFFFF)==(tst_val&0xFFFF)),
                            f_in_total_cnt);
                    //for (;;) {}
                }

                f_dac_rdy_size++;

                if (f_in_proc_pos==STREAM_IN_BUF_SIZE) {
                    lprintf(".");
                    f_in_proc_pos=0;
                }
            }
        }

#ifndef TEST_DONT_CHECK_ADC_CNTR
        cur_in_cntr = *FPGA_SRAM_DMA_ADC_CNTR;
        if (cur_in_cntr!=f_in_cntr) {
#else
        if (f_dac_rdy_size==0) {
#endif

#ifndef TEST_DONT_CHECK_ADC_CNTR
            uint16_t rcv_size = (cur_in_cntr - f_in_cntr) & FPGA_SRAM_BUF_CNTR_MSK;
#else
            uint16_t rcv_size = FPGA_SRAM_DMA_BUF_RING_SIZE;
#endif

            if (rcv_size != 0) {// == FPGA_SRAM_DMA_BUF_RING_SIZE) {
                //lprintf("adc cntr = 0x%04X\n", cur_in_cntr);
                f_in_proc_pos = f_in_buf_pos;
                //lprintf("start read %d\n", rcv_size);
                stream_in_rd_start(in_buf, STREAM_IN_BUF_SIZE, &f_in_buf_pos, rcv_size);
#ifndef TEST_DONT_CHECK_ADC_CNTR
                f_in_cntr = cur_in_cntr;
#endif
            }
        }
    }
#endif


    return 0;
}
