#include "memtest.h"

/* Walking 0 memory test */
int mem_test_walking0(t_memtest_state *test_state) {
    int i = 0;
    uint32_t fbytes = test_state->bytes/4, *addr = test_state->start_addr;

    /* Must be 32-bit algined */
    if ((((uint32_t) addr & 0x3) != 0) || ((fbytes & 0x3) != 0)) {
        return -1;
    }

    /* Write walking 0 pattern */
    while (fbytes > 0) {
        *addr = ~(1U << i);

        addr++;
        fbytes--;
        i++;
        if (i >= 32) {
            i = 0;
        }
    }

    /* Verify walking 0 pattern */
    i = 0;
    fbytes = test_state->bytes/4;
    addr = test_state->start_addr;
    while (fbytes > 0) {
        if (*addr != ~(1U << i)) {
            test_state->fail_addr = addr;
            test_state->is_val = *addr;
            test_state->ex_val = ~(1U << i);
            return -2;
        }

        addr++;
        fbytes--;
        i++;
        if (i >= 32) {
            i = 0;
        }
    }

    return 0;
}
