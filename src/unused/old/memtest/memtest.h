#ifndef MEMTEST_H
#define MEMTEST_H

#include <stdint.h>

typedef struct {
    uint32_t *start_addr;   /*!< Starting address for memory test */
    uint32_t bytes;         /*!< Size in bytes for memory test */
    uint32_t *fail_addr;    /*!< Failed address of test (returned only if failed) */
    uint32_t is_val;        /*!< Failed value of test (returned only if failed) */
    uint32_t ex_val;        /*!< Expected value of test (returned only if failed) */
} t_memtest_state;


int mem_test_walking0(t_memtest_state *test_state);

#endif // MEMTEST_H
