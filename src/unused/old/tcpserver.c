/*================================================================================================*
 * Команды и данные по протоколу TCP/IP (lip sockets)
 *================================================================================================*/

//@@ add timeout on first request (before init)

#include "tcpserver.h"
#include "lip.h"
#include "ctlfunc.h"
#include "ledblink.h"
#include <string.h>

/*================================================================================================*/
#define N_CMD_CONN              TCPSERVER_CMD_NCONN
#define CMD_MAX_DATA            512

#define TX_BLOCK_LEN            LIP_TCP_MAX_SEND_SIZE
#define TX_BUF_SIZE             ALIGN_DOWN(TCPSERVER_DATA_TX_BUF_LEN, TX_BLOCK_LEN)
#define TX_BUF_NBLK             (TX_BUF_SIZE / TX_BLOCK_LEN)

#define RX_BLOCK_LEN            LIP_TCP_RCV_MSS
#define RX_BUF_SIZE             ALIGN_DOWN(TCPSERVER_DATA_RX_BUF_LEN, RX_BLOCK_LEN)
#define RX_BUF_NBLK             (RX_BUF_SIZE / RX_BLOCK_LEN)
    
#define LTR_CMD_SIG             0x314C5443
#define LTR_TCP_PROTO_VER_SIG   0xD91D3B45
#define LTR_TCP_PROTO_VER_HI    1
#define LTR_TCP_PROTO_VER_LO    1
enum
    {
    LTR_CMD_CODE_NOP = 0, LTR_CMD_CODE_GETNAME, LTR_CMD_CODE_GETARRAY, LTR_CMD_CODE_PUTARRAY,
    LTR_CMD_CODE_INIT = 0x80000000UL
    };
enum
    {
    LTR_RESULT_OK = 0, LTR_RESULT_ERR = 1
    };

#define IP_GETARRAY_IS_PRIMARY    (CTLFUNC_SEL_AVR_DM + 0x10070)
#define IP_PUTARRAY_FORCE_PRIMARY (CTLFUNC_SEL_AVR_DM + 0x10072)

enum /* values for f_cmd[].dir */
    {
    CMD_DIR_NONE,   /* not connected */
    CMD_DIR_IN,     /* receiving request */
    CMD_DIR_OUT     /* sending reply */
    };

/* Буфер для приема команд и посылки ответов (по одному на соединение) */
#define CMD_REQUEST_HEADER_SIZE     offsetof(struct st_cmd_request, data)
#define CMD_REPLY_HEADER_SIZE       offsetof(struct st_cmd_reply, data)
typedef union
    {
    struct st_cmd_request
        {
        uint32_t sig;
        uint32_t cmd_code;
        uint32_t param;
        uint32_t data_len;
        uint32_t reply_data_len;
        uint8_t data[CMD_MAX_DATA];
        }
        request;
    struct st_cmd_reply
        {
        uint32_t sig;
        uint32_t result_code;
        uint32_t data_len;
        uint8_t data[CMD_MAX_DATA];
        }
        reply;
    uint8_t u8[sizeof(struct st_cmd_request)];
    }
    cmd_buf_t;

typedef struct
    {
    int sock;
    int last_sock_state;
    int dir;
    size_t xfer_pos;        /* current offset into request/reply */
    size_t xfer_len;        /* length of request/reply */
    cmd_buf_t* buf;
    }
    cmd_ctl_t;

typedef struct
    {
    int sock;
    int last_sock_state;
    }
    data_ctl_t;
/*================================================================================================*/

/*================================================================================================*/
static const struct __attribute__ ((packed))
    {
    uint32_t sig;
    uint8_t hi, lo;
    }
    tcp_protocol_id =
    { .sig = LTR_TCP_PROTO_VER_SIG, .hi = LTR_TCP_PROTO_VER_HI, .lo = LTR_TCP_PROTO_VER_LO };

static cmd_ctl_t* f_primary_conn_ptr = NULL;
static cmd_ctl_t f_cmd[N_CMD_CONN];
static cmd_buf_t f_cmd_buf[N_CMD_CONN];

static data_ctl_t f_data;

static TCPSERVER_BUF_DECL uint8_t f_sockbuf_cmd_rx[N_CMD_CONN][TCPSERVER_CMD_RX_BUF_LEN];
static TCPSERVER_BUF_DECL uint8_t f_sockbuf_cmd_tx[N_CMD_CONN][TCPSERVER_CMD_TX_BUF_LEN];

static TCPSERVER_BUF_DECL uint8_t f_sockbuf_data_rx[TCPSERVER_DATA_RX_BUF_LEN];
static TCPSERVER_BUF_DECL uint8_t f_sockbuf_data_tx[TCPSERVER_DATA_TX_BUF_LEN];
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static void f_datasock_init(void)
    {
    f_data.sock = lsock_init();
    if (f_data.sock < 0) return;
    lsock_set_recv_fifo(f_data.sock, f_sockbuf_data_rx, sizeof(f_sockbuf_data_rx));
    lsock_set_send_fifo(f_data.sock, f_sockbuf_data_tx, sizeof(f_sockbuf_data_tx));
    f_data.last_sock_state = lsock_state(f_data.sock);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_datasock_reopen(void)
    {
    if (lsock_state(f_data.sock) != LIP_TCP_STATE_CLOSED)
        {
        lsock_abort(f_data.sock);
        f_datasock_init();
        }
    lsock_listen(f_data.sock, TCPSERVER_DATA_PORT);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_data_stchg(void)
    {
    if (LIP_TCP_STATE_ESTABLISHED == f_data.last_sock_state)
        { /* data connection open */
        ctlfunc_reset_fpga();
        }
    else
        { /* data connection closed */
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_cmd_stchg(cmd_ctl_t* cc)
    {
    if (LIP_TCP_STATE_ESTABLISHED == cc->last_sock_state)
        { /* command connection open */
        if (!f_primary_conn_ptr) f_primary_conn_ptr = cc;
        cc->dir = CMD_DIR_IN;
        cc->xfer_pos = 0;
        cc->xfer_len = CMD_REQUEST_HEADER_SIZE;
        }
    else
        { /* command connection closed */
        if (f_primary_conn_ptr == cc)
            {
            lsock_shutdown(f_data.sock);
            f_primary_conn_ptr = NULL;
            }
        cc->dir = CMD_DIR_NONE;
        /* listen for another connection */
        lsock_listen(cc->sock, TCPSERVER_CMD_PORT);
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_cmd_drop(cmd_ctl_t* cc)
    {
    if (f_primary_conn_ptr == cc)
        {
        lsock_shutdown(f_data.sock);
        f_primary_conn_ptr = NULL;
        }
    cc->dir = CMD_DIR_NONE;
    lsock_shutdown(cc->sock);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static int f_cmd_process_rq(cmd_ctl_t* cc)
    {
    cmd_buf_t* buf = cc->buf;

    switch (buf->request.cmd_code)
        {
        case LTR_CMD_CODE_INIT:
            if (buf->request.reply_data_len < sizeof(g_ctlfunc_fpga_info))
                return 0;
            memcpy(buf->reply.data, (const uint8_t*)g_ctlfunc_fpga_info,
                sizeof(g_ctlfunc_fpga_info) - sizeof(tcp_protocol_id));
            memcpy(buf->reply.data + sizeof(g_ctlfunc_fpga_info) - sizeof(tcp_protocol_id),
                &tcp_protocol_id, sizeof(tcp_protocol_id));
            if (f_primary_conn_ptr == cc)
                f_datasock_reopen();

            return 1;

        case LTR_CMD_CODE_NOP:
            return 1;

        case LTR_CMD_CODE_GETNAME:
            {
            int n;
            const char* p = ctlfunc_get_module_name(&n);
            if (buf->request.reply_data_len < (size_t)n)
                return 0;
            memcpy(buf->reply.data, p, (size_t)n);
            }
            return 1;

        case LTR_CMD_CODE_GETARRAY:
            switch (buf->request.param)
                {
                case IP_GETARRAY_IS_PRIMARY:
                    buf->reply.data[0] = (f_primary_conn_ptr == cc);
                    return 1;
                default:
                    if (f_primary_conn_ptr != cc) return 0;
                    return ctlfunc_get_array(buf->request.param, buf->request.reply_data_len, buf->reply.data);
                }

        case LTR_CMD_CODE_PUTARRAY:
            switch (buf->request.param)
                {
                case IP_PUTARRAY_FORCE_PRIMARY:
                    if (f_primary_conn_ptr != cc)
                        {
                        f_primary_conn_ptr = cc;
                        /* Reset data connection */
                        f_datasock_reopen();
                        /* Disconnect other clients */
                        for (int i = 0; i < N_CMD_CONN; i++)
                            {
                            if (&f_cmd[i] != cc) f_cmd_drop(&f_cmd[i]);
                            }
                        }
                    return 1;
                default:
                    if (f_primary_conn_ptr != cc) return 0;
                    return ctlfunc_put_array(buf->request.param, buf->request.data_len, buf->request.data);
                }

        default:
            return 0;
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_cmd_worker(cmd_ctl_t* cc)
    {
    cmd_buf_t* buf = cc->buf;
    if (CMD_DIR_IN == cc->dir)
        { /* receiving request */
        int nbytes = lsock_recv(cc->sock, &buf->u8[cc->xfer_pos], cc->xfer_len - cc->xfer_pos);
        if (nbytes < 0)
            return;
        cc->xfer_pos += (size_t)nbytes;
        if (cc->xfer_pos >= cc->xfer_len)
            {
            int process_rq = 0;
            if (CMD_REQUEST_HEADER_SIZE == cc->xfer_len)
                { /* received header */
                if ((LTR_CMD_SIG == buf->request.sig) && (buf->request.data_len <= CMD_MAX_DATA)
                   && (buf->request.reply_data_len <= CMD_MAX_DATA))
                    {
                    if (buf->request.data_len > 0)
                        { /* Proceed to receive variable-length data */
                        cc->xfer_len += buf->request.data_len;
                        }
                    else
                        { /* nothing more to receive */
                        process_rq = 1;
                        }
                    }
                else
                    { /* invalid request */
                    f_cmd_drop(cc);
                    }
                }
            else
                { /* received entire request */
                process_rq = 1;
                }

            if (process_rq)
                {
                size_t rdlen = buf->request.reply_data_len;
                buf->reply.result_code = (f_cmd_process_rq(cc)) ? LTR_RESULT_OK : LTR_RESULT_ERR;
                buf->reply.sig = LTR_CMD_SIG;
                buf->reply.data_len = rdlen;
                cc->dir = CMD_DIR_OUT;
                cc->xfer_pos = 0;
                cc->xfer_len = CMD_REPLY_HEADER_SIZE + rdlen;
                }
            }
        }
    else if (CMD_DIR_OUT == cc->dir)
        { /* sending reply */
        int nbytes = lsock_send(cc->sock, &buf->u8[cc->xfer_pos], cc->xfer_len - cc->xfer_pos);
        if (nbytes < 0)
            return;
        cc->xfer_pos += (size_t)nbytes;
        if (cc->xfer_pos >= cc->xfer_len)
            { /* prepare to receive next command */
            cc->dir = CMD_DIR_IN;
            cc->xfer_pos = 0;
            cc->xfer_len = CMD_REQUEST_HEADER_SIZE;
            }
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static int f_connstate_changed(int sock, int* p_last_state)
    {
    int st = lsock_state(sock);
    if (st != *p_last_state)
        {
        *p_last_state = st;
        switch (st)
            {
            case LIP_TCP_STATE_CLOSE_WAIT:
                lsock_shutdown(sock);
                break;
            case LIP_TCP_STATE_CLOSED:
            case LIP_TCP_STATE_ESTABLISHED:
                return 1;
            }
        }
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_init(void)
    {
    /* LIP must have been already initialized */

    ASSERT(N_CMD_CONN + 1 <= LIP_TCP_SOCKET_CNT);
    for (int i = 0; i < N_CMD_CONN; i++)
        {
        f_cmd[i].buf = &f_cmd_buf[i];
        f_cmd[i].sock = lsock_init();
        /* Можно проверить ошибку if (sock < 0), но условие ASSERT должно гарантировать */
        lsock_set_recv_fifo(f_cmd[i].sock, f_sockbuf_cmd_rx[i], sizeof(f_sockbuf_cmd_rx[i]));
        lsock_set_send_fifo(f_cmd[i].sock, f_sockbuf_cmd_tx[i], sizeof(f_sockbuf_cmd_tx[i]));
        lsock_listen(f_cmd[i].sock, TCPSERVER_CMD_PORT);
        f_cmd[i].last_sock_state = lsock_state(f_cmd[i].sock);
        f_cmd[i].dir = CMD_DIR_NONE;
        }

    f_primary_conn_ptr = NULL;

    f_datasock_init();
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_reset(void)
    {
    /* Called by ctlfunc_reset_fpga, probably nothing to do here */
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t tcpserver_send_avail(void)
    {
    if (f_data.last_sock_state != LIP_TCP_STATE_ESTABLISHED) return 0;
    int nbytes = lsock_send_size(f_data.sock);
    if (nbytes < 0) nbytes = 0;
    return (size_t)nbytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t tcpserver_send(const uint8_t* data, size_t len, int push)
    {
    if (len > tcpserver_send_avail())
        return 0;

    int nbytes = lsock_send(f_data.sock, data, len);
    if (nbytes < 0) nbytes = 0;
    return (size_t)nbytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t tcpserver_recv(uint8_t* data, size_t len)
    {
    int nbytes = lsock_recv(f_data.sock, data, len);
    if (nbytes < 0) nbytes = 0;
    return (size_t)nbytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_worker(void)
    {
    lip_pull();
    for (int i = 0; i < N_CMD_CONN; i++)
        {
        if (f_connstate_changed(f_cmd[i].sock, &f_cmd[i].last_sock_state))
            f_cmd_stchg(&f_cmd[i]);
        }
    if (f_connstate_changed(f_data.sock, &f_data.last_sock_state))
        f_data_stchg();

    /* Handle command connections */
    for (int i = 0; i < N_CMD_CONN; i++)
        {
        f_cmd_worker(&f_cmd[i]);
        }
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
