/*================================================================================================*
 * Данные по протоколу TCP/IP (lip sockets)
 *================================================================================================*/

#include "tcpserver.h"
#include "lip.h"
#include <string.h>
#include "ctlfunc.h"

/*================================================================================================*/
#define TX_BLOCK_LEN            TCPSERVER_DATA_TX_BLOCK_LEN
#define TX_BUF_NBLK             TCPSERVER_DATA_TX_NBLK
#define TX_BUF_SIZE             (TX_BLOCK_LEN * TX_BUF_NBLK)

#define RX_BLOCK_LEN            TCPSERVER_DATA_RX_BLOCK_LEN
#define RX_BUF_NBLK             TCPSERVER_DATA_RX_NBLK
#define RX_BUF_SIZE             (RX_BLOCK_LEN * RX_BUF_NBLK)
/*================================================================================================*/

/*================================================================================================*/
static int f_sock;
static int f_last_sock_state;

/* Примечания по алгоритму - в usb_bulk.c, этот код почти полностью повторяет его. */

typedef uint8_t* bufptr_t;

static TCPSERVER_DATA_BUF_DECL uint8_t f_rx_buf[RX_BUF_SIZE];
static TCPSERVER_DATA_BUF_DECL uint8_t f_tx_buf[TX_BUF_SIZE];

/* переменные состояния передатчика */
static bufptr_t f_tx_rptr = f_tx_buf;   /* позиция, откуда читаются данные для отправки */
static bufptr_t f_tx_wptr = f_tx_buf;   /* позиция, куда пишутся данные в tx_buf */
static size_t f_tx_bytes_used;          /* количество занятых байт в буфере */
static size_t f_tx_bytes_committed; /* кол-во байт всего, поставленных на отправку */
static volatile size_t f_tx_bytes_finished; /* кол-во байт всего, посылка которых закончена */

/* переменные состояния приемника */
static volatile size_t f_rx_blk_len[RX_BUF_NBLK]; /* Размер полезных данных в каждом блоке */
static bufptr_t f_rx_fbptr = f_rx_buf;  /* указатель на следующий блок, добавляемый в очередь */
static size_t f_rx_rbidx;               /* индекс текущего читаемого блока */
static size_t f_rx_rbofs;               /* смещение в текущем читаемом блоке */
static volatile size_t f_rx_blks_rcvd;  /* кол-во ВСЕГО принятых блоков */
static size_t f_rx_blks_read;           /* кол-во ВСЕГО прочитанных и освобожденных блоков */
static size_t f_rx_blks_to_queue;       /* кол-во блоков, которые можно ставить в очередь */
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static inline bufptr_t f_ring_add(bufptr_t x, size_t delta_x, bufptr_t buf, size_t buf_len)
    {
    x += delta_x;
    if (x >= buf + buf_len) x -= buf_len;
    return x;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static inline int f_tcp_send(const uint8_t* data, size_t len)
    {
    return (lsock_send_start(f_sock, (uint8_t*)data, (int)len) >= 0);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static inline int f_tcp_recv(uint8_t* data, size_t len)
    {
    return (lsock_recv_start(f_sock, (uint8_t*)data, (int)len) >= 0);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/* callback по завершению приема/передачи */

#if !defined(LIP_TCP_CB_DD_RECV_DONE) || !defined(LIP_TCP_CB_DD_SEND_DONE)
#error "LIP callbacks must be enabled!"
#endif

void lsock_cb_dd_send_done(int sock_id, int dd_num, t_lip_tcp_dd* pdd)
    {
    if (LIP_TCP_DD_STATUS_DONE == pdd->status)
        f_tx_bytes_finished += TX_BLOCK_LEN;
    }

void lsock_cb_dd_recv_done(int sock_id, int dd_num, t_lip_tcp_dd* pdd)
    {
    if (LIP_TCP_DD_STATUS_DONE == pdd->status)
        {
        size_t nbytes = pdd->trans_cnt;
        int blk_idx = ((bufptr_t)pdd->buf - f_rx_buf) / RX_BLOCK_LEN;
        if ((blk_idx >= 0) && (blk_idx < RX_BUF_NBLK))
            {
            f_rx_blk_len[blk_idx] = nbytes;
            f_rx_blks_rcvd++;
            }
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_start_tx(int push)
    {
    /* Отправка заполненных блоков, а если push != 0 - и неполного */
    while (f_tx_bytes_used > 0)
        {
        size_t len = TX_BLOCK_LEN;
        if (f_tx_bytes_used < len)
            { /* неполный блок отправляется только если push != 0 */
            if (!push)
                break;
            len = f_tx_bytes_used;
            }
        if (!f_tcp_send(f_tx_rptr, len))
            break;
        /* rp и bytes_committed всегда увеличивается на полный блок */
        f_tx_bytes_committed += TX_BLOCK_LEN;
        f_tx_rptr = f_ring_add(f_tx_rptr, TX_BLOCK_LEN, f_tx_buf, TX_BUF_SIZE);
        f_tx_bytes_used -= len;
        /* если блок был неполный (и тогда он последний), то сдвинуть указатель wp, 
           чтобы он не оказался "левее" rp */
        if (len < TX_BLOCK_LEN)
            f_tx_wptr = f_tx_rptr;
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_queue_rx(void)
    {
    /* Постановка освободившихся блоков в очередь для чтения */
    while (f_rx_blks_to_queue > 0)
        {
        if (!f_tcp_recv(f_rx_fbptr, RX_BLOCK_LEN))
            break;
        f_rx_fbptr = f_ring_add(f_rx_fbptr, RX_BLOCK_LEN, f_rx_buf, RX_BUF_SIZE);
        f_rx_blks_to_queue--;
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static int f_sockstate_monitor(void)
    {
    int st = lsock_state(f_sock);
    if (st != f_last_sock_state)
        {
        f_last_sock_state = st;
        switch (st)
            {
            case LIP_TCP_STATE_CLOSE_WAIT:
                lsock_shutdown(f_sock);
                break;
            case LIP_TCP_STATE_CLOSED:
                /* data connection closed */
                break;
            case LIP_TCP_STATE_ESTABLISHED:
                /* data connection open */
                ctlfunc_reset_fpga();
                break;
            }
        }
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_init(void)
    {
    /* LIP must have been already initialized */
    
    /* init socket */
    f_sock = lsock_init();
    f_last_sock_state = lsock_state(f_sock);

    /* init tx */
    f_tx_rptr = f_tx_wptr = f_tx_buf;
    f_tx_bytes_used = 0;
    f_tx_bytes_finished = f_tx_bytes_committed = 0;

    /* init rx */
    f_rx_fbptr = f_rx_buf;
    f_rx_rbofs = f_rx_rbidx = 0;
    f_rx_blks_rcvd = f_rx_blks_read = 0;
    f_rx_blks_to_queue = RX_BUF_NBLK; /* Готов добавить все блоки в очередь */
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_sock_listen(void)
    {
    if (lsock_state(f_sock) != LIP_TCP_STATE_CLOSED)
        {
        lsock_abort(f_sock);
        tcpserver_data_init();
        }
    lsock_listen(f_sock, TCPSERVER_DATA_PORT);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_sock_shutdown(void)
    {
    lsock_shutdown(f_sock);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_reset(void)
    {
    /* Called by ctlfunc_reset_fpga, probably nothing to do here */
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t tcpserver_data_send_avail(void)
    {
    if (f_last_sock_state != LIP_TCP_STATE_ESTABLISHED) return 0;
    size_t unsent_bytes = f_tx_bytes_committed - f_tx_bytes_finished;
    return TX_BUF_SIZE - f_tx_bytes_used - unsent_bytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t tcpserver_data_send(const uint8_t* data, size_t len, int push)
    {
    if (len > tcpserver_data_send_avail())
        return 0;

    bufptr_t new_wptr = f_ring_add(f_tx_wptr, len, f_tx_buf, TX_BUF_SIZE);

    if (new_wptr >= f_tx_wptr)
        {
        memcpy((uint8_t*)f_tx_wptr, data, len);
        }
    else
        {
        size_t len1 = (size_t)(f_tx_buf + TX_BUF_SIZE - f_tx_wptr);
        memcpy((uint8_t*)f_tx_wptr, data, len1);
        memcpy((uint8_t*)f_tx_buf, data + len1, len - len1);
        }

    f_tx_wptr = new_wptr;
    f_tx_bytes_used += len;
    f_start_tx(push);
    return len;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t tcpserver_data_recv(uint8_t* data, size_t len)
    {
    size_t byte_count = 0;
    size_t unread_blks = f_rx_blks_rcvd - f_rx_blks_read;
    size_t blklen = f_rx_blk_len[f_rx_rbidx];

    /* Чтение с пропуском хвостов неполных блоков и блоков нулевой длины */

    while ((byte_count < len) && (unread_blks > 0))
        {
        if (f_rx_rbofs < blklen)
            { /* есть еще данные в этом блоке */
            size_t n = blklen - f_rx_rbofs;
            if (n > len - byte_count) n = len - byte_count;
            memcpy(data + byte_count, (uint8_t*)&f_rx_buf[f_rx_rbidx * RX_BLOCK_LEN + f_rx_rbofs], n);
            byte_count += n;
            f_rx_rbofs += n;
            }

        if (f_rx_rbofs >= blklen)
            { /* в блоке прочитаны все данные */
            f_rx_rbofs = 0;
            f_rx_blks_read++;
            f_rx_blks_to_queue++;
            if (++f_rx_rbidx >= RX_BUF_NBLK) f_rx_rbidx = 0;
            blklen = f_rx_blk_len[f_rx_rbidx];
            unread_blks--;
            }
        }

    if (f_rx_blks_to_queue > 0)
        f_queue_rx();

    return byte_count;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_data_worker(void)
    {
    f_sockstate_monitor();

    if (LIP_TCP_STATE_ESTABLISHED == f_last_sock_state)
        {
        /* Отправка данных. Неполный блок посылается, если очередь пуста. */
        f_start_tx(!lsock_send_req_cnt(f_sock));
        /* Постановка в очередь на чтение свободных блоков */
        f_queue_rx();
        }
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
