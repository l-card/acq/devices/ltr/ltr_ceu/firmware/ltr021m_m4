/*================================================================================================*
 * Переход от USB bulk к вводу-выводу байтов (с кольцевыми буферами)
 *================================================================================================*/

#include "usb_bulk.h"
#include <string.h>

/*================================================================================================*/
#define TX_BLOCK_LEN    USBBULK_TX_BLOCK_LEN
#define TX_BUF_NBLK     USBBULK_TX_BUF_NBLK
#define TX_BUF_SIZE     (TX_BLOCK_LEN * TX_BUF_NBLK)

#define RX_BLOCK_LEN    USBBULK_RX_BLOCK_LEN
#define RX_BUF_NBLK     USBBULK_RX_BUF_NBLK
#define RX_BUF_SIZE     (RX_BLOCK_LEN * RX_BUF_NBLK)
/*================================================================================================*/

/*================================================================================================*/
uint8_t g_usbbulk_flags = 0;

typedef volatile uint8_t* bufptr_t;

static volatile USBBULK_BUF_DECL uint8_t f_rx_buf[RX_BUF_SIZE];
static volatile USBBULK_BUF_DECL uint8_t f_tx_buf[TX_BUF_SIZE];

/* переменные состояния передатчика */
static bufptr_t f_tx_rptr = f_tx_buf;   /* позиция, откуда читаются данные для отправки в USB */
static bufptr_t f_tx_wptr = f_tx_buf;   /* позиция, куда пишутся данные в tx_buf */
static size_t f_tx_bytes_used;          /* количество занятых байт в буфере */
static size_t f_tx_bytes_committed; /* кол-во байт всего, поставленных на отправку в USB */
static int  f_tx_zeropkt_req; /* признак, что последняя посылка была кратна размеру кт и необходимо послать
                              zerolength packet, если не будет еще данных */
static volatile size_t f_tx_bytes_finished; /* кол-во байт всего, посылка которых в USB закончена */

/* Примечание по алгоритму передачи:

   Когда начинается запись в USB, f_tx_bytes_committed увеличивается на размер блока.
   В callback по окончанию DMA увеличивается f_tx_bytes_finished.
   Разность этих счетчиков = кол-ву байт, передача которых не завершена, оно вычитается из
   свободного места в буфере при проверке возможности записи новых данных.
   Счетчики свободно переходят через 0 при переполнении, вычитание беззнаковых будет корректно.

   Это можно было сделать одной переменной (типа bytes_unfinished), но тогда нужно защищать
   цикл read-modify-write от одновременного выполнения с обработчиком прерывания.
 */

/* переменные состояния приемника */
static volatile size_t f_rx_blk_len[RX_BUF_NBLK]; /* Размер полезных данных в каждом блоке */
static bufptr_t f_rx_fbptr = f_rx_buf;  /* указатель на следующий блок, добавляемый в очередь */
static size_t f_rx_rbidx;               /* индекс текущего читаемого блока */
static size_t f_rx_rbofs;               /* смещение в текущем читаемом блоке */
static volatile size_t f_rx_blks_rcvd;  /* кол-во ВСЕГО принятых блоков */
static size_t f_rx_blks_read;           /* кол-во ВСЕГО прочитанных и освобожденных блоков */
static size_t f_rx_blks_to_queue;       /* кол-во блоков, которые можно ставить в очередь */

/* Примечание по алгоритму приема:

   Количество доступных для чтения программой блоков == f_rx_blks_rcvd - f_rx_blks_read
   (счетчики переходят через 0 при переполнении, вычитаются как беззнаковые).

   Размеры прочитанных блоков записываются в f_rx_blk_len[].
   Чтение идет байтами до прочитанной длины, затем f_rx_rbidx переходит на слудеющий блок,
   а освобожденный блок ставится обратно в очередь на прием.

 */
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static inline bufptr_t f_ring_add(bufptr_t x, size_t delta_x, bufptr_t buf, size_t buf_len)
    {
    x += delta_x;
    if (x >= buf + buf_len) x -= buf_len;
    return x;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static inline int f_usb_send(const volatile uint8_t* data, size_t len) {
    __dmb();
    int ret = lusb_ep_add_dd(LUSB_TX_EP_IND, (uint8_t*)data, len, 0);
    if (ret == LUSB_ERR_SUCCESS) {
        if (((len % lusb_ep_descr(0, LUSB_TX_EP_IND)->wMaxPacketSize) == 0) && (len != 0)) {
            f_tx_zeropkt_req = 1;
        } else {
            f_tx_zeropkt_req = 0;
        }
    }
    return (ret == LUSB_ERR_SUCCESS);
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static inline int f_usb_recv(volatile uint8_t* data, size_t len) {
    return (lusb_ep_add_dd(LUSB_RX_EP_IND, (uint8_t*)data, len, 0) == LUSB_ERR_SUCCESS);
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/* callback ИЗ ПРЕРЫВАНИЯ по завершению приема/передачи */
void lusb_appl_cb_dd_event(uint8_t iface, uint8_t ep_ind, t_lusb_dd_event event, const t_lusb_dd_status* p_dd) {
    if (LUSB_DD_EVENT_EOT != event) return;

    if (LUSB_TX_EP_IND == ep_ind) {
        if (p_dd->length != 0) {
            /* закончилась передача блока */
            f_tx_bytes_finished += TX_BLOCK_LEN;
        }
    } else if (LUSB_RX_EP_IND == ep_ind) {
        /* закончился прием блока */
        size_t nbytes = p_dd->trans_cnt;
        int blk_idx = ((bufptr_t)p_dd->last_addr - nbytes - f_rx_buf) / RX_BLOCK_LEN;
        if ((blk_idx >= 0) && (blk_idx < RX_BUF_NBLK)) {
            f_rx_blk_len[blk_idx] = nbytes;
            f_rx_blks_rcvd++;
        }
    }
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_start_tx(int push) {
    /* Отправка в USB заполненных блоков, а если push != 0 - и неполного */
    while (f_tx_bytes_used > 0) {
        size_t len = TX_BLOCK_LEN;
        if (f_tx_bytes_used < len) {
            /* неполный блок отправляется только если push != 0 */
            if (!push)
                break;
            len = f_tx_bytes_used;
        }
        if (!f_usb_send(f_tx_rptr, len))
            break;
        /* rp и bytes_committed всегда увеличивается на полный блок */
        f_tx_bytes_committed += TX_BLOCK_LEN;
        f_tx_rptr = f_ring_add(f_tx_rptr, TX_BLOCK_LEN, f_tx_buf, TX_BUF_SIZE);
        f_tx_bytes_used -= len;
        /* если блок был неполный (и тогда он последний), то сдвинуть указатель wp, 
           чтобы он не оказался "левее" rp */
        if (len < TX_BLOCK_LEN)
            f_tx_wptr = f_tx_rptr;
    }


    if ((f_tx_bytes_used == 0) && f_tx_zeropkt_req && push) {
        f_usb_send(NULL, 0);
    }
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_queue_rx(void)
    {
    /* Постановка освободившихся блоков в очередь для чтения */
    while (f_rx_blks_to_queue > 0)
        {
        if (!f_usb_recv(f_rx_fbptr, RX_BLOCK_LEN))
            break;
        f_rx_fbptr = f_ring_add(f_rx_fbptr, RX_BLOCK_LEN, f_rx_buf, RX_BUF_SIZE);
        f_rx_blks_to_queue--;
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void usbbulk_init(void)
    {
    /* init tx */
    f_tx_rptr = f_tx_wptr = f_tx_buf;
    f_tx_bytes_used = 0;
    f_tx_bytes_finished = f_tx_bytes_committed = 0;

    /* init rx */
    f_rx_fbptr = f_rx_buf;
    f_rx_rbofs = f_rx_rbidx = 0;
    f_rx_blks_rcvd = f_rx_blks_read = 0;
    f_rx_blks_to_queue = RX_BUF_NBLK; /* Готов добавить все блоки в очередь */
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void usbbulk_reset(void)
    {
    lusb_ep_clear(LUSB_TX_EP_IND);
    lusb_ep_clear(LUSB_RX_EP_IND);
#if USBBULK_CLEARDMA_HACK
    /* Хак для борьбы с потерей первого пакета при глючной очистке DMA (баг LPC??)
       Верхний уровень должен уметь отбрасывать пакет нулевой длины без ошибки */
    lusb_ep_add_dd(LUSB_TX_EP_IND, NULL, 0, LUSB_ADDBUF_FLG_AUTOSTART);
#endif
    usbbulk_init();
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t usbbulk_send_buf_avail(void)
    {
    size_t unsent_bytes = f_tx_bytes_committed - f_tx_bytes_finished;
    return TX_BUF_SIZE - f_tx_bytes_used - unsent_bytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t usbbulk_send(const uint8_t* data, size_t len, int push) {
    size_t max_send = usbbulk_send_buf_avail();

    if (len > max_send) {
        if (!(g_usbbulk_flags & USBBULK_FLAG_PARTIAL_SEND))
            return 0;
        len = max_send;
    }

#if USBBULK_TX_USE_MEMCPY
    bufptr_t new_wptr = f_ring_add(f_tx_wptr, len, f_tx_buf, TX_BUF_SIZE);

    if (new_wptr > f_tx_wptr) {
        memcpy((uint8_t*)f_tx_wptr, data, len);
    } else {
        size_t len1 = (size_t)(f_tx_buf + TX_BUF_SIZE - f_tx_wptr);
        memcpy((uint8_t*)f_tx_wptr, data, len1);
        memcpy((uint8_t*)f_tx_buf, data + len1, len - len1);
    }

    f_tx_wptr = new_wptr;
#else
    size_t i;
    for (i = 0; i < len; i++) {
        *f_tx_wptr++ = *data++;
        f_tx_wptr = f_ring_add(f_tx_wptr, 0, f_tx_buf, TX_BUF_SIZE);
    }
#endif

    f_tx_bytes_used += len;

    f_start_tx(push);
    return len;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t usbbulk_recv(uint8_t* data, size_t len)
    {
    size_t byte_count = 0;
    size_t unread_blks = f_rx_blks_rcvd - f_rx_blks_read;
    size_t blklen = f_rx_blk_len[f_rx_rbidx];

    /* Чтение с пропуском хвостов неполных блоков и блоков нулевой длины */

    while ((byte_count < len) && (unread_blks > 0))
        {
        if (f_rx_rbofs < blklen)
            { /* есть еще данные в этом блоке */

#if USBBULK_RX_USE_MEMCPY

            size_t n = blklen - f_rx_rbofs;
            if (n > len - byte_count) n = len - byte_count;
            memcpy(data + byte_count, (uint8_t*)&f_rx_buf[f_rx_rbidx * RX_BLOCK_LEN + f_rx_rbofs], n);
            byte_count += n;
            f_rx_rbofs += n;

#else

            data[byte_count++] = f_rx_buf[f_rx_rbidx * RX_BLOCK_LEN + f_rx_rbofs++];

#endif

            }

        if (f_rx_rbofs >= blklen)
            { /* в блоке прочитаны все данные */
            f_rx_rbofs = 0;
            f_rx_blks_read++;
            f_rx_blks_to_queue++;
            if (++f_rx_rbidx >= RX_BUF_NBLK) f_rx_rbidx = 0;
            blklen = f_rx_blk_len[f_rx_rbidx];
            unread_blks--;
            }

        }

    if (f_rx_blks_to_queue > 0)
        f_queue_rx();

    return byte_count;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void usbbulk_worker(void) {
    /* Отправка данных в USB. Неполный блок посылается, если очередь пуста. */
    f_start_tx(!lusb_ep_get_dd_in_progress(LUSB_TX_EP_IND));

    /* Постановка в очередь на чтение свободных блоков */
    f_queue_rx();
}
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
