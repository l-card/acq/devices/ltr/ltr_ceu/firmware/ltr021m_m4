/*================================================================================================*
 * Команды по протоколу TCP/IP (lip sockets)
 *================================================================================================*/

#include "tcpserver.h"
#include "lip.h"
#include "ctlfunc.h"
#include "ledblink.h"
#include "ltimer.h"
#include "conf.h"
#include <string.h>

/*================================================================================================*/
#define N_CMD_CONN              TCPSERVER_CMD_NCONN
#define CMD_MAX_DATA            512
#define REUSE_DATA_CONNECTION   0

#define LTR_CMD_SIG             0x314C5443
#define LTR_TCP_PROTO_VER_SIG   0xD91D3B45
#define LTR_TCP_PROTO_VER_HI    1
#define LTR_TCP_PROTO_VER_LO    1
enum
    {
    LTR_CMD_CODE_NOP = 0, LTR_CMD_CODE_GETNAME, LTR_CMD_CODE_GETARRAY, LTR_CMD_CODE_PUTARRAY,
    LTR_CMD_CODE_INIT = 0x80000000UL
    };
enum
    {
    LTR_RESULT_OK = 0, LTR_RESULT_ERR = 1
    };

#define IP_GETARRAY_IS_PRIMARY    (CTLFUNC_SEL_AVR_DM + 0x10070)
#define IP_PUTARRAY_FORCE_PRIMARY (CTLFUNC_SEL_AVR_DM + 0x10072)

enum en_cmd_state /* values for f_cmd[].state */
    {
    CMD_ST_CLOSED,      /* not connected */
    CMD_ST_RECV,        /* receiving request */
    CMD_ST_SEND         /* sending reply */
    };

/* Буфер для приема команд и посылки ответов (по одному на соединение) */
#define CMD_REQUEST_HEADER_SIZE     offsetof(struct st_cmd_request, data)
#define CMD_REPLY_HEADER_SIZE       offsetof(struct st_cmd_reply, data)
typedef union
    {
    struct st_cmd_request
        {
        uint32_t sig;
        uint32_t cmd_code;
        uint32_t param;
        uint32_t data_len;
        uint32_t reply_data_len;
        uint8_t data[CMD_MAX_DATA];
        }
        request;
    struct st_cmd_reply
        {
        uint32_t sig;
        uint32_t result_code;
        uint32_t data_len;
        uint8_t data[CMD_MAX_DATA];
        }
        reply;
    uint8_t u8[sizeof(struct st_cmd_request)];
    }
    cmd_buf_t;

typedef struct
    {
    t_lsock_id sock;
    int last_sock_state;
    size_t xfer_pos;        /* current offset into request/reply */
    size_t xfer_len;        /* length of request/reply */
    cmd_buf_t* buf;
    int state;              /* sending/receiving/closed */
    t_ltimer timeout;
    }
    cmd_ctl_t;
/*================================================================================================*/

/*================================================================================================*/
static const struct __attribute__ ((packed))
    {
    uint32_t sig;
    uint8_t hi, lo;
    }
    tcp_protocol_id =
    { .sig = LTR_TCP_PROTO_VER_SIG, .hi = LTR_TCP_PROTO_VER_HI, .lo = LTR_TCP_PROTO_VER_LO };

static cmd_ctl_t* f_primary_conn_ptr = NULL;
static cmd_ctl_t f_cmd[N_CMD_CONN];
static cmd_buf_t f_cmd_buf[N_CMD_CONN];

static TCPSERVER_CMD_BUF_DECL uint8_t f_sockbuf_cmd_rx[N_CMD_CONN][TCPSERVER_CMD_RX_BUF_LEN];
static TCPSERVER_CMD_BUF_DECL uint8_t f_sockbuf_cmd_tx[N_CMD_CONN][TCPSERVER_CMD_TX_BUF_LEN];
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static void f_cmd_sockstate_monitor(cmd_ctl_t* cc)
    {
    int st = lsock_state(cc->sock);
    if (st != cc->last_sock_state)
        {
        cc->last_sock_state = st;
        switch (st)
            {
            case LIP_TCP_SOCK_STATE_CLOSE_WAIT:
                lsock_shutdown(cc->sock);
                break;
            case LIP_TCP_SOCK_STATE_CLOSED:
                /* command connection closed */
                if (f_primary_conn_ptr == cc)
                    {
#if !REUSE_DATA_CONNECTION
                    tcpserver_data_hangup_on_host(cc->sock);
#endif
                    f_primary_conn_ptr = NULL;
                    }
                cc->state = CMD_ST_CLOSED;
                /* listen for another connection */
                lsock_listen(cc->sock, TCPSERVER_CMD_PORT);
                break;
            case LIP_TCP_SOCK_STATE_ESTABLISHED:
                /* command connection open */
                if (!f_primary_conn_ptr) f_primary_conn_ptr = cc;
                cc->state = CMD_ST_RECV;
                cc->xfer_pos = 0;
                cc->xfer_len = CMD_REQUEST_HEADER_SIZE;
                ltimer_set(&cc->timeout, LTIMER_MS_TO_CLOCK_TICKS(TCPSERVER_CMD_INIT_TIMEOUT_MS));
                break;
            }
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_cmd_drop(cmd_ctl_t* cc)
    {
    if (f_primary_conn_ptr == cc)
        {
        tcpserver_data_hangup_on_host(cc->sock);
        f_primary_conn_ptr = NULL;
        }
    cc->state = CMD_ST_CLOSED;
    lsock_close(cc->sock, TCPSERVER_HANGUP_TIMEOUT_MS);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static ssize_t f_cmd_process_rq(cmd_ctl_t* cc) /* returns reply len or < 0 on error */
    {
    cmd_buf_t* buf = cc->buf;
    uint32_t param = buf->request.param;
    size_t put_len = buf->request.data_len;
    size_t get_len = buf->request.reply_data_len;

    switch (buf->request.cmd_code)
        {
        case LTR_CMD_CODE_INIT:
            if (get_len < sizeof(g_ctlfunc_fpga_info))
                return -1;
            memcpy(buf->reply.data, (const uint8_t*)g_ctlfunc_fpga_info,
                sizeof(g_ctlfunc_fpga_info) - sizeof(tcp_protocol_id));
            memcpy(buf->reply.data + sizeof(g_ctlfunc_fpga_info) - sizeof(tcp_protocol_id),
                &tcp_protocol_id, sizeof(tcp_protocol_id));
            if (f_primary_conn_ptr == cc)
                {
#if REUSE_DATA_CONNECTION
                if (!tcpserver_data_sock_connected_to(cc->sock))
#endif
                    tcpserver_data_sock_listen(cc->sock);
                }
            return sizeof(g_ctlfunc_fpga_info);

        case LTR_CMD_CODE_NOP:
            return 0;

        case LTR_CMD_CODE_GETNAME:
            {
            int n;
            const char* p = ctlfunc_get_module_name(&n);
            if (get_len < (size_t)n)
                return -1;
            memcpy(buf->reply.data, p, (size_t)n);
            return n;
            }

        case LTR_CMD_CODE_GETARRAY:
            switch (param)
                {
                case IP_GETARRAY_IS_PRIMARY:
                    if (get_len < 1)
                        return -1;
                    buf->reply.data[0] = (f_primary_conn_ptr == cc);
                    return 1;
                default:
                    if (f_primary_conn_ptr != cc)
                        return -1;
                    if (!ctlfunc_get_array(CONF_IFACE_NET, param, get_len, buf->reply.data))
                        return -1;
                    return (ssize_t)get_len;
                }

        case LTR_CMD_CODE_PUTARRAY:
            switch (param)
                {
                case IP_PUTARRAY_FORCE_PRIMARY:
                    if (f_primary_conn_ptr != cc)
                        {
                        f_primary_conn_ptr = cc;
                        /* Disconnect other clients */
                        for (int i = 0; i < N_CMD_CONN; i++)
                            {
                            if (&f_cmd[i] != cc) f_cmd_drop(&f_cmd[i]);
                            }
                        /* Reset data connection */
                        tcpserver_data_sock_listen(cc->sock);
                        }
                    return 0;
                default:
                    if (f_primary_conn_ptr != cc)
                        return -1;
                    if (!ctlfunc_put_array(CONF_IFACE_NET, param, put_len, buf->request.data))
                        return -1;
                    return 0;
                }

        default:
            return -1;
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_cmd_worker(cmd_ctl_t* cc)
    {
    cmd_buf_t* buf = cc->buf;

    if ((cc->timeout.interval != 0) && ltimer_expired(&cc->timeout))
        {
        f_cmd_drop(cc);
        return;
        }

    if (CMD_ST_RECV == cc->state)
        { /* receiving request */
        int nbytes = lsock_recv(cc->sock, &buf->u8[cc->xfer_pos], (int)(cc->xfer_len - cc->xfer_pos));
        if (nbytes < 0)
            return;
        cc->xfer_pos += (size_t)nbytes;
        if (cc->xfer_pos >= cc->xfer_len)
            {
            int process_rq = 0;
            if (CMD_REQUEST_HEADER_SIZE == cc->xfer_len)
                { /* received header */
                if ((LTR_CMD_SIG == buf->request.sig) && (buf->request.data_len <= CMD_MAX_DATA)
                   && (buf->request.reply_data_len <= CMD_MAX_DATA))
                    {
                    if (buf->request.data_len > 0)
                        { /* Proceed to receive variable-length data */
                        cc->xfer_len += buf->request.data_len;
                        }
                    else
                        { /* nothing more to receive */
                        process_rq = 1;
                        }
                    }
                else
                    { /* invalid request */
                    f_cmd_drop(cc);
                    }
                }
            else
                { /* received entire request */
                process_rq = 1;
                }

            if (process_rq)
                {
                ssize_t reply_len = f_cmd_process_rq(cc);
                if (reply_len >= 0)
                    {
                    buf->reply.result_code = LTR_RESULT_OK;
                    buf->reply.data_len = (uint32_t)reply_len;
                    }
                else
                    {
                    buf->reply.result_code = LTR_RESULT_ERR;
                    buf->reply.data_len = 0;
                    }
                buf->reply.sig = LTR_CMD_SIG;
                cc->state = CMD_ST_SEND;
                cc->xfer_pos = 0;
                cc->xfer_len = CMD_REPLY_HEADER_SIZE + buf->reply.data_len;
                cc->timeout.interval = 0;   /* Disable timeout after receiving any command */
                }
            }
        }
    else if (CMD_ST_SEND == cc->state)
        { /* sending reply */
        int nbytes = lsock_send(cc->sock, &buf->u8[cc->xfer_pos], (int)(cc->xfer_len - cc->xfer_pos));
        if (nbytes < 0)
            return;
        cc->xfer_pos += (size_t)nbytes;
        if (cc->xfer_pos >= cc->xfer_len)
            { /* prepare to receive next command */
            cc->state = CMD_ST_RECV;
            cc->xfer_pos = 0;
            cc->xfer_len = CMD_REQUEST_HEADER_SIZE;
            }
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_cmd_init(void)
    {
    /* LIP must have been already initialized */

    ASSERT(N_CMD_CONN + 1 <= LIP_TCP_USER_SOCKET_CNT);
    for (int i = 0; i < N_CMD_CONN; i++)
        {
        f_cmd[i].buf = &f_cmd_buf[i];
        f_cmd[i].sock = lsock_create();
        /* Можно проверить ошибку if (sock < 0), но условие ASSERT должно гарантировать */
        lsock_set_recv_fifo(f_cmd[i].sock, f_sockbuf_cmd_rx[i], sizeof(f_sockbuf_cmd_rx[i]));
        lsock_set_send_fifo(f_cmd[i].sock, f_sockbuf_cmd_tx[i], sizeof(f_sockbuf_cmd_tx[i]));
        if (g_conf_user.net_options & CONF_NETOPT_NODELAY_CMD)
            {
            int nodelay = 1;
            lsock_set_opt(f_cmd[i].sock, IPPROTO_TCP, TCP_NODELAY, &nodelay, sizeof(nodelay));
            }
        lsock_listen(f_cmd[i].sock, TCPSERVER_CMD_PORT);
        f_cmd[i].last_sock_state = lsock_state(f_cmd[i].sock);
        f_cmd[i].state = CMD_ST_CLOSED;
        }

    f_primary_conn_ptr = NULL;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_cmd_worker(void)
    {
    static cmd_ctl_t* cc = &f_cmd[0];

    lip_pull();

    f_cmd_sockstate_monitor(cc);
    if (cc->state != CMD_ST_CLOSED)
        f_cmd_worker(cc);

    if (++cc >= f_cmd + N_CMD_CONN)
        cc = &f_cmd[0];
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void tcpserver_cmd_hangup(void)
    {
    for (int i = 0; i < N_CMD_CONN; i++)
        {
        f_cmd[i].state = CMD_ST_CLOSED;
        lsock_close(f_cmd[i].sock, TCPSERVER_HANGUP_TIMEOUT_MS);
        }
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
