/*================================================================================================*
 * Управление сигналами синхронизации, DIGIN/DIGOUT и дополнительными входами/выходами
 *================================================================================================*/

#include "aux_io.h"
#include "settings.h"

/*================================================================================================*/
#define GPIO_INIT_IN(pin, mode) \
    do { LPC_PIN_CONFIG((pin), 1, (mode)); LPC_PIN_DIR_IN(pin); } while (0)
#define GPIO_INIT_OUT(pin, mode, val) \
    do { LPC_PIN_OUT((pin), (val)); LPC_PIN_DIR_OUT(pin); LPC_PIN_CONFIG((pin), 0, (mode)); } while (0)

#define SCT_WIDTH_START     CYCLES_NS_ROUND(LPC_CLK_SCT, 500) /* ширина импульса "старт" в тактах SCT */

/* SCT имеет 8-bit prescaler, т.е. в 16-битном режиме период счетчика может быть не более
   256 * 65536 = 16777216 тактов LPC_SYSCLK
   Для отсчета интервала в 1с используется цепочка состояний SCT (всего их может быть до 32).

   1. Prescaler ставится так, чтобы получить такт таймера 1 мкс.
   2. Период в 1000000 тактов делится на 20*50000
   3. Выход в 1 по условию STATE == 0, COUNTER == 0
   4. Выход в 0 по условию STATE == t/50000, COUNTER == t%50000 (t = длительность импульса, мкс)
   5. После 49999 тактов COUNTER = 0, STATE++ (для всех состояний, кроме последнего)
   6. В последнем состоянии COUNTER = 0, STATE = 0
 */
#define SCT_1MHZ            1000000UL
#define SCT_STATEDUR_1S     50000UL         /* кол-во тактов таймера на одно состояние, <= 65536 */
#define SCT_WIDTH_1S        470000UL        /* ширина импульса "секунда" (в тактак по 1 мкс) */
#define SCT_PERIOD_1S       1000000UL       /* период импульса "секунда" (в тактах по 1 мкс) */

#define SCT_CLKDIV_1S       (LPC_CLK_SCT / SCT_1MHZ)
#define SCT_NSTATES_1S      ((SCT_PERIOD_1S + SCT_STATEDUR_1S - 1) / SCT_STATEDUR_1S)

/* Состояния и значения счетчика, при которых меняется выход */
#define SCT_SET1_STATE_1S   0
#define SCT_SET1_COUNT_1S   0
#define SCT_SET0_STATE_1S   (SCT_WIDTH_1S / SCT_STATEDUR_1S)
#define SCT_SET0_COUNT_1S   (SCT_WIDTH_1S % SCT_STATEDUR_1S)
#define SCT_1SHOT_STATE_1S  31  /* состояние для однократных импульсов на выходе СЕКУНДА */

#if (LPC_CLK_SCT % SCT_1MHZ) || (LPC_CLK_SCT > 256 * SCT_1MHZ)
#error "SCT clock must be divisible by 1 MHz and no more than 256 MHz for 1s-mark counter"
#endif

#if SCT_NSTATES_1S > SCT_1SHOT_STATE_1S
#error "Not enough states to cover SCT_PERIOD_1S, increase prescaler or increase SCT_STATEDUR_1S"
#endif

#if SCT_WIDTH_1S > SCT_PERIOD_1S
#error "Invalid 1S pulse width"
#endif

/* Константы для 16-битных регистров CTRL_L, CTRL_H */
#define SCT_CTRL_STOP       SCT_CTRL_STOP_L
#define SCT_CTRL_HALT       SCT_CTRL_HALT_L
#define SCT_CTRL_CLRCTR     SCT_CTRL_CLRCTR_L
#define SCT_CTRL_BIDIR(x)   SCT_CTRL_BIDIR_L(x)
#define SCT_CTRL_PRE(x)     SCT_CTRL_PRE_L(x)

/* Установка MATCH и MATCHREL в одинаковое значение */
#define LPC_SCT_MATCH(n, L_or_H, val) \
    do { LPC_SCT->MATCH[n].L_or_H = (val); LPC_SCT->MATCHREL[n].L_or_H = (val); } while (0)
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void aux_init(void)
    {
    //@@ Пока не используются дополнительные UART на ножках DIGIN/DIGOUT
    //@@ Вероятно, здесь должна быть инициализация портов UART1 и UART2
    GPIO_INIT_IN(SET_PIN_DIGIN1_GPIO, LPC_PIN_PULLDN);
    GPIO_INIT_IN(SET_PIN_DIGIN2_GPIO, LPC_PIN_PULLDN);
    GPIO_INIT_OUT(SET_PIN_DTR1_GPIO, LPC_PIN_NOPULL, 0);
    
    //@@ Пока не используется ножка IRIG
    GPIO_INIT_IN(SET_PIN_IRIG_IN, LPC_PIN_PULLDN);

    /* Настройка таймера SCT : L = секунда, H = старт */

#if SET_AUX_CTOUT_OR_WITH_TIMER
    LPC_CREG->CREG6 &= ~(1U << 4);  /* CTOUTCTRL = 0, OR CTOUTx with timer match */
#else
    LPC_CREG->CREG6 |= (1U << 4);   /* CTOUTCTRL = 1, don't OR CTOUTx with timer match */
#endif

#if 1
    /* Reset SCT */
    LPC_RGU->RESET_CTRL1 = ~LPC_RGU->RESET_ACTIVE_STATUS1 | (1UL << (RGU_SCT_RST - 32));
    while (!(LPC_RGU->RESET_ACTIVE_STATUS1 & (1UL << (RGU_SCT_RST - 32))));
#endif

    LPC_SCT->CONFIG = SCT_CONFIG_16BIT_COUNTER | SCT_CONFIG_CLKMODE_BUSCLK;
    LPC_SCT->CTRL_U |= (SCT_CTRL_HALT_L | SCT_CTRL_HALT_H);

    LPC_SCT->OUTPUTDIRCTRL = 0; /* no inverse set/clear depending on direction */
    /* no interrupts or DMA requests */
    LPC_SCT->DMA0REQUEST = 0;
    LPC_SCT->DMA1REQUEST = 0;
    LPC_SCT->EVEN = 0;
    LPC_SCT->CONEN = 0;

    /* Все выходы SCT поставить принудительно в 0 */
    LPC_SCT->OUTPUT = 0;

    /* ---- Configure LOW counter (секунда) ---- */

    LPC_SCT->CTRL_L = SCT_CTRL_HALT | SCT_CTRL_CLRCTR | SCT_CTRL_BIDIR(0) | SCT_CTRL_PRE((SCT_CLKDIV_1S - 1));
    LPC_SCT->REGMODE_L = 0; /* all match, no capture */
    LPC_SCT->STATE_L = 0;

    /* event 0: set output to 1 */
    LPC_SCT_MATCH(0, L, SCT_SET1_COUNT_1S);
    LPC_SCT->EVENT[0].STATE = (1UL << SCT_SET1_STATE_1S);
    LPC_SCT->EVENT[0].CTRL = SCT_EVCTRL_MATCHSEL(0) | SCT_EVCTRL_COMBMODE_MATCH | SCT_EVCTRL_STATE_ADD(0);

    /* event 1: set output to 0 */
    LPC_SCT_MATCH(1, L, SCT_SET0_COUNT_1S);
    LPC_SCT->EVENT[1].STATE = (1UL << SCT_SET0_STATE_1S);
    LPC_SCT->EVENT[1].CTRL = SCT_EVCTRL_MATCHSEL(1) | SCT_EVCTRL_COMBMODE_MATCH | SCT_EVCTRL_STATE_ADD(0);

    /* event 2: increment state at SCT_STATEDUR_1S and reset counter in all states but the last */
    LPC_SCT_MATCH(2, L, SCT_STATEDUR_1S - 1);
    LPC_SCT->EVENT[2].STATE = (1UL << (SCT_NSTATES_1S - 1)) - 1;
    LPC_SCT->EVENT[2].CTRL = SCT_EVCTRL_MATCHSEL(2) | SCT_EVCTRL_COMBMODE_MATCH | SCT_EVCTRL_STATE_ADD(1);

    /* event 3: reset state and counter to 0 in the last state */
    LPC_SCT_MATCH(3, L, SCT_PERIOD_1S - 1 - SCT_STATEDUR_1S * (SCT_NSTATES_1S - 1));
    LPC_SCT->EVENT[3].STATE = (1UL << (SCT_NSTATES_1S - 1));
    LPC_SCT->EVENT[3].CTRL = SCT_EVCTRL_MATCHSEL(3) | SCT_EVCTRL_COMBMODE_MATCH | SCT_EVCTRL_STATE_SET(0);

    /* events 4,5 are for 1-shot pulse (like START) and for stopping */
    
    /* event 4: set output to 1 at COUNTER == 0 in 1-shot state */
    LPC_SCT_MATCH(4, L, 0);
    LPC_SCT->EVENT[4].STATE = (1UL << SCT_1SHOT_STATE_1S);
    LPC_SCT->EVENT[4].CTRL = SCT_EVCTRL_MATCHSEL(4) | SCT_EVCTRL_COMBMODE_MATCH | SCT_EVCTRL_STATE_ADD(0);

    /* event 5: set output to 0, state to 0, and stop */
    LPC_SCT_MATCH(5, L, SCT_WIDTH_START);
    LPC_SCT->EVENT[5].STATE = (1UL << SCT_1SHOT_STATE_1S);
    LPC_SCT->EVENT[5].CTRL = SCT_EVCTRL_MATCHSEL(5) | SCT_EVCTRL_COMBMODE_MATCH | SCT_EVCTRL_STATE_SET(0);

    /* which events set and clear output */
    LPC_SCT->OUT[SET_AUX_MARK_1S_CTOUTn].SET = (1U << 0) | (1U << 4);
    LPC_SCT->OUT[SET_AUX_MARK_1S_CTOUTn].CLR = (1U << 1) | (1U << 5);
    /* which events affect the counter */
    LPC_SCT->LIMIT_L = (1U << 2) | (1U << 3);
    LPC_SCT->HALT_L = (1U << 5);
    LPC_SCT->STOP_L = 0;
    LPC_SCT->START_L = 0;

    /* ---- Configure HIGH counter (старт) ---- */

    LPC_SCT->CTRL_H = SCT_CTRL_HALT | SCT_CTRL_CLRCTR | SCT_CTRL_BIDIR(0) | SCT_CTRL_PRE(0);
    LPC_SCT->REGMODE_H = 0; /* all match, no capture */
    LPC_SCT->STATE_H = 0;

    /* event 8: set output to 1 at COUNTER == 0 */
    LPC_SCT_MATCH(8, H, 0);
    LPC_SCT->EVENT[8].STATE = (1UL << 0);
    LPC_SCT->EVENT[8].CTRL = SCT_EVCTRL_HEVENT | 
                             SCT_EVCTRL_MATCHSEL(8) | SCT_EVCTRL_COMBMODE_MATCH | SCT_EVCTRL_STATE_ADD(0);

    /* event 9: set output to 0, state to 0, and stop */
    LPC_SCT_MATCH(9, H, SCT_WIDTH_START);
    LPC_SCT->EVENT[9].STATE = (1UL << 0);
    LPC_SCT->EVENT[9].CTRL = SCT_EVCTRL_HEVENT | 
                             SCT_EVCTRL_MATCHSEL(9) | SCT_EVCTRL_COMBMODE_MATCH | SCT_EVCTRL_STATE_SET(0);

    /* which events set and clear output */
    LPC_SCT->OUT[SET_AUX_MARK_START_CTOUTn].SET = (1U << 8);
    LPC_SCT->OUT[SET_AUX_MARK_START_CTOUTn].CLR = (1U << 9);
    /* which events affect the counter */
    LPC_SCT->LIMIT_H = 0;
    LPC_SCT->HALT_H = (1U << 9); /* event 9 halts counter */
    LPC_SCT->STOP_H = 0;
    LPC_SCT->START_H = 0;

    /* Ножки синхрометок */
    LPC_PIN_CONFIG(SET_PIN_MARK_START, 0, LPC_PIN_NOPULL);
    LPC_PIN_CONFIG(SET_PIN_MARK_1S, 0, LPC_PIN_NOPULL);

    /* Ножки DIGOUTx первоначально ставятся в состояние "константа 0" */
    GPIO_INIT_OUT(SET_PIN_DIGOUT1_GPIO, LPC_PIN_NOPULL, 0);
    GPIO_INIT_OUT(SET_PIN_DIGOUT2_GPIO, LPC_PIN_NOPULL, 0);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void aux_digout_set_gpio(enum aux_digout digout_n, uint8_t val)
    {
    if (AUX_DIGOUT1 == digout_n)
        GPIO_INIT_OUT(SET_PIN_DIGOUT1_GPIO, LPC_PIN_NOPULL, val);
    else
        GPIO_INIT_OUT(SET_PIN_DIGOUT2_GPIO, LPC_PIN_NOPULL, val);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void aux_digout_set_uarttx(enum aux_digout digout_n)
    {
    if (AUX_DIGOUT1 == digout_n)
        LPC_PIN_CONFIG(SET_PIN_DIGOUT1_UART, 0, LPC_PIN_PULLUP);
    else
        LPC_PIN_CONFIG(SET_PIN_DIGOUT2_UART, 0, LPC_PIN_PULLUP);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void aux_mark_start_off(void)       /* Остановка генерации меток СТАРТ (выход в 0) */
    {
    LPC_SCT->CTRL_H |= SCT_CTRL_HALT;
    /* Очистка выхода установкой счетчика в конечное значение (чтобы он сразу очистил выход) */
    LPC_SCT->COUNT_H = SCT_WIDTH_START;
    LPC_SCT->STATE_H = 0;
    LPC_SCT->CTRL_H = SCT_CTRL_BIDIR(0) | SCT_CTRL_PRE(0);
    /* Дождаться остановки */
    while (!(LPC_SCT->CTRL_H & SCT_CTRL_HALT));
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void aux_mark_start_go(void)        /* Генерация метки СТАРТ */
    {
    LPC_SCT->CTRL_H |= SCT_CTRL_HALT;
    LPC_SCT->STATE_H = 0;
    LPC_SCT->CTRL_H = SCT_CTRL_CLRCTR | SCT_CTRL_BIDIR(0) | SCT_CTRL_PRE(0);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void aux_mark_1s_off(void)          /* Остановка генерации секундных меток (выход в 0) */
    {
    LPC_SCT->CTRL_L |= SCT_CTRL_HALT;
    /* Очистка выхода установкой счетчика в конечное значение (чтобы он сразу очистил выход) */
    LPC_SCT->COUNT_L = SCT_WIDTH_START;
    LPC_SCT->STATE_L = SCT_1SHOT_STATE_1S;
    LPC_SCT->CTRL_L = SCT_CTRL_BIDIR(0) | SCT_CTRL_PRE(0);
    /* Дождаться остановки */
    while (!(LPC_SCT->CTRL_L & SCT_CTRL_HALT));
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void aux_mark_1s_go_single(void)    /* Генерация однократной метки на выходе СЕКУНДА */
    {
    LPC_SCT->CTRL_L |= SCT_CTRL_HALT;
    LPC_SCT->STATE_L = SCT_1SHOT_STATE_1S;
    LPC_SCT->CTRL_L = SCT_CTRL_CLRCTR | SCT_CTRL_BIDIR(0) | SCT_CTRL_PRE(0);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void aux_mark_1s_go(void)           /* Запуск периодических секундных меток */
    {
    LPC_SCT->CTRL_L |= SCT_CTRL_HALT;
    LPC_SCT->STATE_L = 0;
    LPC_SCT->CTRL_L = SCT_CTRL_CLRCTR | SCT_CTRL_BIDIR(0) | SCT_CTRL_PRE((SCT_CLKDIV_1S - 1));
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
