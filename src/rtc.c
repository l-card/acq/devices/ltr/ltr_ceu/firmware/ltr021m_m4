/*================================================================================================*
 * Поддержка часов реального времени
 *================================================================================================*/

#include "rtc.h"

/* Если поддержка RTC выключена, код не компилируется, функции заменяются inline заглушками */
#if RTC_SUPPORT_ENABLED

#include "microtimer.h"
#include <string.h>

/*================================================================================================*/
#define CREG0_EN1KHZ        (1U << 0)
#define CREG0_EN32KHZ       (1U << 1)
#define CREG0_REST32KHZ     (1U << 2)
#define CREG0_PD32KHZ       (1U << 3)

#define CREG0_MASK          (CREG0_EN1KHZ | CREG0_EN32KHZ | CREG0_REST32KHZ | CREG0_PD32KHZ)
#define CREG0_VAL_RTC_ON    (CREG0_EN1KHZ | CREG0_EN32KHZ)
#define CREG0_VAL_RTC_OFF   (CREG0_REST32KHZ | CREG0_PD32KHZ)

#define RTC_INIT_DELAY_uS   2000000     /* Задержка после включения кварца RTC */

/* NB: 1. В один и тот же регистр RTC нельзя писать два раза в течение 1 тика таймера,
       то есть это значит, что после записи регистра нужно ждать СЕКУНДУ, прежде чем можно
       будет писать в этот регистр опять. Иначе цикл шины APB может зависнуть на секунду.

       2. Судя по тестам, из-за привязки к 1 Гц бит CTCRST не работает, поменять фазу
       опоры 1 Гц не получается. То есть точность установки времени 1 с.
       Добиться более точной установки часов удалось только так:
       - по первой команде от ПК останавливаются 32 кГц осциллятор, причем не сразу, а в момент
         смены секунды (внутренний невидимый prescaler около 0);
       - ПК должен дождаться статуса готовности (до 1 с): часы остановлены, можно ставить время;
       - по второй команде ставится время и запускается кварц. При этом время должно быть +1 с
         относительно реального
       То есть алгоритм для верхнего уровня условно выглядит так:
       if (!IOCTL("rtc_set_prepare")) error;
       while !IOCTL("rtc_ready_to_set") { if (timeout) error; }
       do { T = gettime() } while (T.millisec > epsilon); (дождаться начала секунды на ПК)
       T += 1 s;
       if (!IOCTL("rtc_set_time", T)) error;
 */
/*================================================================================================*/

/*================================================================================================*/
#define FL_PRESENT      0x01    /* RTC присутствует */
#define FL_STOPPED      0x02    /* часы остановлены */
#define FL_NEED_STOP    0x04    /* запрос на остановку часов в начале следующей секунды */
#define FL_RESTORE_CCR  0x08    /* нужно восстановить регистр CCR после сброса CALIBRATION */
#define FL_TIME_WAIT    0x10    /* выдержка времени после установки часов */
#define FL_SET(var, x)  ((var) |= (typeof(var))(x))
#define FL_CLR(var, x)  ((var) &= (typeof(var))~(x))

static uint8_t f_flags = 0;
static uint8_t f_prev_sec;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static inline unsigned int f_month_len
    (
    unsigned int month,
    unsigned int leap_year
    )
    {
    const uint8_t month_len[12] =
        { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    return month_len[month - 1] + (unsigned int)((2 == month) && leap_year);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static inline unsigned int f_day_of_week
    (
    unsigned int d, unsigned int m, unsigned int y
    )
    {
    if (m < 3) { y--; m += 12; }
    return (y + y / 4 - y / 100 + y / 400 + 26 * (m + 1) / 10 - 1 + d) % 7;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/* Регистр CALIBRATION:
    caldir = CALIBRATION.bit17
    calval = CALIBRATION.bit[16:0]
    "+": caldir == 0, добваляется +1 с через каждые calval + 1 секунд
    "-": caldir == 1, пропускается 1 с через каждые calval + 1 секунд
    Поправка (с/сут):
    "+": +86400 / (calval + 1)
    "-": -86400 / (calval + 1)
    Если взять со знаком int_val = (calval + 1) * (caldir) ? -1 : 1
    "+": int_val > 1, +86400 / int_val с/сут, calval = int_val - 1, caldir = 0
    "-": int_val < -1, +86400 / int_val с/сут, calval = -int_val - 1, caldir = 1
    "0": int_val = 0, calval = 0, caldir = 0
 */
/*------------------------------------------------------------------------------------------------*/
static int32_t f_calib_reg_to_int(uint32_t calib_reg_val)
    {
    int32_t int_val = (int32_t)(calib_reg_val & 0x1FFFF);
    if (int_val)
        {
        int_val++;
        if (calib_reg_val & (1UL << 17)) int_val = -int_val;
        }
    return int_val;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static uint32_t f_int_to_calib_reg(int32_t int_val)
    {
    uint32_t calval;
    if (!int_val) return 0;
    calval = (uint32_t)((int_val > 0) ? int_val : -int_val) - 1;
    if (calval > 0x1FFFF) return 0;
    if (int_val < 0) calval |= (1UL << 17);
    return calval;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static inline void f_wait_start(void)
    {
    LPC_ATIMER->DOWNCOUNTER = 1024;
    LPC_ATIMER->CLR_STAT = 1;
    }

static inline int f_wait_done(void)
    {
    return (LPC_ATIMER->STATUS & 1);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void rtc_init                       /* Инициализация */
    (void)
    {
    f_flags = 0;

    microtimer_init();

    /* Enable RTC clocks */
    if ((LPC_CREG->CREG0 & CREG0_MASK) != CREG0_VAL_RTC_ON)
        { /* RTC был выключен (вынималась батарея) */
        LPC_CREG->CREG0 = (LPC_CREG->CREG0 & ~CREG0_MASK) | CREG0_VAL_RTC_ON;
        /* Задержка после включения кварца на 2 секунды (!) - по user manual */
        ASSERT(RTC_INIT_DELAY_uS <= MICROTIMER_MAX_uS);
        for (uint32_t tstart = microtimer_now();
            microtimer_since(tstart) < MICROTIMER_uS(RTC_INIT_DELAY_uS); )
            {
            lpc_wdt_reset();
            }
        }

    /* Проверить, работает ли RTC */
#define TEST_DC_NCYCLES 16
    LPC_ATIMER->CLR_EN = 1;
    LPC_ATIMER->PRESET = 0xFFFF;
    unsigned int dc = LPC_ATIMER->DOWNCOUNTER;
    for (uint32_t tstart = microtimer_now();
        microtimer_since(tstart) < MICROTIMER_CLK * 1ULL * TEST_DC_NCYCLES / 1024; )
        {
        lpc_wdt_reset();
        }
    dc = (dc - LPC_ATIMER->DOWNCOUNTER) & 0xFFFF;
    if ((dc < TEST_DC_NCYCLES * 7 / 8) || (dc > TEST_DC_NCYCLES * 9 / 8))
        {
        LPC_CREG->CREG0 = (LPC_CREG->CREG0 & ~CREG0_MASK) | CREG0_VAL_RTC_OFF;
        return;
        }

    LPC_RTC->CIIR = 0;                      /* disable interrupts */
    LPC_RTC->AMR = RTC_AMR_CIIR_BITMASK;    /* disable alarm */
    LPC_RTC->ILR = RTC_IRL_RTCCIF | RTC_IRL_RTCALF; /* clear interrupts */
    LPC_RTC->CCR = RTC_CCR_CLKEN;

    f_flags = FL_PRESENT | FL_TIME_WAIT;
    /* Подождать 1с для синхронизации */
    f_wait_start();
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int rtc_get_time                    /* Чтение часов */
    (
    rtc_time_t* p_tm                /* (out) Текущее время */
    )                               /* Возвращает 1 = ok, 0 = ошибка */
    {
    uint32_t ct[3][2];
    uint32_t *pct;

    if (!p_tm) return 0;
    memset(p_tm, 0, sizeof(*p_tm));

    if (!(f_flags & FL_PRESENT)) return 0;

    /* Регистры RTC меняются синхронно (не должно быть промежуточных значений, где изменилась
       только часть бит), но между собой регистры не связаны, т.е. в 23:59:59 при чтении
       CTIME[0] и CTIME[1] может получиться рассинхронизированная пара */
    
    /* Чтение consolidated time три раза */
    ct[0][0] = LPC_RTC->CTIME[0]; ct[0][1] = LPC_RTC->CTIME[1];
    ct[1][0] = LPC_RTC->CTIME[0]; ct[1][1] = LPC_RTC->CTIME[1];
    ct[2][0] = LPC_RTC->CTIME[0]; ct[2][1] = LPC_RTC->CTIME[1];

    /* Возможны следующие варианты при изменении времени между чтениями
       (! = момент изменения):
       a1!b2   a1 b1!  a1 b1   a1 b1   a1 b1   a1 b1
       a2 b2   a2 b2   a1!b2   a1 b1!  a1 b1   a1 b1
       a2 b2   a2 b2   a2 b2   a2 b2   a1!b2   a1 b1
    
       Если средняя пара совпадает с первой или последней (два одинаковых значения подряд),
       то берем среднюю пару:
       a2 b2   a2 b2           a1 b1   a1 b1   a1 b1

       Если не совпадает (случай (a1,b1), (a1,b2), (a2,b2)), то берем последнюю пару:
                       a2 b2
    */

    pct = &ct[1][0];
    if (((pct[0] != ct[0][0]) || (pct[1] != ct[0][1]))
        && ((pct[0] != ct[2][0]) || (pct[1] != ct[2][1])))
        pct = &ct[2][0];
    
    p_tm->second = (uint8_t)((pct[0] & RTC_CTIME0_SECONDS_MASK) >> RTC_CTIME0_SECONDS_POS);
    p_tm->minute = (uint8_t)((pct[0] & RTC_CTIME0_MINUTES_MASK) >> RTC_CTIME0_MINUTES_POS);
    p_tm->hour =   (uint8_t)((pct[0] & RTC_CTIME0_HOURS_MASK) >> RTC_CTIME0_HOURS_POS);
    p_tm->dow =    (uint8_t)((pct[0] & RTC_CTIME0_DOW_MASK) >> RTC_CTIME0_DOW_POS);
    p_tm->day =    (uint8_t)((pct[1] & RTC_CTIME1_DOM_MASK) >> RTC_CTIME1_DOM_POS);
    p_tm->month =  (uint8_t)((pct[1] & RTC_CTIME1_MONTH_MASK) >> RTC_CTIME1_MONTH_POS);
    p_tm->year =  (uint16_t)((pct[1] & RTC_CTIME1_YEAR_MASK) >> RTC_CTIME1_YEAR_POS);

    p_tm->calib = f_calib_reg_to_int(LPC_RTC->CALIBRATION);

    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int rtc_set_prepare                 /* Подготовка к установке часов (остановка) */
    (void)                          /* Возвращает 1 = ok, 0 = ошибка */
    {
    if (!(f_flags & FL_PRESENT)) return 0;

    if (!(f_flags & (FL_NEED_STOP | FL_STOPPED)))
        {
        f_prev_sec = (uint8_t)LPC_RTC->TIME[RTC_TIMETYPE_SECOND];
        FL_SET(f_flags, FL_NEED_STOP);
        }

    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int rtc_ready_to_set                /* Проверка, что часы готовы к установке */
    (void)                          /* Возвращает 1 = ok, 0 = не готов или не было rtc_set_prepare */
    {
    return ((f_flags & (FL_PRESENT | FL_STOPPED)) == (FL_PRESENT | FL_STOPPED));
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int rtc_set_time                    /* Установка часов после rtc_set_prepare(), rtc_ready_to_set() */
    (
    const rtc_time_t* p_tm          /* Устанавливаемое время (= текущее время + 1 с) */
    )                               /* Возвращает 1 = ok, 0 = ошибка */
    {
    unsigned int leap_year, doy, month;

    if (!p_tm || !rtc_ready_to_set()) return 0;

    if ((p_tm->hour > 23) || (p_tm->minute > 59) || (p_tm->second > 59)) return 0;
    if ((p_tm->year < 2000) || (p_tm->month < 1) || (p_tm->month > 12)) return 0;

    leap_year = !(p_tm->year % 4) && (!(p_tm->year % 400) || (p_tm->year % 100));
    month = p_tm->month;

    if ((p_tm->day < 1) || (p_tm->day > f_month_len(month, leap_year)))
        return 0;

    doy = p_tm->day;
    while (--month)
        {
        doy += f_month_len(month, leap_year);
        }

    /* Пока кварц остановлен, нельзя писать ни в какой регистр больше одного раза! */

    if (p_tm->calib != RTC_CALIB_UNCHANGED)
        {
        LPC_RTC->CCR |= RTC_CCR_CCALEN;
        LPC_RTC->CALIBRATION = f_int_to_calib_reg(p_tm->calib);
        /* начать отсчет интервала в 1 с */
        FL_SET(f_flags, FL_RESTORE_CCR);
        }

    LPC_RTC->TIME[RTC_TIMETYPE_SECOND]      = p_tm->second;
    LPC_RTC->TIME[RTC_TIMETYPE_MINUTE]      = p_tm->minute;
    LPC_RTC->TIME[RTC_TIMETYPE_HOUR]        = p_tm->hour;
    LPC_RTC->TIME[RTC_TIMETYPE_DAYOFMONTH]  = p_tm->day;
    LPC_RTC->TIME[RTC_TIMETYPE_DAYOFWEEK]   = f_day_of_week(p_tm->day, p_tm->month, p_tm->year);
    LPC_RTC->TIME[RTC_TIMETYPE_DAYOFYEAR]   = doy;
    LPC_RTC->TIME[RTC_TIMETYPE_MONTH]       = p_tm->month;
    LPC_RTC->TIME[RTC_TIMETYPE_YEAR]        = p_tm->year;

    /* Запустить часы */
    LPC_CREG->CREG0 &= ~CREG0_REST32KHZ;

    FL_CLR(f_flags, FL_STOPPED);

    /* Начать отсчет интервала, в течение которого не принимаются новые запросы на остановку */
    FL_SET(f_flags, FL_TIME_WAIT);
    f_wait_start();

    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void rtc_worker                     /* Фоновая процедура */
    (void)
    {
    if (!(f_flags & FL_PRESENT)) return;

    if (f_flags & FL_NEED_STOP)
        {
        uint8_t sec = (uint8_t)LPC_RTC->TIME[RTC_TIMETYPE_SECOND];
        if (!(f_flags & FL_TIME_WAIT) && (sec != f_prev_sec))
            { /* недавно сменилась секунда, на этом месте остановить кварц */
            LPC_CREG->CREG0 |= CREG0_REST32KHZ;
            FL_CLR(f_flags, FL_NEED_STOP);
            FL_SET(f_flags, FL_STOPPED);
            }
        else
            {
            f_prev_sec = sec;
            }
        }

    if ((f_flags & FL_TIME_WAIT) && f_wait_done())
        {
        if (f_flags & FL_RESTORE_CCR)
            {
            LPC_RTC->CCR &= ~(uint32_t)RTC_CCR_CCALEN;
            FL_CLR(f_flags, FL_RESTORE_CCR);
            f_wait_start(); /* подождать еще секунду */
            }
        else
            {
            FL_CLR(f_flags, FL_TIME_WAIT);
            }
        }
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
#endif /* RTC_SUPPORT_ENABLED */
