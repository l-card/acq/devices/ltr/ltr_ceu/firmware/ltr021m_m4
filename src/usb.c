/*================================================================================================*/
/* Файл содержит логику работы USB-интерфейсом.
 * Работа идет с использованием lusb_embedded, которая обработывает все
 * стандартные запросы и дает не зависящий от аппаратуры интерфейс для работы с USB.
 *
 * В данном файле содержится логика обработки пользовательских запросов и
 * логика управления светодиодом индикации состояния USB */
/*================================================================================================*/

#include "usb.h"
#include "global.h"
#include "ctlfunc.h"
#include "fwinfo.h"
#include "settings.h"
#include <stdlib.h>

/*================================================================================================*/
enum en_usb_ioctl_vendor_request {
    USB_CTL_O_RESET_FPGA        = 0,
    USB_CTL_O_PUT_ARRAY         = 1,
    USB_CTL_I_GET_ARRAY         = 2,
    USB_CTL_O_INIT_FPGA         = 3,
    USB_CTL_O_INIT_DMA          = 4,
    USB_CTL_I_GET_MODULE_NAME   = 11,
    USB_CTL_O_CALL_APPLICATION  = 15
};

#define USB_RX_DD_SIZE     512

typedef enum {
    USB_LED_STATE_DISCON = 0x03,
    USB_LED_STATE_FS     = 0x01,
    USB_LED_STATE_HS     = 0x02,
    USB_LED_STATE_OFF    = 0x00
} t_usb_led_state;

static t_usb_led_state f_led_state;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static inline void f_led_set(t_usb_led_state state) {
    LPC_PIN_OUT(SET_PIN_USB_RED, state & 1);
    LPC_PIN_OUT(SET_PIN_USB_GREEN, (state >> 1) & 1);
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_led_set_state(t_usb_led_state state) {
    f_led_set(state);
    f_led_state = state;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void usb_init(void) {
    LPC_PIN_CONFIG(SET_PIN_USB_GREEN, 0, LPC_PIN_PULLUP);
    LPC_PIN_DIR_OUT(SET_PIN_USB_GREEN);
    LPC_PIN_CONFIG(SET_PIN_USB_RED, 0, LPC_PIN_PULLUP);
    LPC_PIN_DIR_OUT(SET_PIN_USB_RED);
    f_led_set_state(USB_LED_STATE_DISCON);
    lusb_init();
    lusb_connect(1);
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void lusb_appl_cb_activity_start(void) {
    f_led_set(USB_LED_STATE_OFF);
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void lusb_appl_cb_activity_end(void) {
    f_led_set(f_led_state);
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int lusb_appl_cb_custom_ctrlreq_rx(const t_lusb_req *req, uint8_t *buf) {
    int res = LUSB_ERR_UNSUPPORTED_REQ;
    if ((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_VENDOR) {
        switch (req->request) {
            case USB_CTL_O_RESET_FPGA:
                ctlfunc_reset_fpga();
                res = LUSB_ERR_SUCCESS;
                break;

            case USB_CTL_O_PUT_ARRAY:
                if (ctlfunc_put_array(CONF_IFACE_USB,
                    ((uint32_t)req->index << 16) | req->val, req->length, buf))
                    res = LUSB_ERR_SUCCESS;
                break;

            /* Эмулируемые для совместимости команды, которые ничего не делают */
            case USB_CTL_O_INIT_FPGA:
            case USB_CTL_O_INIT_DMA:
            case USB_CTL_O_CALL_APPLICATION:
                res = LUSB_ERR_SUCCESS;
                break;
        }
    }
    return res;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
const void* lusb_appl_cb_custom_ctrlreq_tx(const t_lusb_req *req, int *p_length, uint8_t *tx_buf) {
    const void* wr_buf = NULL;
    if ((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_VENDOR) {
        switch (req->request) {
            case USB_CTL_I_GET_ARRAY:
                if ((req->length <= LUSB_EP0_BUF_SIZE) &&
                    ctlfunc_get_array(CONF_IFACE_USB,
                        ((uint32_t)req->index << 16) | req->val, req->length, tx_buf)) {
                    wr_buf = tx_buf;
                    *p_length = req->length;
                }
                break;

            case USB_CTL_I_GET_MODULE_NAME:
                wr_buf = ctlfunc_get_module_name(p_length);
                break;
        }
    }
    return wr_buf;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void lusb_appl_cb_devstate_ch(uint8_t old_state, uint8_t new_state) {
    /* Connected = DEFAULT bit set, SUSPEND bit not set */
    int new_on =
        ((new_state & (LUSB_DEVSTATE_DEFAULT | LUSB_DEVSTATE_SUSPENDED)) == LUSB_DEVSTATE_DEFAULT);
    f_led_set_state((new_on)
            ? (lusb_get_speed() == LUSB_SPEED_HIGH) ? USB_LED_STATE_HS : USB_LED_STATE_FS
            : USB_LED_STATE_DISCON);
}
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
