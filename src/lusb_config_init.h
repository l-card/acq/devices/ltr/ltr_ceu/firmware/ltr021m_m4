/*
 * lusb_config.h
 *
 *  Файл содержит пример настроек стека lusb.
 *  Каждый проект, который использует стек lusb должен включать
 *  свою копию данного файла с нужными настройками
 *
 *  Created on: 05.07.2010
 *      Author: borisov
 */

#ifndef __LUSB_CONFIG_INIT_H__
#define __LUSB_CONFIG_INIT_H__

#include "fwinfo.h"
#include "lusb_usbdefs.h"
#include "lprintf.h"
#include "dbg_config.h"

#if PRINT_DBG_LUSB
    #define lusb_printf lprintf
#endif

/* Количество десерипторов на каждую конечную точку */
#define LUSB_DMA_EP_DESCR_CNT 128
/* Количество конечных точек без 0-ой. Точки в 2 стороны с одним адресом считаются за 1.
 * Определяет по сути максимальный адрес кт */
#define LUSB_EP_NONCTL_CNT    2

/* размер буфера на прием для 0 кт
 * должен быть не меньше максимального размера данных
 * передаваемых от PC к устройству за управляющий запрос
  */
#define LUSB_EP0_BUF_SIZE  512

/* Если определен данный макрос, то пользовательские управляющие
 * запросы host->device обрабатываются по пакетам, а не целиком
 */
//#define LUSB_EP0_RX_PARTIAL
/* Если определен данный макрос, то пользовательские управляющие
 * запросы device->host обрабатываются по пакетам, а не целиком
 */
//#define LUSB_EP0_TX_PARTIAL


/* флаг означает, что для обработки событий USB-модуля
 * (проверка флагов, вызов callback, считывание/запись данных)
 * используются прерывания, если не определен - эти действия
 * выполняются опросом в lusb_progress
 */
#define LUSB_INTERRUPT


/* спецификатор, использующийся при объявлении дескрипторов в H файле
 * и их определении в C файле соответственно (по-умолчанию - const) */
//#define LUSB_DESCR_SPEC_H const
//#define LUSB_DESCR_SPEC_C const


//************************ настройки USB-устройства, определяемые дескрипторами USB *****************/
//настройки устройства (из Device Descriptor)
#define LUSB_DEVICE_CLASS           0
#define LUSB_DEVICE_SUBCLASS        0
#define LUSB_DEVICE_PROTOCOL        0
#define LUSB_DEVICE_VENDOR_ID       FWINFO_USB_VID
#define LUSB_DEVICE_PRODUCT_ID      FWINFO_USB_PID
#define LUSB_DEVICE_RELEASE         0x0100

//размер пакета 0 конечной точки (8,16,32,64)
#define LUSB_EP0_PACKET_SIZE  64

//настройки конфигурации
#define LUSB_CONFIG_CNT          1  //кол-во конфигураций
#define LUSB_CONFIG_ATTR         (USB_CONFIG_ATTR_SELFPOW) //атрибуты конфигурации SELFPOW и WU
#define LUSB_DEV_POW             0  //потребляемый ток в мА


//********************** пользовательские callback'и **********************************************/
#define LUSB_APPL_CB_CUSTOM_CTRLREQ_RX    //обработка нестандартного
                                          //управляющего запроса PC->Device
#define LUSB_APPL_CB_CUSTOM_CTRLREQ_TX   //подготовка данных для нестандартного
                                        //управляющего запроса Device->PC
//#define LUSB_APPL_CB_BUS_RESET         //возникновение bus reset
//#define LUSB_APPL_CB_CONNECT_CHANGED   //изменение состояния подключение
//#define LUSB_APPL_CB_SUSPEND_CHANGED   //вход/выход из suspend
//#define LUSB_APPL_CB_ENTER_TEST_MODE   //перевод модуля в тестовый режим запросом SET_FEATURE
#define LUSB_APPL_CB_DD_EVENT         //событие передачи данных
#define LUSB_APPL_CB_DEVSTATE_CHANGED  //изменение состояния устройства
                                         //(подключено/сконфигурировано)
//#define LUSB_APPL_CB_INTF_ALTSET_CHANGED /* изменение набора настроек для интерфейса */
#define LUSB_APPL_CB_ACTIVITY_IND      /* callback для отображения активности на шине.
                                          Требет таймера (или можно реализовать свои функции
                                          из lusb_activity_indication.c) */

/**********************************************************************
 *  настройки для стандартного интерфейса, который может
 *  использовать точки типа Bulk на прием и на передачу
 ***********************************************************************/
/* признак, что используется пользовательский интерфейс через bulk */
#define LUSB_USE_STDBULK_INTERFACE

/* Переопределяет класс интерфейса в дескрипторе (по-умолчанию - vendor-specific) */
//#define LUSB_STDBULK_INTERFACE_CLASS     0xFF
/* Переопределяет субкласс интерфейса в дескрипторе (по-умолчанию - 0) */
//#define LUSB_STDBULK_INTERFACE_SUBCLASS  0x10
/* Переопределяет протокол интерфейса в дескрипторе (по-умолчанию - vendor-specific) */
//#define LUSB_STDBULK_INTERFACE_PROTOCOL  0x01
/* количество конечных точек на прием и передачу */
#define LUSB_STDBULK_TX_EP_CNT 1
#define LUSB_STDBULK_RX_EP_CNT 1

/*  адреса для конечных точек на прием и передачу */
#if LUSB_EP_NONCTL_CNT > 1
#define LUSB_STDBULK_TX_EP_ADDR(i)      2
#define LUSB_STDBULK_RX_EP_ADDR(i)      1
#else
#define LUSB_STDBULK_TX_EP_ADDR(i)      1
#define LUSB_STDBULK_RX_EP_ADDR(i)      1
#endif

/* размер пакета конечной точки для обмена (bulk)
 * только для задания в LUSB_STDBULK_TX_EP_SIZE/LUSB_STDBULK_RX_EP_SIZE*/
#define LUSB_STDBULK_EPIO_SIZE        512

/* размеры конечных точек на прием и передачу */
#define LUSB_STDBULK_TX_EP_SIZE(i)    LUSB_STDBULK_EPIO_SIZE
#define LUSB_STDBULK_RX_EP_SIZE(i)    LUSB_STDBULK_EPIO_SIZE
/************************************** настройки DMA ********************************************/
 /*если определено - в стек включен код для использования DMA */
#define LUSB_USE_DMA

//*************************************** настройки для софтовой передачи ***************************
//#define LUSB_USE_SIO
#ifdef LUSB_USE_SIO
    //какие ep используют double buffering
    #define LUSB_EP_DB_MSK   (LUSB_EPFLAG(LUSB_EP1_ADDR(0)) | LUSB_EPFLAG(LUSB_EP2_ADDR(0)))
    #define LUSB_SIO_DD_CNT   16
#endif



//************************************  строки из строковых дескрипторов ****************************
/* если этот макрос определен, строки из дескрипторов
 * задаются просто константами LUSB_STR_XXX
 * (но такая инициализация может поддерживается не
 *  всеми компилятором - если это так, то данных макрос
 *  не нужно определять, а дескрипторы строк нужно определять
 * вручную в lusb_descriptors.c */
#define LUSB_DEF_UTF16_STRING

/* если данный макрос определен, то используются так же
 * строки для русского LANG ID */
#define LUSB_USE_RUS_STRINGS

/* если данный макрос определен, то строковый дескриптор
 * с серийным номером получается с помощью пользовательской
 * функции lusb_app_cb_get_serial()
 */
//#define LUSB_USE_MAN_GEN_SERIAL

/********************************************************
 * Определение строк для строковых дескрипторов
 ********************************************************/
#ifdef LUSB_DEF_UTF16_STRING
    /* при LUSB_DEF_UTF16_STRING определяем просто строки */
    #define LUSB_STR_MANUFACTURER     FWINFO_MANUFACTURER
    #define LUSB_STR_PRODUCT          FWINFO_DEVICE_NAME_GENERIC
    #define LUSB_STR_CONFIG           "Main Configuration"
    #define LUSB_STR_BULK_INTERFACE   FWINFO_DEVICE_NAME_GENERIC " Interface"

/*
    #ifndef LUSB_USE_MAN_GEN_SERIAL
        #define LUSB_STR_SERIAL           "00R123456"
    #else
        #define LUSB_MAX_SERIAL_SIZE       32
    #endif
*/
    #ifdef LUSB_USE_RUS_STRINGS
        #define LUSB_STR_MANUFACTURER_RUS      "Л Кард"
        #define LUSB_STR_PRODUCT_RUS           FWINFO_DEVICE_NAME_GENERIC
        #define LUSB_STR_CONFIG_RUS            "Основная конфигурация"
        #define LUSB_STR_BULK_INTERFACE_RUS    "Интерфейс " FWINFO_DEVICE_NAME_GENERIC
    #endif
#endif

#endif /* LUSB_CONFIG_H_ */
