#ifndef FWINFO_H_
#define FWINFO_H_

#include "global.h"

/*================================================================================================*/
/* Если FWINFO_OLD_SW_COMPAT определено и не 0, включается режим совместимости со старым софтом
     (драйвер ldevusbu, ltrserver):
     USB VID/PID как у LTR21,
     GetModuleName возвращает "LTR031" вместо реального названия
 */
//#define FWINFO_OLD_SW_COMPAT        1
/*================================================================================================*/

/*================================================================================================*/
#define FWINFO_FW_VER_MAJ           1
#define FWINFO_FW_VER_MIN           0
#define FWINFO_FW_VER_REV           6

#define FWINFO_FW_VERSION_STR       \
    STRINGIZE(FWINFO_FW_VER_MAJ) "." STRINGIZE(FWINFO_FW_VER_MIN) "." STRINGIZE(FWINFO_FW_VER_REV)

#define FWINFO_HW_REVISION          1

enum en_fwinfo_dev_model
    {
    FWINFO_DEV_MODEL_CU, FWINFO_DEV_MODEL_CEU,
    FWINFO_DEV_MODEL__COUNT__
    };

#define FWINFO_DEVICE_NAME_GENERIC  "LTR-CU/CEU-1"
#define FWINFO_DEVICE_NAME_CU       "LTR-CU-1"
#define FWINFO_DEVICE_NAME_CEU      "LTR-CEU-1"
#define FWINFO_DEVICE_NAME_BOOTLD   "LTR021M"   /* внутреннее имя (для бутлоадерв) */
#define FWINFO_MANUFACTURER         "L-Card"

#if !(defined(FWINFO_OLD_SW_COMPAT) && FWINFO_OLD_SW_COMPAT)
#define FWINFO_USB_VID              0x2A52
#define FWINFO_USB_PID              0x2122
#else
#define FWINFO_USB_VID              0x0471
#define FWINFO_USB_PID              0x2121
#define FWINFO_FAKE_MODULE_NAME     "LTR031"
#endif
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void fwinfo_init(void);             /* Инициализация, чтение номера модели и серийного номера */
/*------------------------------------------------------------------------------------------------*/
enum en_fwinfo_dev_model fwinfo_get_dev_model(void);
/*------------------------------------------------------------------------------------------------*/
const char* fwinfo_get_dev_name(void);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
#endif /* FWINFO_H_ */
