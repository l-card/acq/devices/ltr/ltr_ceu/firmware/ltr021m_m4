/*================================================================================================*
 * Обмен данными и командами с ПЛИС
 *================================================================================================*/

#include "cpld.h"
#include "dma.h"
#include "microtimer.h"
#include <string.h>
#include "test.h"

#if !SET_USE_M0_CORE
#error non-M0 code removed, see unused/cpld-without-m0core for old sources
#endif

/*================================================================================================*/
#define LPC_CLK(device)     CONCAT(LPC_CLK_, device)

/* -- порты SSP -- */
#define SSPn_IN             CONCAT(SSP, SET_SSPn_CPLD_IN)
#define LPC_SSP_IN          CONCAT(LPC_, SSPn_IN)
#define SSP_IN_PRESCALE     2
#define SSP_IN_CLKDIV       (LPC_CLK(SSPn_IN) / SSP_IN_PRESCALE / SET_SSP_IN_BITRATE)
#if (SSP_IN_CLKDIV < 1) || (SSP_IN_CLKDIV > 256)
#error "Invalid SSP_IN clock"
#endif

#define SSPn_OUT            CONCAT(SSP, SET_SSPn_CPLD_OUT)
#define LPC_SSP_OUT         CONCAT(LPC_, SSPn_OUT)
#define SSP_OUT_PRESCALE    2
#define SSP_OUT_CLKDIV      (LPC_CLK(SSPn_OUT) / SSP_OUT_PRESCALE / SET_SSP_OUT_BITRATE)
#if (SSP_OUT_CLKDIV < 1) || (SSP_OUT_CLKDIV > 256)
#error "Invalid SSP_OUT clock"
#endif

/* -- порт UART -- */
#define LPC_USARTx          CONCAT(LPC_USART, SET_USARTn_CPLD_REG)

/* -- Таймеры -- */
#define TIMERn_IN           CONCAT(TIMER, SET_TIMERn_SSP_IN)
#define LPC_TIMER_IN        CONCAT(LPC_, TIMERn_IN)

/* -- DMA -- */
#define LPC_DMA_IN_TX       (&LPC_GPDMA->CH[SET_DMACH_SSP_IN_TX])
#define LPC_DMA_IN_RX       (&LPC_GPDMA->CH[SET_DMACH_SSP_IN_RX])

/* DMA burst size field: 0 = burst 1, 1..7 = burst 4..256 */
#define GPDMA_BSIZE(bs)     (((bs) >= 4) ? FLOOR_LOG2(bs) - 1 : 0)

#define ALIGN_U16_BURST(bs) (sizeof(uint16_t) * (bs))

/* Размер burst для обмена с памятью приемника (SSP_IN), 1 или 4 */
#define SSP_IN_MEMBURST     4
#define SSP_IN_MEMALIGN     ALIGN_U16_BURST(SSP_IN_MEMBURST)

/* Размер burst для чтения из FIFO приемника (SSP_IN) */
/* BUGBUG: Как будто бы на этом LPC43xx не работают single DMA requests, во всяком
   случае, если burst = 4 (по документации, половина FIFO), оставшиеся 1..3 слова
   не забирались из порта. Пришлось сделать burst = 1. */
#define SSP_IN_PORTBURST    1
/*================================================================================================*/

/*================================================================================================*/
/* ---- Настройки каналов DMA (значения регистров CONFIG, CONTROL) ----
 *      В словах CONTROL параметр - длина передачи.
 *      В словах CONFIG не включены биты старта и прерывания
 * ---- */

/* SSP_IN: прием данных от ПЛИС.
   По сигналу готовности (timer CAP) на SSP TX посылаются нули из фиктивной переменной, чтобы
   инициировать прием одного слова.
   Поток данных идет через второй канал DMA от SSP RX.
 */
#define DMA_CFG_WORD_SSP_IN_TX ( 0 \
    | GPDMA_DMACCxConfig_DestPeripheral(DMA_PER_BY_ID(SET_SSP_IN_DMARQ_TX)) \
    | GPDMA_DMACCxConfig_TransferType((GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA)) \
    )
#define DMA_CTL_WORD_SSP_IN_TX(len) ( 0 \
    | GPDMA_DMACCxControl_TransferSize((len)) \
    | GPDMA_DMACCxControl_SBSize(GPDMA_BSIZE(SSP_IN_MEMBURST)) \
    | GPDMA_DMACCxControl_DBSize(GPDMA_BSIZE_1) \
    | GPDMA_DMACCxControl_SWidth(GPDMA_WIDTH_HALFWORD) \
    | GPDMA_DMACCxControl_DWidth(GPDMA_WIDTH_HALFWORD) \
    | GPDMA_DMACCxControl_DestTransUseAHBMaster1 \
    )

#define DMA_CFG_WORD_SSP_IN_RX ( 0 \
    | GPDMA_DMACCxConfig_SrcPeripheral(DMA_PER_BY_ID(SET_SSP_IN_DMARQ_RX)) \
    | GPDMA_DMACCxConfig_TransferType((GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA)) \
    | ((CPLD_RX_DMA_IRQ_BITS) ? GPDMA_DMACCxConfig_ITC : 0) \
    )
#define DMA_CTL_WORD_SSP_IN_RX(len) ( 0 \
    | GPDMA_DMACCxControl_TransferSize((len)) \
    | GPDMA_DMACCxControl_SBSize(GPDMA_BSIZE(SSP_IN_PORTBURST)) \
    | GPDMA_DMACCxControl_DBSize(GPDMA_BSIZE(SSP_IN_MEMBURST)) \
    | GPDMA_DMACCxControl_SWidth(GPDMA_WIDTH_HALFWORD) \
    | GPDMA_DMACCxControl_DWidth(GPDMA_WIDTH_HALFWORD) \
    | GPDMA_DMACCxControl_DI \
    | GPDMA_DMACCxControl_SrcTransUseAHBMaster1 \
    | ((CPLD_RX_DMA_IRQ_BITS) ? GPDMA_DMACCxControl_I : 0) \
    )
/*================================================================================================*/

/*================================================================================================*/
/* Буфер приемника */
volatile CPLD_RXBUF_DECL __attribute__((aligned(SSP_IN_MEMALIGN))) uint16_t
    g_cpld_recv_buf[CPLD_RXBUF_LEN];
volatile const uint16_t* volatile g_cpld_recv_rptr = g_cpld_recv_buf;
static volatile dma_lli_t f_lli_recv[CPLD_RXBUF_NBLOCKS];

/* Фиктивный буфер для посылки нулей в SSP, чтобы инициировать цикл чтения */
static volatile const __attribute__((aligned(SSP_IN_MEMALIGN))) uint16_t
    f_zero_words[SSP_IN_MEMBURST] = { 0 };
static volatile const dma_lli_t f_lli_zero =
    {
    .src_addr = f_zero_words,
    .dest_addr = &LPC_SSP_IN->DR,
    .next = &f_lli_zero,
    .ctl_word = DMA_CTL_WORD_SSP_IN_TX(DMA_MAX_XFER_LEN)
    };

static uint8_t f_hispeed = 0;

/* Признак присутствия модуля (обновляется cpld_worker) */
uint8_t g_cpld_module_mask = 0;  /* 1 = модуль присутствует */
static uint32_t f_module_mask_timer;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void cpld_init(void)
    {
    /* Ножка сброса ПЛИС (начальное состояние - в сбросе) */
    LPC_PIN_OUT(SET_PIN_CPLD_RESET, 1);
    LPC_PIN_DIR_OUT(SET_PIN_CPLD_RESET);
    LPC_PIN_CONFIG(SET_PIN_CPLD_RESET, 0, LPC_PIN_NOPULL);
    
    /* Ножка определения наличия модуля */
    LPC_PIN_CONFIG(SET_PIN_MODULE_DETECT, 1, LPC_PIN_PULLDN);
    LPC_PIN_DIR_IN(SET_PIN_MODULE_DETECT);

    /* ---- Инициализация приемника данных ---- */
    ASSERT(CPLD_RXBUF_BLKLEN <= DMA_MAX_XFER_LEN);
    ASSERT(!((CPLD_RXBUF_BLKLEN * sizeof(uint16_t)) % SSP_IN_MEMALIGN));
    for (int i = 0; i < CPLD_RXBUF_NBLOCKS; i++)
        {
        f_lli_recv[i].src_addr = &LPC_SSP_IN->DR;
        f_lli_recv[i].dest_addr = &g_cpld_recv_buf[i * CPLD_RXBUF_BLKLEN];
        f_lli_recv[i].next = &f_lli_recv[(i < CPLD_RXBUF_NBLOCKS - 1) ? i + 1 : 0];
        f_lli_recv[i].ctl_word = DMA_CTL_WORD_SSP_IN_RX(CPLD_RXBUF_BLKLEN);
        }

    /* SSP: master, TI SSI, 10 bit, read on leading edge of T2_CAP0 = P6_1 */
    LPC_SSP_IN->CR1 &= ~SSP_CR1_SSP_EN;
    LPC_SSP_IN->CPSR = SSP_IN_PRESCALE;
    LPC_SSP_IN->CR0 = SSP_CR0_DSS(10) | SSP_CR0_FRF_TI | SSP_CR0_SCR((SSP_IN_CLKDIV - 1));
    LPC_SSP_IN->CR1 = SSP_CR1_MASTER_EN;
    LPC_SSP_IN->IMSC = 0;
    LPC_SSP_IN->DMACR = 0;
    LPC_SSP_IN->CR1 |= SSP_CR1_SSP_EN;

    /* Каналы DMA для приемника */
    dma_stop(LPC_DMA_IN_TX);
    dma_mux_select(SET_SSP_IN_DMARQ_TX);
    dma_sync_select(SET_SSP_IN_DMARQ_TX, 0);
    dma_stop(LPC_DMA_IN_RX);
    dma_mux_select(SET_SSP_IN_DMARQ_RX);
    dma_sync_select(SET_SSP_IN_DMARQ_RX, 0);
    /* Инициализация самих каналов потом, при запуске (cpld_start) */

    LPC_SSP_IN->DMACR = SSP_DMA_RX | SSP_DMA_TX;

    /* Таймер для генерации запросов по DMA по положительному фронту SET_PIN_CPLD_DRDY */
    LPC_GIMA->CAP0_IN[SET_TIMERn_SSP_IN][SET_SSP_IN_TIMER_CAPn] = SET_GIMACFG_CPLD_DRDY;
    LPC_TIMER_IN->TCR = TIMER_RESET;
    LPC_TIMER_IN->CTCR = TIMER_CAPSRC_RISING_CAPN | (SET_SSP_IN_TIMER_CAPn << 2);
    LPC_TIMER_IN->PR = 0;
    LPC_TIMER_IN->TC = 0;
    LPC_TIMER_IN->PC = 0;
    LPC_TIMER_IN->MCR = TIMER_RESET_ON_MATCH(SET_SSP_IN_TIMER_MATn);
    LPC_TIMER_IN->MR[SET_SSP_IN_TIMER_MATn] = 1; /* счет до 1 импульса */
    LPC_TIMER_IN->CCR = 0;
    LPC_TIMER_IN->EMR = 0;
//    LPC_TIMER_IN->EMR = TIMER_EXTMATCH_SET << (4 + 2 * SET_SSP_IN_TIMER_MATn);
    LPC_TIMER_IN->IR = 0xFF; /* clear timer DMA request */
    LPC_TIMER_IN->TCR = TIMER_ENABLE;

    /* ---- Инициализация передатчика данных ---- */
    /* SSP: master, SPI mode 0, 9 bit */
    LPC_SSP_OUT->CR1 &= ~SSP_CR1_SSP_EN;
    LPC_SSP_OUT->CPSR = SSP_OUT_PRESCALE;
    LPC_SSP_OUT->CR0 = SSP_CR0_DSS(9) | SSP_CR0_FRF_SPI | SSP_CLOCK_CPHA0_CPOL0 |
        SSP_CR0_SCR((SSP_OUT_CLKDIV - 1));
    LPC_SSP_OUT->CR1 = SSP_CR1_MASTER_EN;
    LPC_SSP_OUT->IMSC = 0;
    LPC_SSP_OUT->DMACR = 0;
    LPC_SSP_OUT->CR1 |= SSP_CR1_SSP_EN;

    /* Ножки SPI_IN */
    LPC_PIN_CONFIG(SET_PIN_SSP_IN_CLK,  0, LPC_PIN_NOPULL | LPC_PIN_FAST | LPC_PIN_NOFILTER);
    LPC_PIN_CONFIG(SET_PIN_SSP_IN_DR_M, 1, LPC_PIN_PULLUP | LPC_PIN_FAST | LPC_PIN_NOFILTER);
    LPC_PIN_CONFIG(SET_PIN_SSP_IN_FS,   0, LPC_PIN_NOPULL | LPC_PIN_FAST | LPC_PIN_NOFILTER);

    /* Ножки SPI_OUT */
    LPC_PIN_CONFIG(SET_PIN_SSP_OUT_SCK,  0, LPC_PIN_NOPULL);
    LPC_PIN_CONFIG(SET_PIN_SSP_OUT_MOSI, 0, LPC_PIN_NOPULL);
    LPC_PIN_CONFIG(SET_PIN_SSP_OUT_SSEL, 0, LPC_PIN_NOPULL);

    /* Строб готовности данных для SPI_IN */
    LPC_PIN_CONFIG(SET_PIN_CPLD_DRDY, 1, LPC_PIN_PULLDN);

    /* Инициализация USART для доступа к регистрам ПЛИС */
    /* Synchronous slave, CPOL = 0, 8n1 */
    LPC_USARTx->IER = 0; /* no interrupts */
    LPC_USARTx->LCR =
          UART_LCR_WLEN8        /* 8-bit */
        | UART_LCR_SBS_1BIT     /* 1 stop */
        | UART_LCR_PARITY_DIS;  /* no parity */

#if 0 /* baud rate is irrelevant in synchronous mode */
    LPC_USARTx->LCR |= UART_LCR_DLAB_EN;
    LPC_USARTx->DLM = BAUDCALC_OUT_UDL >> 8;
    LPC_USARTx->DLL = BAUDCALC_OUT_UDL & 0xFF;
    LPC_USARTx->LCR &= ~UART_LCR_DLAB_EN;
    LPC_USARTx->FDR = UART_FDR_MULVAL((BAUDCALC_OUT_FDM))
                    | UART_FDR_DIVADDVAL((BAUDCALC_OUT_FDA));
#endif

    LPC_USARTx->SYNCCTRL = UART_SYNCCTRL_SYNC   /* synchronous */
        | (0 & UART_SYNCCTRL_CSRC_MASTER)       /* slave */
        | (0 & UART_SYNCCTRL_FES)               /* sample RXD on rising edge */
        | (0 & UART_SYNCCTRL_TSBYPASS)          /* no xmit sync bypass */
        | UART_SYNCCTRL_CSCEN                   /* (irrelevant in slave mode) */
        | (0 & UART_SYNCCTRL_STARTSTOPDISABLE)  /* 0: normal start/stop bits */
        | (0 & UART_SYNCCTRL_CCCLR);            /* (irrelevant in slave mode) */

    LPC_USARTx->FCR = UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS
        | UART_FCR_TRG_LEV2; /* 8 bytes to interrupt */
    LPC_USARTx->ACR = 0;
    LPC_USARTx->ICR = 0;
    LPC_USARTx->HDEN = 0;
    LPC_USARTx->SCICTRL = 0;
    LPC_USARTx->RS485CTRL = 0;
    LPC_USARTx->TER2 = UART_TER2_TXEN;

    LPC_PIN_CONFIG(SET_PIN_USART_CPLDREG_TXD, 0, LPC_PIN_PULLUP);
    LPC_PIN_CONFIG(SET_PIN_USART_CPLDREG_RXD, 1, LPC_PIN_PULLUP);
    LPC_PIN_CONFIG(SET_PIN_USART_CPLDREG_CLK, 1, LPC_PIN_PULLDN);

    cpld_send_setspeed(0);
    g_cpld_module_mask = 0;
    f_module_mask_timer = microtimer_now();
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void cpld_start(void)
    {
    if (dma_running(LPC_DMA_IN_TX))
        cpld_stop();

    /* Очистка FIFO SSP */
    /* NB: если бы SSP был slave, нужна была бы очистка через тестовые регистры */
    while (LPC_SSP_OUT->SR & SSP_STAT_BSY);
    while (LPC_SSP_IN->SR & SSP_STAT_BSY);

    /* Инициализация каналов DMA для приемника */
    /* TX: запрос от таймера, без инкремента из фиктивной переменной */
    dma_chan_setup(LPC_DMA_IN_TX, &f_lli_zero, DMA_CFG_WORD_SSP_IN_TX);
    /* RX: запрос от SSP, в буфер */
    dma_chan_setup(LPC_DMA_IN_RX, &f_lli_recv[0], DMA_CFG_WORD_SSP_IN_RX);

    LPC_TIMER_IN->IR = 0xFF; /* clear timer DMA request */
    dma_start(LPC_DMA_IN_RX);
    dma_start(LPC_DMA_IN_TX);
    LPC_TIMER_IN->TCR = TIMER_ENABLE;

    /* Запуск передатчика на ядре M0 */
    g_m0_tx_interval = M0_TX_INTERVAL_SLOW;
    m0_core_start();

    /* Снятие сброса ПЛИС */
    LPC_PIN_OUT(SET_PIN_CPLD_RESET, 0);
    lpc_delay_clk(CYCLES_NS_NLESS(LPC_SYSCLK, 100));
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void cpld_stop(void)
    {
    /* Сброс ПЛИС */
    LPC_PIN_OUT(SET_PIN_CPLD_RESET, 1);

    LPC_TIMER_IN->TCR = TIMER_RESET;
    dma_stop(LPC_DMA_IN_TX);
    dma_stop(LPC_DMA_IN_RX);
    m0_core_stop();
    g_m0_tx_tail = g_m0_tx_head;

    g_cpld_recv_rptr = g_cpld_recv_buf;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int cpld_send_getspeed(void)
    {
    return f_hispeed;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void cpld_send_setspeed(int hi_speed)
    {
    /* Запомнить скорость порта, она вступит в силу при следующей посылке */
    f_hispeed = !!hi_speed;
    g_m0_tx_interval = (hi_speed) ? M0_TX_INTERVAL_FAST : M0_TX_INTERVAL_SLOW;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t cpld_send(volatile const uint16_t* buf, size_t nwords)
    {
    size_t count = 0, tail = g_m0_tx_tail;
    while (count < nwords)
        {
        size_t new_tail = (tail + 1) % M0_TX_BUF_LEN;
        if (new_tail == g_m0_tx_head)
            break;
        g_m0_tx_buf[tail] = buf[count++];
        g_m0_tx_tail = tail = new_tail;
        }
    return count;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int cpld_write_regs(unsigned int first_addr, const uint8_t* buf, size_t count)
    {
    uint32_t t = microtimer_now();
    if ((first_addr >= CPLD_REGS_MAX) || (first_addr + count > CPLD_REGS_MAX))
        return 0;
    while (count > 0)
        {
        size_t n = UART_TX_FIFO_SIZE / 2;   /* запись одного регистра = 2 байта FIFO */
        if (n > count) n = count;
        while (!(LPC_USARTx->LSR & UART_LSR_THRE))
            {
            if (microtimer_since(t) > MICROTIMER_uS(CPLD_REGS_TIMEOUT_uS)) return 0;
            }
        count -= n;
        while (n--)
            {
            LPC_USARTx->THR = (uint8_t)(0x01 /* address byte */ | 0x02 /* write */
                | (first_addr++ << 2));
            LPC_USARTx->THR = (uint8_t)(*buf++ << 1); /* data (7 bits) */
            }
        }
    /* Дождаться окончания передачи */
    while (!(LPC_USARTx->LSR & UART_LSR_TEMT))
        {
        if (microtimer_since(t) > MICROTIMER_uS(CPLD_REGS_TIMEOUT_uS)) return 0;
        }
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void cpld_worker(void)
    {
    /* Отслеживание изменения сигнала присутствия модуля (с антидребезгом) */
    uint8_t mask_in = LPC_PIN_IN(SET_PIN_MODULE_DETECT);
    uint32_t now = microtimer_now();
    if (mask_in != g_cpld_module_mask)
        {
        if (microtimer_since(f_module_mask_timer) >= MICROTIMER_uS(SET_MODULE_DETECT_BOUNCE_uS))
            {
            cpld_send_setspeed(0);
            g_cpld_module_mask = mask_in;
            f_module_mask_timer = now;
            }
        }
    else
        {
        f_module_mask_timer = now;
        }
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
