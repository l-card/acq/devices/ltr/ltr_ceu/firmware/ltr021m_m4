#ifndef DMA_H_
#define DMA_H_

#include "global.h"

/*================================================================================================*/
#define DMA_MAX_XFER_LEN    0xFFF

/* Кодирование номера источника DMA и мультиплексора (для регистра DMAMUX) */
#define DMA_RQID(per, mux)  (((mux) << 8) | (per))
#define DMA_PER_BY_ID(rqid) ((rqid) & 0x0F)
#define DMA_MUX_BY_ID(rqid) (((rqid) >> 8) & 3)
enum en_dma_req_mux
    {
    DMA_REQ0_SPIFI      = DMA_RQID(0, 0),
    DMA_REQ0_CTOUT2     = DMA_RQID(0, 1),
    DMA_REQ0_SGPIO14    = DMA_RQID(0, 2),
    DMA_REQ0_T3MAT1     = DMA_RQID(0, 3),
    DMA_REQ1_T0MAT0     = DMA_RQID(1, 0),
    DMA_REQ1_USART0TX   = DMA_RQID(1, 1),
    DMA_REQ2_T0MAT1     = DMA_RQID(2, 0),
    DMA_REQ2_USART0RX   = DMA_RQID(2, 1),
    DMA_REQ3_T1MAT0     = DMA_RQID(3, 0),
    DMA_REQ3_UART1TX    = DMA_RQID(3, 1),
    DMA_REQ3_I2S1DMA1   = DMA_RQID(3, 2),
    DMA_REQ3_SSP1TX     = DMA_RQID(3, 3),
    DMA_REQ4_T1MAT1     = DMA_RQID(4, 0),
    DMA_REQ4_UART1RX    = DMA_RQID(4, 1),
    DMA_REQ4_I2S1DMA2   = DMA_RQID(4, 2),
    DMA_REQ4_SSP1RX     = DMA_RQID(4, 3),
    DMA_REQ5_T2MAT0     = DMA_RQID(5, 0),
    DMA_REQ5_UASRT2TX   = DMA_RQID(5, 1),
    DMA_REQ5_SSP1TX     = DMA_RQID(5, 2),
    DMA_REQ5_SGPIO15    = DMA_RQID(5, 3),
    DMA_REQ6_T2MAT1     = DMA_RQID(6, 0),
    DMA_REQ6_USART2RX   = DMA_RQID(6, 1),
    DMA_REQ6_SSP1RX     = DMA_RQID(6, 2),
    DMA_REQ6_SGPIO14    = DMA_RQID(6, 3),
    DMA_REQ7_T3MAT0     = DMA_RQID(7, 0),
    DMA_REQ7_USART3TX   = DMA_RQID(7, 1),
    DMA_REQ7_SCTDMA0    = DMA_RQID(7, 2),
    DMA_REQ7_ADCHS_W    = DMA_RQID(7, 3),
    DMA_REQ8_T3MAT1     = DMA_RQID(8, 0),
    DMA_REQ8_USART3RX   = DMA_RQID(8, 1),
    DMA_REQ8_SCTDMA1    = DMA_RQID(8, 2),
    DMA_REQ8_ADCHS_R    = DMA_RQID(8, 3),
    DMA_REQ9_SSP0RX     = DMA_RQID(9, 0),
    DMA_REQ9_I2S0DMA1   = DMA_RQID(9, 1),
    DMA_REQ9_SCTDMA1    = DMA_RQID(9, 2),
    DMA_REQ10_SSP0TX    = DMA_RQID(10, 0),
    DMA_REQ10_I2S0DMA2  = DMA_RQID(10, 1),
    DMA_REQ10_SCTDMA0   = DMA_RQID(10, 2),
    DMA_REQ11_SSP1RX    = DMA_RQID(11, 0),
    DMA_REQ11_SGPIO14   = DMA_RQID(11, 1),
    DMA_REQ11_USART0TX  = DMA_RQID(11, 2),
    DMA_REQ12_SSP1TX    = DMA_RQID(12, 0),
    DMA_REQ12_SGPIO15   = DMA_RQID(12, 1),
    DMA_REQ12_USART0RX  = DMA_RQID(12, 2),
    DMA_REQ13_ADC0      = DMA_RQID(13, 0),
    DMA_REQ13_SSP1RX    = DMA_RQID(13, 2),
    DMA_REQ13_USART3RX  = DMA_RQID(13, 3),
    DMA_REQ14_ADC1      = DMA_RQID(14, 0),
    DMA_REQ14_SSP1TX    = DMA_RQID(14, 2),
    DMA_REQ14_USART3TX  = DMA_RQID(14, 3),
    DMA_REQ15_DAC       = DMA_RQID(15, 0),
    DMA_REQ15_CTOUT3    = DMA_RQID(15, 1),
    DMA_REQ15_SGPIO15   = DMA_RQID(15, 2),
    DMA_REQ15_T3MAT0    = DMA_RQID(15, 3)
    };
/*================================================================================================*/

/*================================================================================================*/
typedef volatile struct __attribute__ ((packed)) st_dma_lli
    {
    volatile const void* src_addr;
    volatile const void* dest_addr;
    volatile const struct st_dma_lli* next;
    uint32_t ctl_word;
    }
    dma_lli_t;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void dma_init(void);
/*------------------------------------------------------------------------------------------------*/
void dma_chan_setup(GPDMA_CH_T* pchan, const dma_lli_t* lli, uint32_t config);
/*------------------------------------------------------------------------------------------------*/
static inline void dma_sync_select(uint32_t rqid, int on)
    {
    if (on)
        LPC_GPDMA->SYNC = (LPC_GPDMA->SYNC & ~(1U << DMA_PER_BY_ID(rqid))) & 0xFFFF;
    else
        LPC_GPDMA->SYNC = (LPC_GPDMA->SYNC | (1U << DMA_PER_BY_ID(rqid))) & 0xFFFF;
    }
/*------------------------------------------------------------------------------------------------*/
static inline void dma_mux_select(uint32_t rqid)
    {
    LPC_CREG->DMAMUX = (LPC_CREG->DMAMUX & ~(3U << (2 * DMA_PER_BY_ID(rqid))))
                                         | (DMA_MUX_BY_ID(rqid) << (2 * DMA_PER_BY_ID(rqid)));
    }
/*------------------------------------------------------------------------------------------------*/
static inline void dma_start(GPDMA_CH_T* pchan)
    {
    pchan->CONFIG |= GPDMA_DMACCxConfig_E;
    }
/*------------------------------------------------------------------------------------------------*/
static inline void dma_stop(GPDMA_CH_T* pchan)
    {
    pchan->CONFIG &= ~GPDMA_DMACCxConfig_E;
    while (pchan->CONFIG & GPDMA_DMACCxConfig_E);
    }
/*------------------------------------------------------------------------------------------------*/
static inline int dma_running(GPDMA_CH_T* pchan)
    {
    return (pchan->CONFIG & GPDMA_DMACCxConfig_E);
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/

#endif /* DMA_H_ */
