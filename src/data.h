/*================================================================================================*
 * Преобразование данных из формата ПЛИС (3 9-битных слова) в формат крейт-контроллера (4 байта)
 * и обратно с буферизацией.
 *================================================================================================*/

#ifndef DATA_H_
#define DATA_H_

#include "global.h"
#include "test.h"

/*================================================================================================*/
#define DATA_IN_BUF_LEN                 (4 * 1048576) /* в словах хоста */
#define DATA_IN_BUF_DECL                __attribute__ ((section(".sdram")))

#define DATA_OUT_BUF_LEN                (2 * 1048576) /* в словах хоста */
#define DATA_OUT_BUF_DECL               __attribute__ ((section(".sdram")))

#define DATA_USE_DMA_INTERRUPT          1

#define DATA_OVF_SIGNAL_LEN             2
#define DATA_SIGNAL_PREFIX              0xFFFFFFFF
#define DATA_SIGNAL_MODULE_MASK_UPDATED 0xFFFFAA01
#define DATA_SIGNAL_BUFFER_OVERFLOW     0xFFFFAA02
#define DATA_TIMESTAMP_WORD             0x0000FF10 /* бит 28 = старт, бит 29 = секунда */

/* Размеры буферов для диагностического дампа данных (0 = выключить) */
/* NB: На больших скоростях мешает работать */
#define DATA_DIAG_TRACE_LEN             0

#if DATA_IN_BUF_LEN & (DATA_IN_BUF_LEN - 1)
#error "DATA_IN_BUF_LEN should be a power of 2"
#endif
#if DATA_OUT_BUF_LEN & (DATA_OUT_BUF_LEN - 1)
#error "DATA_OUT_BUF_LEN should be a power of 2"
#endif
/*================================================================================================*/

/*================================================================================================*/
#if TEST_RAW_DATA_FROM_HOST
typedef uint16_t data_host_out_t;
#else
typedef uint32_t data_host_out_t;
#endif

#if TEST_RAW_DATA_TO_HOST
typedef uint16_t data_host_in_t;
#else
typedef uint32_t data_host_in_t;
#endif
/*================================================================================================*/

/*================================================================================================*/
/* Буфер приема */
extern volatile DATA_IN_BUF_DECL data_host_in_t g_data_in_buf[DATA_IN_BUF_LEN];
extern volatile size_t g_data_in_head, g_data_in_tail;

/* Буфер передачи */
extern DATA_OUT_BUF_DECL data_host_out_t g_data_out_buf[DATA_OUT_BUF_LEN];
extern size_t g_data_out_head, g_data_out_tail;

/* отладочные переменные */
typedef struct
    {
    uint32_t in_word_count;         /* количество принятых и обработанных слов */
    uint32_t in_word_count_irq;     /* в т.ч. количество слов, обработанных в прерывании */
    uint32_t in_ovf_count;          /* счетчик переполнений приемника */
    uint32_t out_word_count;        /* количество посланных в модуль слов */
    uint32_t max_in_words_bgnd;     /* наибольшая порция данных, обработанная за один вызов worker */
    uint32_t max_in_words_irq;      /* наибольшая порция данных, обработанная в одном прерывании */
    }
    data_diag_t;

extern data_diag_t g_data_diag;

#if DATA_DIAG_TRACE_LEN > 0
typedef struct __attribute__ ((packed))
    {
    uint16_t in_pos;
    uint16_t out_pos;
    uint16_t in[DATA_DIAG_TRACE_LEN];
    uint16_t out[DATA_DIAG_TRACE_LEN];
    }
    data_diag_trace_t;
extern data_diag_trace_t g_data_diag_trace;
#endif
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void data_restart(void);
/*------------------------------------------------------------------------------------------------*/
void data_stop(void);
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static inline size_t data_in_words_rdy(void)
    {
    ssize_t count = (ssize_t)g_data_in_tail - (ssize_t)g_data_in_head;
    if (count < 0) count += (ssize_t)ARRLEN(g_data_in_buf);
    return (size_t)count;
    }
/*------------------------------------------------------------------------------------------------*/
/* Количество готовых слов в буфере приема, доступных без перехода через конец буфера */
static inline size_t data_in_contig_words_rdy(void)
    {
    size_t head = g_data_in_head, tail = g_data_in_tail;
    return (tail >= head) ? tail - head : ARRLEN(g_data_in_buf) - head;
    }
/*------------------------------------------------------------------------------------------------*/
static inline volatile const data_host_in_t* data_in_cur_ptr(void)
    {
    return &g_data_in_buf[g_data_in_head];
    }
/*------------------------------------------------------------------------------------------------*/
/* Извлечение n слов из буфера приема (приращение головы) */
static inline void data_in_pop_words(size_t n)
    {
    g_data_in_head = (g_data_in_head + n) % ARRLEN(g_data_in_buf);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static inline size_t data_out_words_free(void)
    {
    ssize_t count = (ssize_t)g_data_out_head - (ssize_t)g_data_out_tail - 1;
    if (count < 0) count += (ssize_t)ARRLEN(g_data_out_buf);
    return (size_t)count;
    }
/*------------------------------------------------------------------------------------------------*/
/* Количество свободных слов в буфере передачи, доступных без перехода через конец буфера */
static inline size_t data_out_contig_words_free(void)
    {
    size_t head = g_data_out_head, tail = g_data_out_tail;
    return (tail < head) ? head - tail - 1 : ARRLEN(g_data_out_buf) - tail - !head;
    }
/*------------------------------------------------------------------------------------------------*/
static inline data_host_out_t* data_out_cur_ptr(void)
    {
    return &g_data_out_buf[g_data_out_tail];
    }
/*------------------------------------------------------------------------------------------------*/
/* Учет помещенных в буфер передачи n слов (приращение хвоста) */
static inline void data_out_push_words(size_t n)
    {
    g_data_out_tail = (g_data_out_tail + n) % ARRLEN(g_data_out_buf);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void data_worker(void);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/

#endif /* DATA_H_ */
