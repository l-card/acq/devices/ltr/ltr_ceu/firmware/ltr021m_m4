#include "global.h"
#include "settings.h"
#include "m0_task.h"

/*================================================================================================*/
/* Порт SSP инициализируюется в cpld.c */
#define SSPn_OUT            CONCAT(SSP, SET_SSPn_CPLD_OUT)
#define LPC_SSP_OUT         CONCAT(LPC_, SSPn_OUT)

#define USE_INTERRUPT       0
/*================================================================================================*/

/*================================================================================================*/
/* Интервал посылки в тиках таймера (таймер настраивается в cpld.c) */
_M0_DATA_Z_ volatile uint32_t g_m0_tx_interval;  

/* Буфер передатчика */
_M0_DATA_Z_ volatile uint16_t g_m0_tx_buf[M0_TX_BUF_LEN];
_M0_DATA_Z_ volatile size_t g_m0_tx_head, g_m0_tx_tail;
/*================================================================================================*/

/*================================================================================================*/
#if 0
/*------------------------------------------------------------------------------------------------*/
__attribute__ ((__noinline__))
_M0_CODE_ static void delay_clk(unsigned int clk_count) {
#define CYCLES_OVERHEAD     7   // проверить, это от m4
    /* Задержка примерно на clk_count тактов, min = 11 тактов */
    asm volatile (
         " .syntax unified\n"
         "  subs %[reg], %[reg], %[adj]\n"
         "  asrs %[reg], %[reg], #2\n"  /* 4 cycles per loop */
         "0:"
         "  subs %[reg], %[reg], #0\n"
         "  subs %[reg], %[reg], #1\n"
         "  bgt 0b\n"
         " .syntax divided"
         : [reg] "+r" (clk_count)
         : [adj] "i" (CYCLES_OVERHEAD - 3)
         : "cc"
        );
#undef CYCLES_OVERHEAD
    }
/*------------------------------------------------------------------------------------------------*/
#endif

#if USE_INTERRUPT
/*------------------------------------------------------------------------------------------------*/
_M0_CODE_ void __attribute__ ((__interrupt__)) MCPWM_IRQHandler_m0(void)
    {
    LPC_MCPWM->INTF_CLR = -1UL;
    size_t head = g_m0_tx_head;
    if (head != g_m0_tx_tail)
        {
        LPC_SSP_OUT->DR = g_m0_tx_buf[head++];
        head %= M0_TX_BUF_LEN;
        g_m0_tx_head = head;
        }
    LPC_MCPWM->LIM[0] = g_m0_tx_interval; /* reload interval (if changed) */
    __dsb();
    }
/*------------------------------------------------------------------------------------------------*/
#endif

/*------------------------------------------------------------------------------------------------*/
_M0_CODE_ void m0_main(void)
    {
    /* Настройка MCPWM на генерацию периодических прерываний */
    LPC_MCPWM->CON_CLR = -1UL; /* clear all channels */
    LPC_MCPWM->CAPCON_CLR = -1UL;
    LPC_MCPWM->CNTCON_CLR = -1UL;
    LPC_MCPWM->TC[0] = 0;
    LPC_MCPWM->LIM[0] = g_m0_tx_interval;
    LPC_MCPWM->INTF_CLR = -1UL;
    LPC_MCPWM->INTEN_CLR = -1UL;
    LPC_MCPWM->INTEN_SET = MCPWM_INT_ILIM(0); /* enable LIM0 interrupt */

#if !USE_INTERRUPT
    __set_PRIMASK(0x01); /* disable interrupts even if they are enabled in NVIC */
#endif

    NVIC_EnableIRQ(MCPWM_IRQn);
    LPC_MCPWM->CON_SET = MCPWM_CON_RUN(0);

    for (;;)
        {
#if !USE_INTERRUPT
        size_t head;

        LPC_MCPWM->INTF_CLR = -1UL;
        while (LPC_MCPWM->INTF & MCPWM_INT_ILIM(0));
        NVIC_ClearPendingIRQ(MCPWM_IRQn);
        __dsb();
        __wfi(); /* wait for next MCPWM period */

        head = g_m0_tx_head;
        if (head != g_m0_tx_tail)
            {
            LPC_SSP_OUT->DR = g_m0_tx_buf[head++];
            head %= M0_TX_BUF_LEN;
            g_m0_tx_head = head;
            }
        LPC_MCPWM->LIM[0] = g_m0_tx_interval; /* reload interval (if changed) */
#else
        __wfi();
#endif
        }
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
