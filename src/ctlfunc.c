/*================================================================================================*
 * Реализация функций control pipe (GetArray, PutArray и т.п.)
 *================================================================================================*/

#include "ctlfunc.h"
#include "fpga_reg.h"
#include "fwinfo.h"
#include "conf.h"
#include "cpld.h"
#include "data.h"
#include "hostio.h"
#include <string.h>
#include "test.h"
#include "rtc.h"
#include "boot.h"
#include "sdram_test.h"

/*================================================================================================*/
#define CTLFUNC_MCS_VERSION     0x0000

/* Адреса и размеры (в байтах) структур для getarray/putarray */

/* 1) Адреса, совместимые с другими LTR */
#define IOADDR_LTR_REGS         (CTLFUNC_SEL_AVR_DM + 0x8000)
#define     IOSIZE_LTR_REGS         16
#define IOADDR_MCS_VER          (CTLFUNC_SEL_AVR_DM + 0x10101)
#define     IOSIZE_MCS_VER          2
#define IOADDR_DUMMY            (CTLFUNC_SEL_AVR_PM + 0x0000)
#define     IOSIZE_DUMMY            16
#define IOADDR_FW_VER           (CTLFUNC_SEL_AVR_PM + 0x2FF0)
#define     IOSIZE_FW_VER           16
#define IOADDR_DEVDESCR         (CTLFUNC_SEL_AVR_PM + 0x3000)
#define     IOSIZE_DEVDESCR         128
#define IOADDR_BOOTLD_VER       (CTLFUNC_SEL_AVR_PM + 0x3FF0)
#define     IOSIZE_BOOTLD_VER       16
#define IOADDR_FPGAREGS_E       (CTLFUNC_SEL_FPGA_DATA + 0x0000) /* emulated LTR030 FPGA regs */
#define     IOSIZE_FPGAREGS_E       (2 * FPGA_REG_COUNT_EMU)
#define IOADDR_HW_REV           (CTLFUNC_SEL_DSP    + 0x8001)
#define     IOSIZE_HW_REV           1

/* 2) Адреса, новые для этой модели */
#define IOADDR_FPGAREGS_N       (CTLFUNC_SEL_FPGA_DATA + 0x8000) /* native LTR021M CPLD regs */
#define     IOSIZE_FPGAREGS_N       (2 * FPGA_REG_COUNT_NATIVE)
#define IOADDR_RTC_PREP         (CTLFUNC_SEL_DSP    + 0x4000)   /* RTC set prepare */
#define     IOSIZE_RTC_PREP         2
#define IOADDR_RTC              (CTLFUNC_SEL_DSP    + 0x4002)   /* RTC get/set */
#define     IOSIZE_RTC              sizeof(rtc_time_t)
#define IOADDR_DATA_DIAG        (CTLFUNC_SEL_DSP    + 0x4200)   /* debug: статистика */
#define     IOSIZE_DATA_DIAG        sizeof(g_data_diag)
#if DATA_DIAG_TRACE_LEN > 0
#define IOADDR_DATA_TRACE       (CTLFUNC_SEL_DSP    + 0x4300)   /* debug: послендние N слов */
#define     IOSIZE_DATA_TRACE       sizeof(g_data_diag_trace)
#endif
#define IOADDR_SYS_COMMAND      (CTLFUNC_SEL_DSP    + 0x5000)   /* commands (reboot etc.) */
#define     IOSIZE_SYS_COMMAND      sizeof(uint32_t)
#define IOADDR_CUR_IFACE        (CTLFUNC_SEL_DSP    + 0x5004)   /* get current interface (0 USB, 1 TCP) */
#define     IOSIZE_CUR_IFACE        1
#define IOADDR_RAMTEST_RESULT   (CTLFUNC_SEL_DSP    + 0x5008)   /* RAM test result (0 ok, -1 can't start, or failaddr) */
#define     IOSIZE_RAMTEST_RESULT   sizeof(uintptr_t)

    /* При записи конфигурации в конце приписывается новый пароль, если нужно его сменить */
#define IOADDR_CONF_FACT        (CTLFUNC_SEL_DSP    + 0x5100)   /* factory config + new password */
#define     IOSIZE_CONF_FACT_R      sizeof(g_conf_fact)
#define     IOSIZE_CONF_FACT_W      (sizeof(g_conf_fact) + CONF_PASSWD_SIZE)
#define IOADDR_CONF_USER        (CTLFUNC_SEL_DSP    + 0x5200)   /* user config + new password */
#define     IOSIZE_CONF_USER_R      sizeof(g_conf_user)
#define     IOSIZE_CONF_USER_W      (sizeof(g_conf_user) + CONF_PASSWD_SIZE)

/* Команда установки часов */
#define LTR021M_RTC_PREP_MAGIC  0x2359

/* Системные команды (пишутся в IOADDR_SYS_COMMAND) */
#define LTR021M_CMD_REBOOT      0x5AFEC0DE
#define LTR021M_CMD_GO_BOOTLDR  0xB00710AD
#define LTR021M_CMD_SDRAM_TEST  0xCE117E57
/*================================================================================================*/

/*================================================================================================*/
#define ARRAY_END(arr)          (&(arr)[sizeof(arr) / sizeof((arr)[0])])
#define ARRAY_FIT_BYTES(t, n)   (((n) + sizeof(t) - 1) / sizeof(t))

#define GETPUT_CALLBACK_DECL(func_name) \
    int func_name(const struct st_getput_def* pdef, \
                  conf_iface_t iface, uint32_t offset, size_t len, uint8_t* buf)

enum en_ltr_reg_no
    {
    LTR_REG_HISPEED_MASK = 3,   /* номер регистра LTR010 с маской скоростей передачи */
    LTR_REG_MODULE_MASK = 4     /* номер регистра LTR010 с маской наличия модулей */
    };

static uint16_t f_ltr_regs[ARRAY_FIT_BYTES(uint16_t, IOSIZE_LTR_REGS)];

/* Константные данные для GETARRAY */
static const uint8_t fw_ver_array[IOSIZE_FW_VER] =
    { 0, FWINFO_FW_VER_REV, FWINFO_FW_VER_MIN, FWINFO_FW_VER_MAJ };
static const uint8_t dummy_array[IOSIZE_DUMMY] =
    { 0 };
static const uint8_t hw_revision[IOSIZE_HW_REV] =
    { FWINFO_HW_REVISION };
static const uint16_t mcs_version[ARRAY_FIT_BYTES(uint16_t, IOSIZE_MCS_VER)] =
    { CTLFUNC_MCS_VERSION };

/* Дескриптор (для GETDESCRIPTOR) */
typedef struct __attribute__ ((packed))
    {
    char name[16];
    char serial_no[16];
    char cpu_type[16];
    uint8_t revision;
    uint32_t clk_freq;
    char comment[IOSIZE_DEVDESCR - 3*16 - sizeof(uint8_t) - sizeof(uint32_t)];
    }
    ctlfunc_dev_descr_t;

static ctlfunc_dev_descr_t dev_descr =
    {
    .name = "",
    .serial_no = "",
    .cpu_type = "LPC4337",
    .revision = FWINFO_HW_REVISION,
    .clk_freq = LPC_SYSCLK,
    .comment = "Single-slot LTR crate"
    };

static uint16_t f_bootld_ver;

static uintptr_t f_sdram_test_result = SDRAM_TEST_RES_NOT_STARTED;

uint16_t g_ctlfunc_fpga_info[CTLFUNC_FPGA_INFO_LEN];
/*================================================================================================*/

/*================================================================================================*/
/* Таблица объектов для GETARRAY/PUTARRAY. Если есть callback, то для чтения/записи вызывается он,
   а если callback == NULL, то просто memcpy */

typedef struct st_getput_def
    {
    uint32_t start_addr;
    size_t size;
    const void* data;
    GETPUT_CALLBACK_DECL((*callback));
    }
    getput_def_t;

static GETPUT_CALLBACK_DECL(rd_bootld_ver);
static GETPUT_CALLBACK_DECL(rd_ltr_regs);
static GETPUT_CALLBACK_DECL(wr_ltr_regs);
static GETPUT_CALLBACK_DECL(rd_fpga_regs);
static GETPUT_CALLBACK_DECL(wr_fpga_regs);
static GETPUT_CALLBACK_DECL(rd_uint32);
static GETPUT_CALLBACK_DECL(rd_rtc_time_rdy);
static GETPUT_CALLBACK_DECL(rd_rtc_time);
static GETPUT_CALLBACK_DECL(wr_rtc_time_prep);
static GETPUT_CALLBACK_DECL(wr_rtc_time);
static GETPUT_CALLBACK_DECL(rd_conf);
static GETPUT_CALLBACK_DECL(wr_command);
static GETPUT_CALLBACK_DECL(wr_conf_fact);
static GETPUT_CALLBACK_DECL(wr_conf_user);
static GETPUT_CALLBACK_DECL(rd_iface);

static const getput_def_t getarray_tab[] =
    {
    { IOADDR_LTR_REGS,      IOSIZE_LTR_REGS,            f_ltr_regs,             rd_ltr_regs },
    { IOADDR_MCS_VER,       IOSIZE_MCS_VER,             mcs_version,            NULL },
    { IOADDR_DUMMY,         IOSIZE_DUMMY,               dummy_array,            NULL },
    { IOADDR_FW_VER,        IOSIZE_FW_VER,              fw_ver_array,           NULL },
    { IOADDR_DEVDESCR,      IOSIZE_DEVDESCR,            &dev_descr,             NULL },
    { IOADDR_BOOTLD_VER,    IOSIZE_BOOTLD_VER,          NULL,                   rd_bootld_ver },
    { IOADDR_FPGAREGS_E,    IOSIZE_FPGAREGS_E,          NULL,                   rd_fpga_regs },
    { IOADDR_FPGAREGS_N,    IOSIZE_FPGAREGS_N,          NULL,                   rd_fpga_regs },
    { IOADDR_HW_REV,        IOSIZE_HW_REV,              hw_revision,            NULL },
    { IOADDR_RTC_PREP,      IOSIZE_RTC_PREP,            NULL,                   rd_rtc_time_rdy },
    { IOADDR_RTC,           IOSIZE_RTC,                 NULL,                   rd_rtc_time },
    { IOADDR_DATA_DIAG,     IOSIZE_DATA_DIAG,           &g_data_diag,           rd_uint32 },
#if DATA_DIAG_TRACE_LEN > 0
    { IOADDR_DATA_TRACE,    IOSIZE_DATA_TRACE,          &g_data_diag_trace,     rd_uint32 },
#endif
    { IOADDR_CUR_IFACE,     IOSIZE_CUR_IFACE,           NULL,                   rd_iface },
    { IOADDR_RAMTEST_RESULT,IOSIZE_RAMTEST_RESULT,      &f_sdram_test_result,   NULL },
    { IOADDR_CONF_FACT,     IOSIZE_CONF_FACT_R,         &g_conf_fact,           rd_conf },
    { IOADDR_CONF_USER,     IOSIZE_CONF_USER_R,         &g_conf_user,           rd_conf },
    };

static const getput_def_t putarray_tab[] =
    {
    { IOADDR_LTR_REGS,      IOSIZE_LTR_REGS,            f_ltr_regs,             wr_ltr_regs },
    { IOADDR_FPGAREGS_E,    IOSIZE_FPGAREGS_E,          NULL,                   wr_fpga_regs },
    { IOADDR_FPGAREGS_N,    IOSIZE_FPGAREGS_N,          NULL,                   wr_fpga_regs },
    { IOADDR_RTC_PREP,      IOSIZE_RTC_PREP,            NULL,                   wr_rtc_time_prep },
    { IOADDR_RTC,           IOSIZE_RTC,                 NULL,                   wr_rtc_time },
    { IOADDR_SYS_COMMAND,   IOSIZE_SYS_COMMAND,         NULL,                   wr_command },
    { IOADDR_CONF_FACT,     IOSIZE_CONF_FACT_W,         &g_conf_fact,           wr_conf_fact },
    { IOADDR_CONF_USER,     IOSIZE_CONF_USER_W,         &g_conf_user,           wr_conf_user },
    };
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static void strncpy_s(char* dest, const char* src, size_t size)
    {
    strncpy(dest, src, size);
    dest[size - 1] = '\0';
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static int f_apply_ltr_regs(void)
    {
    cpld_send_setspeed(f_ltr_regs[LTR_REG_HISPEED_MASK] & 0x0001);
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(rd_bootld_ver)
    {
    /* [+2] = minor, [+3] = major, остальные нули */
    while (len--)
        {
        uint8_t b;
        switch (offset++)
            {
            case 2:     b = (uint8_t)(f_bootld_ver & 0xFF); break;
            case 3:     b = (uint8_t)(f_bootld_ver >> 8); break;
            default:    b = 0; break;
            }
        *buf++ = b;
        }
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(rd_ltr_regs)
    {
    /* Обновить регистры наличия модуля и скорости */
    f_ltr_regs[LTR_REG_MODULE_MASK] = g_cpld_module_mask;
    f_ltr_regs[LTR_REG_HISPEED_MASK] = (uint16_t)cpld_send_getspeed();
    memcpy(buf, (uint8_t*)f_ltr_regs + offset, len);
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(wr_ltr_regs)
    {
    memcpy((uint8_t*)f_ltr_regs + offset, buf, len);
    f_ltr_regs[LTR_REG_MODULE_MASK] = g_cpld_module_mask; /* восстановить R/O регистр */
    f_ltr_regs[LTR_REG_HISPEED_MASK] &= 0x0001;  /* обнулить незначащие биты */
    return f_apply_ltr_regs();
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(rd_fpga_regs)
    {
    /* Обращение к регистрам FPGA сделано криво: в запросе от хоста 16-битные слова,
       а начальный адрес (offset) указывается в байтах */
    uint8_t (*get_func)(unsigned int) =
        (IOADDR_FPGAREGS_E == pdef->start_addr) ? fpga_reg_get_emu : fpga_reg_get_native;
    len /= 2;
    while (len--)
        {
        *buf++ = get_func(offset++);
        *buf++ = 0;
        }
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(wr_fpga_regs)
    {
    /* Обращение к регистрам FPGA сделано криво: в запросе от хоста 16-битные слова,
       а начальный адрес (offset) указывается в байтах */
    void (*set_func)(unsigned int, uint8_t) =
        (IOADDR_FPGAREGS_E == pdef->start_addr) ? fpga_reg_set_emu : fpga_reg_set_native;
    len /= 2;
    while (len--)
        {
        set_func(offset++, *buf);
        buf += 2;
        }
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(rd_uint32)
    {
    /* callback для чтения 32-битными словами (годится для массивов uint32, модифицируемых в прерывании) */
    if ((len % sizeof(uint32_t)) || (offset % sizeof(uint32_t)))
        return 0;
    const uint32_t* p = (const uint32_t*)pdef->data + (offset / sizeof(uint32_t));
    len /= sizeof(uint32_t);
    while (len--)
        {
        uint32_t tmp = *p++;
#if __BYTE_ORDER__ != __ORDER_LITTLE_ENDIAN__
#error "Little endian code!"
#endif
        *buf++ = (uint8_t)(tmp >> 0);
        *buf++ = (uint8_t)(tmp >> 8);
        *buf++ = (uint8_t)(tmp >> 16);
        *buf++ = (uint8_t)(tmp >> 24);
        }
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(rd_rtc_time_rdy)    /* готовность RTC к установке */
    {
    int result = 0;
    if (!offset && (len > 0))
        {
        *buf++ = !!rtc_ready_to_set();
        while (--len) *buf++ = 0;
        result = 1;
        }
    return result;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(rd_rtc_time)        /* чтение RTC */
    {
    int result = 0;
    if (!offset && (len >= sizeof(rtc_time_t)))
        {
        rtc_time_t tm;
        result = rtc_get_time(&tm);
        memcpy(buf, &tm, sizeof(tm));
        }
    return result;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(wr_rtc_time_prep)   /* подготовка к установке RTC */
    {
    if (!offset && (len >= 2) && (LTR021M_RTC_PREP_MAGIC == (buf[0] | (buf[1] << 8))))
        return rtc_set_prepare();
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(wr_rtc_time)        /* установка RTC */
    {
    int result = 0;
    if (!offset && (len >= sizeof(rtc_time_t)))
        {
        rtc_time_t tm;
        memcpy(&tm, buf, sizeof(tm));
        result = rtc_set_time(&tm);
        }
    return result;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(rd_conf)            /* чтение конфигурации */
    {
    /* Скопировать данные, исключая область пароля */
    size_t pw_ofs = (&g_conf_fact == pdef->data)
        ? offsetof(conf_fact_t, passwd) : offsetof(conf_user_t, passwd);
    for (size_t pos = offset; pos < offset + len; pos++)
        {
        *buf++ = ((pos < pw_ofs) || (pos >= pw_ofs + CONF_PASSWD_SIZE))
            ? ((const uint8_t*)pdef->data)[pos] : 0;
        }
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(wr_conf_fact)       /* запись конфигурации (factory) */
    {
    conf_fact_t tmp;

    /* Структура должна писаться целиком */
    if (offset || (len < sizeof(tmp)))
        return 0;

    memcpy(&tmp, buf, sizeof(tmp));

    /* Проверить пароль */
    if (strncmp(tmp.passwd, g_conf_fact.passwd, sizeof(tmp.passwd)))
        return 0;

    /* Если задан новый пароль, записать его на место старого */
    if (len >= sizeof(tmp) + CONF_PASSWD_SIZE)
        {
        strncpy_s(tmp.passwd, (char*)buf + sizeof(tmp), sizeof(tmp.passwd));
        }
    else
        { /* сохранить старый пароль */
        memcpy(tmp.passwd, g_conf_fact.passwd, sizeof(tmp.passwd));
        }

    return conf_save_fact(&tmp);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(wr_conf_user)       /* запись конфигурации (user) */
    {
    conf_user_t tmp;

    /* Структура должна писаться целиком */
    if (offset || (len < sizeof(tmp)))
        return 0;

    memcpy(&tmp, buf, sizeof(tmp));

    /* Проверить пароль */
    /* По интерфейсу USB вместо пароля подходит серийный номер */
    if  (
        strncmp(tmp.passwd, g_conf_user.passwd, sizeof(tmp.passwd))
        &&
        ((CONF_IFACE_USB != iface) || strncmp(tmp.passwd, g_conf_fact.serial, sizeof(tmp.passwd)))
        )
        return 0;

    /* Если задан новый пароль, записать его на место старого */
    if (len >= sizeof(tmp) + CONF_PASSWD_SIZE)
        {
        strncpy_s(tmp.passwd, (char*)buf + sizeof(tmp), sizeof(tmp.passwd));
        }
    else
        { /* сохранить старый пароль */
        memcpy(tmp.passwd, g_conf_user.passwd, sizeof(tmp.passwd));
        }

    return conf_save_user(&tmp);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(wr_command)         /* системная команда */
    {
    if (!offset && (len >= sizeof(uint32_t)))
        {
        uint32_t cmd;
        memcpy(&cmd, buf, sizeof(cmd));
        switch (cmd)
            {
            case LTR021M_CMD_REBOOT:
                boot_schedule_reset(BOOT_RST_NORMAL);
                break;
            case LTR021M_CMD_GO_BOOTLDR:
                boot_schedule_reset(
                    (CONF_IFACE_USB == iface) ? BOOT_RST_BOOTLDR_USB : BOOT_RST_BOOTLDR_NET);
                break;
            case LTR021M_CMD_SDRAM_TEST:
                f_sdram_test_result = sdram_test_runtime(); /* long operation! */
                break;
            default:
                return 0;
            }
        return 1;
        }
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static GETPUT_CALLBACK_DECL(rd_iface)           /* чтение активного интерфейса */
    {
    buf[0] = hostio_get_active_iface();
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_reset_registers(void)
    {
    /* установить виртуальные "регистры крейта" и регистры ПЛИС по умолчанию */
    memset(f_ltr_regs, 0, sizeof(f_ltr_regs));  /* здесь hispeed = 0 */
#if TEST_START_AT_HISPEED
    f_ltr_regs[LTR_REG_HISPEED_MASK] = 0x0001;
#endif
    fpga_reg_reset();   /* Сбросить регистры ПЛИС */
    f_apply_ltr_regs(); /* Применить значения регистров крейта */
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static uint16_t* f_add_fpga_str(uint16_t* dest, uint16_t* dest_end, const char* src)
    {
    while ((dest != dest_end) && *src)
        {
        *dest++ = (uint16_t)(0xE000 | *src++);
        }
    return dest;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void ctlfunc_startup(void)
    {
    /* Заполнение комментария в fpga_info */
    uint16_t* fi_ptr = g_ctlfunc_fpga_info;
    uint16_t* fi_end = g_ctlfunc_fpga_info + ARRLEN(g_ctlfunc_fpga_info);

    fi_ptr = f_add_fpga_str(fi_ptr, fi_end, "<Name>{");
    fi_ptr = f_add_fpga_str(fi_ptr, fi_end, fwinfo_get_dev_name());
    fi_ptr = f_add_fpga_str(fi_ptr, fi_end, "}<Version>{" FWINFO_FW_VERSION_STR "}<Comments>{No FPGA}");
    while (fi_ptr != fi_end)
        {
        *fi_ptr++ = 0xE000;
        }

    /* Чтение версии бутлоадера */
    f_bootld_ver = boot_loader_version();

    /* Заполнение имени и серийного номера в дескрипторе */
    strncpy_s(dev_descr.name, fwinfo_get_dev_name(), sizeof(dev_descr.name));
    strncpy_s(dev_descr.serial_no, g_conf_fact.serial, sizeof(dev_descr.serial_no));

    f_reset_registers();
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
const char* ctlfunc_get_module_name(int* p_size)
    {
#ifdef FWINFO_FAKE_MODULE_NAME
    static const char fake_module_name[16] = FWINFO_FAKE_MODULE_NAME;
    if (p_size)
        *p_size = sizeof(fake_module_name);
    return fake_module_name;
#else
    if (p_size)
        *p_size = sizeof(dev_descr.name);
    return dev_descr.name;
#endif
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int ctlfunc_get_array
    (
    conf_iface_t iface,             /* с какого интерфейса обращение */
    uint32_t addr,                  /* адрес чтения */
    size_t len,                     /* длина в байтах */
    uint8_t* dest                   /* данные */
    )                               /* Возвращает bool success */
    {
    int result = 0;

    for (const getput_def_t* pd = getarray_tab; pd != ARRAY_END(getarray_tab); pd++)
        {
        uint32_t obj_start = pd->start_addr;
        uint32_t obj_end = obj_start + pd->size;
        if ((addr >= obj_start) && (addr < obj_end) && (len <= obj_end - addr))
            {
            uint32_t ofs = addr - obj_start;
            if (pd->callback)
                {
                result = pd->callback(pd, iface, ofs, len, dest);
                }
            else if (pd->data)
                {
                memcpy(dest, (const uint8_t*)pd->data + ofs, len);
                result = 1;
                }
            break;
            }
        }
    return result;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int ctlfunc_put_array
    (
    conf_iface_t iface,             /* с какого интерфейса обращение */
    uint32_t addr,                  /* адрес чтения */  
    size_t len,                     /* длина в байтах */
    const uint8_t* src              /* данные */        
    )                               /* Возвращает bool success */
    {
    int result = 0;

    for (const getput_def_t* pd = putarray_tab; pd != ARRAY_END(putarray_tab); pd++)
        {
        uint32_t obj_start = pd->start_addr;
        uint32_t obj_end = obj_start + pd->size;
        if ((addr >= obj_start) && (addr < obj_end) && (len <= obj_end - addr))
            {
            uint32_t ofs = addr - obj_start;
            if (pd->callback)
                {
                result = pd->callback(pd, iface, ofs, len, (uint8_t*)src);
                }
            else if (pd->data)
                {
                memcpy((uint8_t*)pd->data + ofs, src, len);
                result = 1;
                }
            break;
            }
        }
    return result;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void ctlfunc_reset_fpga(void)
    {
    hostio_reset(); /* сбросить сессию хоста, начать с посылки FPGA INFO */
    f_reset_registers();
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
