/*================================================================================================*
 * Отображение карты регистров FPGA LTR030 на регистры CPLD LTR021M
 *================================================================================================*/

#ifndef FPGA_REG_H_
#define FPGA_REG_H_

#include "global.h"

/*================================================================================================*/
#define FPGA_REG_COUNT_EMU          16  /* Количество эмулируемых регистров */
#define FPGA_REG_COUNT_NATIVE       6   /* Количество реальных регистров ПЛИС */

enum en_fpga_regidx_emu         /* Номера эмулируемых регистров */
    {
    FPGA_REGIDX_EMU_INIT            = 0x00, /* w: bit0=#RST FPGA (ignored); r: FPGA info */
    FPGA_REGIDX_EMU_TEST1           = 0x01, /* w: bit0=ring mode, r: 0 */
    /* 0x02..0x03: module speed,
       0x04..0x05: module mask
       - не поддерживаются, с ними работают через эмуляцию регистров LTR010 (LTR_REGS)
     */
    FPGA_REGIDX_EMU_FANCTL          = 0x06, /* w: ignored */
    FPGA_REGIDX_EMU_TEST2           = 0x07, /* r/w: any, initial state = board version = 1 */
    FPGA_REGIDX_EMU_LEDCTL          = 0x08, /* w: ignored */
    FPGA_REGIDX_EMU_DIGOUTEN        = 0x09, /* w: bit0 = DIGOUT{1,2} enable */
    FPGA_REGIDX_EMU_DIGOUT1         = 0x0A, /* w: DIGOUT1 source */
    FPGA_REGIDX_EMU_DIGOUT2         = 0x0B, /* w: DIGOUT2 source */
    FPGA_REGIDX_EMU_PF0CTL          = 0x0C, /* w: ignored */
    FPGA_REGIDX_EMU_PG13CTL         = 0x0D, /* w: ignored */
    FPGA_REGIDX_EMU_STARTCTL        = 0x0E, /* w: START mark control */
    FPGA_REGIDX_EMU_1SCTL           = 0x0F  /* w: 1S mark control */
    };

enum en_fpga_regval_emu         /* Значения эмулируемых регистров */
    {
    /* регистр INIT */
    FPGA_REGVAL_EMU_INIT_nRST       = 0x01,
    /* регистр TEST1 */
    FPGA_REGVAL_EMU_TEST1_RINGMODE  = 0x01,
    /* регистр DIGOUTEN */
    FPGA_REGVAL_EMU_DIGOUTEN_ENA    = 0x01,
    /* регистры DIGOUT1, DIGOUT2 */
    FPGA_REGVAL_EMU_DIGOUTx_CONST0  = 0,
    FPGA_REGVAL_EMU_DIGOUTx_CONST1  = 1,
    FPGA_REGVAL_EMU_DIGOUTx_UARTTX  = 2,
    FPGA_REGVAL_EMU_DIGOUTx_PG13    = 3,
    FPGA_REGVAL_EMU_DIGOUTx_DIGIN1  = 4,
    FPGA_REGVAL_EMU_DIGOUTx_DIGIN2  = 5,
    FPGA_REGVAL_EMU_DIGOUTx_START   = 6,
    FPGA_REGVAL_EMU_DIGOUTx_1S      = 7,
    FPGA_REGVAL_EMU_DIGOUTx_IRIG    = 8,
    /* регистр STARTCTL */
    FPGA_REGVAL_EMU_STARTCTL_OFF    = 0,
    FPGA_REGVAL_EMU_STARTCTL_DIGIN1R= 1,
    FPGA_REGVAL_EMU_STARTCTL_DIGIN1F= 2,
    FPGA_REGVAL_EMU_STARTCTL_DIGIN2R= 3,
    FPGA_REGVAL_EMU_STARTCTL_DIGIN2F= 4,
    FPGA_REGVAL_EMU_STARTCTL_NOW    = 5,
    /* регистр 1SCTL */
    FPGA_REGVAL_EMU_1SCTL_OFF       = 0,
    FPGA_REGVAL_EMU_1SCTL_DIGIN1R   = 1,
    FPGA_REGVAL_EMU_1SCTL_DIGIN1F   = 2,
    FPGA_REGVAL_EMU_1SCTL_DIGIN2R   = 3,
    FPGA_REGVAL_EMU_1SCTL_DIGIN2F   = 4,
    FPGA_REGVAL_EMU_1SCTL_NOW       = 5,
    FPGA_REGVAL_EMU_1SCTL_1S        = 6
    };

enum en_fpga_regidx_native      /* Номера нативных регистров */
    {
    FPGA_REGIDX_NAT_DIGOUTEN        = 0x00,
    FPGA_REGIDX_NAT_DIGOUT1         = 0x01,
    FPGA_REGIDX_NAT_DIGOUT2         = 0x02,
    FPGA_REGIDX_NAT_STARTCTL        = 0x03,
    FPGA_REGIDX_NAT_1SCTL           = 0x04,
    FPGA_REGIDX_NAT_TEST            = 0x05
    };

enum en_fpga_regval_native      /* Значения нативных регистров */
    {
    /* регистр DIGOUTEN */
    FPGA_REGVAL_NAT_DIGOUTEN_ENA    = 0x01,
    /* регистры DIGOUT1, DIGOUT2 */
    FPGA_REGVAL_NAT_DIGOUTx_UARTTX  = 0,
    FPGA_REGVAL_NAT_DIGOUTx_DIGIN1  = 1,
    FPGA_REGVAL_NAT_DIGOUTx_DIGIN2  = 2,
    FPGA_REGVAL_NAT_DIGOUTx_START   = 3,
    FPGA_REGVAL_NAT_DIGOUTx_1S      = 4,
    FPGA_REGVAL_NAT_DIGOUT2_UARTDTR = 5,
    /* регистр STARTCTL */
    FPGA_REGVAL_NAT_STARTCTL_DIGIN1R= 0,
    FPGA_REGVAL_NAT_STARTCTL_DIGIN1F= 1,
    FPGA_REGVAL_NAT_STARTCTL_DIGIN2R= 2,
    FPGA_REGVAL_NAT_STARTCTL_DIGIN2F= 3,
    FPGA_REGVAL_NAT_STARTCTL_OFF    = 4,
    FPGA_REGVAL_NAT_STARTCTL_TIMER  = 5,
    /* регистр 1SCTL */
    FPGA_REGVAL_NAT_1SCTL_OFF       = 0,
    FPGA_REGVAL_NAT_1SCTL_TIMER     = 1,
    FPGA_REGVAL_NAT_1SCTL_DIGIN1R   = 2,
    FPGA_REGVAL_NAT_1SCTL_DIGIN1F   = 3,
    FPGA_REGVAL_NAT_1SCTL_DIGIN2R   = 4,
    FPGA_REGVAL_NAT_1SCTL_DIGIN2F   = 5,
    /* регистр TEST */
    FPGA_REGVAL_NAT_TEST_RINGMODE   = 0x01
    };
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void fpga_reg_reset(void);
/*------------------------------------------------------------------------------------------------*/
uint8_t fpga_reg_get_emu(unsigned int idx);
/*------------------------------------------------------------------------------------------------*/
uint8_t fpga_reg_get_native(unsigned int idx);
/*------------------------------------------------------------------------------------------------*/
void fpga_reg_set_emu(unsigned int idx, uint8_t val);
/*------------------------------------------------------------------------------------------------*/
void fpga_reg_set_native(unsigned int idx, uint8_t val);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
#endif /* FPGA_REG_H_ */
