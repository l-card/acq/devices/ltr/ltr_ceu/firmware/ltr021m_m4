/*================================================================================================*
 * Обмен данными и командами с ПЛИС
 *================================================================================================*/

#ifndef CPLD_H_
#define CPLD_H_

#include "global.h"
#include "settings.h"
#include "m0_task.h"

/*================================================================================================*/
/* Размер буфера приемника задается блоками 16-битных слов */
#define CPLD_RXBUF_BLKLEN       0x800
#define CPLD_RXBUF_NBLOCKS      8
#define CPLD_RXBUF_DECL         __attribute__ ((section(".bss2")))
#define CPLD_RXBUF_LEN          (CPLD_RXBUF_NBLOCKS * CPLD_RXBUF_BLKLEN)

#define CPLD_RX_DMA_PTR         ((volatile const uint16_t*)LPC_GPDMA->CH[SET_DMACH_SSP_IN_RX].DESTADDR)

/* Если CPLD_RX_DMA_IRQ_BITS == 1, то в конфигурации канала DMA на прием данных включаются запросы
   прерывания по окончанию передачи одного блока.
   (само прерывание DMA через NVIC в cpld.c не включается и обработчика нет,
   но его может использовать верхний уровень) */
#define CPLD_RX_DMA_IRQ_BITS    1

/* Параметры интерфейса регистров ПЛИС */
#define CPLD_REGS_MAX           64  /* максимальное количество регистров */
#define CPLD_REGS_TIMEOUT_uS    100 /* таймаут, мкс */
/*================================================================================================*/

/*================================================================================================*/
/* Буфер чтения */
extern volatile uint16_t g_cpld_recv_buf[CPLD_RXBUF_LEN];
extern volatile const uint16_t* volatile g_cpld_recv_rptr;

/* Признак присутствия модуля (обновляется cpld_worker) */
extern uint8_t g_cpld_module_mask;  /* 1 = модуль присутствует */
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void cpld_init(void);
/*------------------------------------------------------------------------------------------------*/
void cpld_start(void);
/*------------------------------------------------------------------------------------------------*/
void cpld_stop(void);
/*------------------------------------------------------------------------------------------------*/
static inline size_t cpld_recv_words_avail(void)
    {
    volatile const uint16_t* head = g_cpld_recv_rptr;
    volatile const uint16_t* tail = CPLD_RX_DMA_PTR;
    return (size_t)((tail >= head) ? tail - head : CPLD_RXBUF_LEN + tail - head);
    }
/*------------------------------------------------------------------------------------------------*/
static inline uint16_t cpld_recv_get_word(void) /* free condition unchecked! */
    {
    volatile const uint16_t* head = g_cpld_recv_rptr;
    uint16_t data = *head & 0x3FF;
    if (++head >= g_cpld_recv_buf + CPLD_RXBUF_LEN)
        head = g_cpld_recv_buf;
    g_cpld_recv_rptr = head;
    return data;
    }
/*------------------------------------------------------------------------------------------------*/
int cpld_send_getspeed(void);
/*------------------------------------------------------------------------------------------------*/
void cpld_send_setspeed(int hi_speed);
/*------------------------------------------------------------------------------------------------*/
static inline size_t cpld_send_words_avail(void)
    {
    ssize_t free = (ssize_t)g_m0_tx_head - (ssize_t)g_m0_tx_tail - 1;
    if (free < 0) free += M0_TX_BUF_LEN;
    return (size_t)free;
    }
/*------------------------------------------------------------------------------------------------*/
size_t cpld_send(volatile const uint16_t* buf, size_t nwords);
/*------------------------------------------------------------------------------------------------*/
static inline size_t cpld_send_word(uint16_t word)
    {
    size_t tail = g_m0_tx_tail;
    size_t new_tail = (tail + 1) % M0_TX_BUF_LEN;
    if (new_tail == g_m0_tx_head)
        return 0;
    g_m0_tx_buf[tail] = word;
    g_m0_tx_tail = new_tail;
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/
static inline size_t cpld_send_3words(uint16_t w0, uint16_t w1, uint16_t w2) {
    size_t tail = g_m0_tx_tail;
    size_t head = g_m0_tx_head;
    size_t free = head > tail ? head - tail - 1 : M0_TX_BUF_LEN + head - tail - 1;
    if (free < 3)
        return 0;
    g_m0_tx_buf[tail++] = w0; tail %= M0_TX_BUF_LEN;
    g_m0_tx_buf[tail++] = w1; tail %= M0_TX_BUF_LEN;
    g_m0_tx_buf[tail++] = w2; tail %= M0_TX_BUF_LEN;
    g_m0_tx_tail = tail;
    return 3;
}
/*------------------------------------------------------------------------------------------------*/
int cpld_write_regs(unsigned int first_addr, const uint8_t* buf, size_t count);
/*------------------------------------------------------------------------------------------------*/
void cpld_worker(void);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/

#endif /* CPLD_H_ */
