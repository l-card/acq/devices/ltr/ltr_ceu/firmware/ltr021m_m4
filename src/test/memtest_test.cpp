#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define NBLK    3
#define BLKLEN  4
uint8_t test_buf[NBLK * BLKLEN];

template<typename T>
static void wmem(volatile T* p, T x)
    {
    printf("W[%04X] %0*X\n", p - (volatile T*)test_buf, sizeof(T) * 2, x);
    *p = x;
    }

template<typename T>
static T rmem(volatile T* p)
    {
    printf("R[%04X] %0*X\n", p - (volatile T*)test_buf, sizeof(T) * 2, *p);
    return *p;
    }

static void progress(size_t n, size_t m)
    {
    printf("progress: %u / %u\n", n, m);
    }

typedef void (*memtest_progress_func_t)(size_t n, size_t m);

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/* Quick memory test (write sequential numbers)
   Returns: NULL on success or fault address
 */
void* memtest_quick_32bit
    (
    void* memory,
    size_t block_size,
    size_t nblocks,
    memtest_progress_func_t progress_func
    )
    {
    typedef uint32_t memword_t;
    typedef volatile memword_t* memptr_t;
    const size_t blklen = block_size / sizeof(memword_t);
    const memptr_t mem_start = (memptr_t)memory;
    const memptr_t mem_end = mem_start + blklen * nblocks;
    size_t n = 0, m = nblocks * 2;

#define DO_UP(stmt) do { \
    memptr_t p;                                     \
    for (p = mem_start; p != mem_end; )             \
        {                                           \
        memptr_t eblk = p + blklen;                 \
        for (; p != eblk; p++)                      \
           { stmt; }                                \
        if (progress_func) progress_func(++n, m);   \
        }                                           \
    } while (0)

    DO_UP( wmem(p, (memword_t)p) );
    DO_UP( if (rmem(p) != (memword_t)p) return (void*)p );

#undef DO_UP

    return NULL;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/* "March C-" on 16-bit memory.
   Returns: NULL on success or fault address
 */
void* memtest_march_c_16bit
    (
    void* memory,
    size_t block_size,
    size_t nblocks,
    memtest_progress_func_t progress_func
    )
    {
    typedef uint16_t memword_t;
    typedef volatile memword_t* memptr_t;
    const size_t blklen = block_size / sizeof(memword_t);
    const memptr_t mem_start = (memptr_t)memory;
    const memptr_t mem_end = mem_start + blklen * nblocks;
    size_t n = 0, m = nblocks * 6 * (4 + (sizeof(memword_t) > 1) + (sizeof(memword_t) > 2));
    memword_t x = 0;
    unsigned int nbit = sizeof(memword_t) * 8;

#define DO_UP(stmt) do { \
    memptr_t p;                                     \
    for (p = mem_start; p != mem_end; )             \
        {                                           \
        memptr_t eblk = p + blklen;                 \
        for (; p != eblk; p++)                      \
           { stmt; }                                \
        if (progress_func) progress_func(++n, m);   \
        }                                           \
    } while (0)

#define DO_DN(stmt) do { \
    memptr_t p;                                     \
    for (p = mem_end; p != mem_start; )             \
        {                                           \
        memptr_t eblk = p - blklen;                 \
        for (; p != eblk; )                         \
           { p--; stmt; }                           \
        if (progress_func) progress_func(++n, m);   \
        }                                           \
    } while (0)

    do
        {
        memword_t not_x = (memword_t)~x;

        /* write x, any direction */
        DO_UP( wmem(p, x) );

        /* read x, write ~x, upwards */
        DO_UP( if (rmem(p) != x) return (void*)p; wmem(p, not_x) );

        /* read ~x, write x, upwards */
        DO_UP( if (rmem(p) != not_x) return (void*)p; wmem(p, x) );

        /* read x, write ~x, downwards */
        DO_DN( if (rmem(p) != x) return (void*)p; wmem(p, not_x) );

        /* read ~x, write x, downwards */
        DO_DN(if (rmem(p) != not_x) return (void*)p; wmem(p, x) );

        /* read x, any direction */
        DO_UP( if (rmem(p) != x) return (void*)p );

        /* choose next pattern: 0000, FF00, F0F0, CCCC, AAAA */
        nbit >>= 1;
        x ^= (memword_t)(~x << nbit);
        }
    while (nbit != 0);

#undef DO_DN
#undef DO_UP

    return NULL;
    }
/*------------------------------------------------------------------------------------------------*/

int main(void)
    {
    void* result = memtest_march_c_16bit(test_buf, BLKLEN, NBLK, progress);
    puts((result) ? "FAIL" : "PASS");
    }

/*================================================================================================*/
