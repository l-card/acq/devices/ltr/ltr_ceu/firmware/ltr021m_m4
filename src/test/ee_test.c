#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>

#define EEPROM_SIZE         512
#define EEPROM_PAGE_SIZE    128

typedef uint32_t ee_word_t;

#define PAGE_WORDS          (EEPROM_PAGE_SIZE / sizeof(ee_word_t))
ee_word_t ee_array[EEPROM_SIZE / sizeof(ee_word_t)];
ee_word_t ee_pagebuf[PAGE_WORDS];
unsigned int ee_wpage_idx = -1U;
char ee_buf_used[PAGE_WORDS];

#define DIE(...) do { fprintf(stderr, __VA_ARGS__); exit(1); } while (0)

static ee_word_t ee_read_at(uintptr_t addr)
    {
    ee_word_t val;
    if (addr % sizeof(ee_word_t))
        DIE("Unaligned EEPROM read @ 0x%X\n", addr);
    val = ee_array[addr / sizeof(ee_word_t)];
    printf("read [0x%X] => 0x%08X\n", addr, val);
    return val;
    }

static void ee_write_at(uintptr_t addr, ee_word_t val)
    {
    if (addr % sizeof(ee_word_t))
        DIE("Unaligned EEPROM write @ 0x%X\n", addr);
    printf("write [0x%X] <= 0x%08X\n", addr, val);
    ee_pagebuf[(addr / sizeof(ee_word_t)) % PAGE_WORDS] = val;
    ee_buf_used[(addr / sizeof(ee_word_t)) % PAGE_WORDS] = 1;
    ee_wpage_idx = addr / EEPROM_PAGE_SIZE;
    }

static void ee_commit(void)
    {
    unsigned int i;
    if (ee_wpage_idx >= EEPROM_SIZE / EEPROM_PAGE_SIZE)
        DIE("Invalid commit\n");
    printf("commit page [0x%X]\n", ee_wpage_idx * EEPROM_PAGE_SIZE);
    for (i = 0; i < PAGE_WORDS; i++)
        {
        if (ee_buf_used[i])
            {
            ee_array[ee_wpage_idx * PAGE_WORDS + i] = ee_pagebuf[i];
            ee_buf_used[i] = 0;
            }
        }
    }

/*------------------------------------------------------------------------------------------------*/
static uintptr_t f_read_eeprom(uintptr_t ee_addr, uint8_t* buf, size_t len)
    {
    /* Read EEPROM, converting byte access to 32-bit access */
    union { ee_word_t w; uint8_t b[sizeof(ee_word_t)]; } t;
    uintptr_t byte_ofs = ee_addr % sizeof(ee_word_t);
    t.w = ee_read_at(ee_addr - byte_ofs);
    while (len--)
        {
        *buf++ = t.b[byte_ofs];
        byte_ofs = (++ee_addr) % sizeof(ee_word_t);
        if (!byte_ofs && len)
            t.w = ee_read_at(ee_addr);
        }
    return ee_addr;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static uintptr_t f_write_eeprom(uintptr_t ee_addr, const uint8_t* buf, size_t len)
    {
    /* Write EEPROM, converting byte access to 32-bit access */
    union { ee_word_t w; uint8_t b[sizeof(ee_word_t)]; } t;
    uintptr_t byte_ofs = ee_addr % sizeof(ee_word_t);
    uintptr_t ret_val = ee_addr + len;

    ee_addr -= byte_ofs;
    t.w = ee_read_at(ee_addr);
    while (len--)
        {
        t.b[byte_ofs++] = *buf++;
        byte_ofs %= sizeof(ee_word_t);
        if (!byte_ofs || !len)
            {
            ee_write_at(ee_addr, t.w);
            ee_addr += sizeof(ee_word_t);
            if (!len || !(ee_addr % EEPROM_PAGE_SIZE))
                { /* � ����� �������� ��� ����� ������ ���������� ����� ��������� ���������������� */
                ee_commit();
                }
            if (len && (len < sizeof(ee_word_t)))
                t.w = ee_read_at(ee_addr);
            }
        }
    return ret_val;
    }
/*------------------------------------------------------------------------------------------------*/

void dump(const uint8_t* buf, size_t len)
    {
    size_t i;
    for (i = 0; i < len; i++)
        {
        printf("%02X ", *buf++);
        if (3 == (i % 4)) printf(" ");
        if ((len - 1 == i) || (15 == (i % 16))) puts("");
        }
    puts("");
    }

void read_dump(uintptr_t addr, uint8_t* buf, size_t len)
    {
    f_read_eeprom(addr, buf, len);
    dump(buf, len);
    }

int main(void)
    {
#define START 120
    const uint8_t w1[] = "foo bar baz";
    const uint8_t w2[] = "AAA";
    uint8_t r1[16];

    memset(ee_array, 0xFF, sizeof(ee_array));
    memset(ee_pagebuf, 0, sizeof(ee_pagebuf));
    memset(ee_buf_used, 0, sizeof(ee_buf_used));

    f_write_eeprom(START + 3, w1, sizeof(w1));

    read_dump(START + 0, r1, sizeof(r1));

    f_write_eeprom(START + 7, w2, sizeof(w2));

    read_dump(START + 0, r1, sizeof(r1));

    //dump((void*)ee_array, sizeof(ee_array));

    return 0;
    }
