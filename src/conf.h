#ifndef CONF_H_
#define CONF_H_

#include "global.h"

/*================================================================================================*/
#define CONF_SERIAL_SIZE        16
#define CONF_PASSWD_SIZE        16
#define CONF_MAC_SIZE           6
#define CONF_IP_SIZE            4

#define CONF_FACT_SIG           0xEEDA0001
#define CONF_USER_SIG           0xEEDB0001

/* Значения для поля выбора интерфейса (iface) */
typedef enum
    {
    CONF_IFACE_INVALID = 0,
    CONF_IFACE_USB,                     /* USB */
    CONF_IFACE_NET                      /* Ethernet, TCP/IP */
    }
    conf_iface_t;

/* Битовые флаги для опций интерфейса CONF_IFACE_NET */
#define CONF_NETOPT_USERMAC     0x01    /* 1 = использовать user mac_addr, 0 = factory mac_addr */
#define CONF_NETOPT_DHCP        0x02    /* 1 = получать адрес через DHCP, 0 = static IP */
#define CONF_NETOPT_NODELAY_CMD 0x10    /* отключить Nagle Algorithm для командного сокета */
#define CONF_NETOPT_NODELAY_DTA 0x20    /* отключить Nagle Algorithm для сокета данных */

#define CONST_ALIAS(var)        (*(const typeof(var)*)&(var))
/*================================================================================================*/

/*================================================================================================*/
/* Видимая приложению структура с конфигурацией */
typedef struct
    {
    uint32_t signature;
    uint32_t size;
    char passwd[CONF_PASSWD_SIZE];
    char serial[CONF_SERIAL_SIZE];
    uint8_t mac_addr[CONF_MAC_SIZE];
    uint8_t reserved[2];
    }
    conf_fact_t;

typedef struct
    {
    uint32_t signature;
    uint32_t size;
    char passwd[CONF_PASSWD_SIZE];
    uint8_t iface;                      /* Выбор интерфейса (CONF_IFACE_...) */
    uint8_t net_options;                /* Опции сетевого интерфейса */
    uint8_t mac_addr[CONF_MAC_SIZE];    /* используется, если в net_options USERMAC == 1 */
    uint8_t ip_addr[CONF_IP_SIZE];
    uint8_t ip_netmask[CONF_IP_SIZE];
    uint8_t ip_gateway[CONF_IP_SIZE];
    }
    conf_user_t;

/* Рабочая копия конфигурации в RAM (read/write) */
extern conf_fact_t g_conf_fact_w;
extern conf_user_t g_conf_user_w;

/* Рабочая копия конфигурации в RAM (read only alias) */
#define g_conf_fact CONST_ALIAS(g_conf_fact_w)
#define g_conf_user CONST_ALIAS(g_conf_user_w)
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
int conf_load(void);                /* Чтение конфигурации из EEPROM в g_conf_... */
/*------------------------------------------------------------------------------------------------*/
int conf_save_fact                  /* Запись фабричной конфигурации */
    (
    const conf_fact_t* conf         /* записываемая структура */
    );
/*------------------------------------------------------------------------------------------------*/
int conf_save_user                  /* Запись пользовательской конфигурации */
    (
    const conf_user_t* conf         /* записываемая структура */
    );
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
#endif /* CONF_H_ */
