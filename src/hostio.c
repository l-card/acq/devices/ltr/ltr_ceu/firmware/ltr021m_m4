#include "hostio.h"
#include "data.h"
#include "usb_bulk.h"
#include "tcpserver.h"
#include "cpld.h"
#include "ctlfunc.h"
#include "ledblink.h"
#include "test.h"

/*================================================================================================*/
/* Состояние (битовые маски) */
enum { ST_CLOSED, ST_WAIT_RESET, ST_RUNNING, ST_SEND_FPGA_INFO };
/*================================================================================================*/

/*================================================================================================*/
static const uint32_t f_module_update_signal[] =
    { DATA_SIGNAL_PREFIX, DATA_SIGNAL_MODULE_MASK_UPDATED };

/* Активный интерфейс */
static conf_iface_t f_active_iface = CONF_IFACE_INVALID;

/* Последняя маска модулей, сообщенная хосту */
static uint8_t f_last_sent_module_mask = 0;

/* Вспомогательный размер не кратного 4 остатка при чтении */
static size_t f_read_ofs = 0;

/* Состояние алгоритма */
static uint8_t f_state = ST_CLOSED;

/* Указатели на функции нижнего уровня (зависят от выбранного интерфейса) */
static void (*iface_worker)(void);
static size_t (*iface_send_avail)(void);
static size_t (*iface_recv)(uint8_t* buf, size_t len); 
static size_t (*iface_send)(const uint8_t* buf, size_t len, int push);
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static void f_host_read(void) {
    /* Чтение данных от хоста и передача в модуль */
    size_t bytes_free = data_out_contig_words_free() * sizeof(data_host_out_t);
    if (bytes_free > 0) {
        size_t len = f_read_ofs + iface_recv((uint8_t*)data_out_cur_ptr() + f_read_ofs,
            bytes_free - f_read_ofs);
        /* Если считалось не кратное размеру слова, то лишние байты останутся в памяти,
           в слове, которое будет иметь адрес data_out_cur_ptr() после сдвига на целые слова. */
        f_read_ofs = len % sizeof(data_host_out_t);
        data_out_push_words(len / sizeof(data_host_out_t));
    }
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static inline uint8_t f_send_block(const volatile data_host_in_t *data, size_t nw, int push) {
    uint8_t ok = 0;
    /* тут мы используем факт, что наличие свободного места уже было проверено, а значит
     * передача должна пройти полностью, либо завершиться с ошибкой.
     * в связи с этим мы проверяем только на факт > 0 и нам также не нужно
     * при передаче пытаться обрабатывать нецелое число переданных слов,
     * что упрощает алгортм передачи */
    if (iface_send((uint8_t*)data, nw * sizeof(data_host_in_t), push) > 0) {
        data_in_pop_words(nw);
        ok = 1;
    }
    return ok;
}


static void f_host_write(void) {
    /* Чтение данных от модуля и передача в хост */
    size_t nw = data_in_words_rdy();
    size_t max_words = iface_send_avail() / sizeof(data_host_in_t);
    if (nw > max_words)
        nw = max_words;
    if (nw > 0) {
        /* Длина уже проверена - send вернет либо полную длину, либо 0 */
        volatile const data_host_in_t* cur_ptr = data_in_cur_ptr();
        size_t nw_to_end = (size_t)(&g_data_in_buf[ARRLEN(g_data_in_buf)] - cur_ptr);
        if (nw <= nw_to_end) { /* нет перехода через начало массива - послать одним куском */
            f_send_block(cur_ptr, nw, 0);
        } else { /* есть переход - послать двумя кусками. второй посылаем только если полностью передан первый */
            if (f_send_block(cur_ptr, nw_to_end, 0)) {
                f_send_block(g_data_in_buf, nw - nw_to_end, 0);
            }
        }
    }
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void hostio_init(conf_iface_t iface)
    {
    switch (iface)
        {
        case CONF_IFACE_USB:
            iface_worker = usbbulk_worker;
            iface_recv = usbbulk_recv;
            iface_send = usbbulk_send;
            iface_send_avail = usbbulk_send_buf_avail;
            usbbulk_init();
            break;

        case CONF_IFACE_NET:
            iface_worker = tcpserver_worker;
            iface_recv = tcpserver_data_recv;
            iface_send = tcpserver_data_send;
            iface_send_avail = tcpserver_data_send_avail;
            tcpserver_init();
            break;

        default:
            f_active_iface = CONF_IFACE_INVALID;
            f_state = ST_CLOSED;
            return;
        }
    f_active_iface = iface;
    f_state = ST_WAIT_RESET;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
conf_iface_t hostio_get_active_iface(void) {
    return f_active_iface;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void hostio_close(void) {
    if ((f_state != ST_CLOSED) && (CONF_IFACE_NET == f_active_iface))
        tcpserver_hangup();
    f_state = ST_CLOSED;
    f_active_iface = CONF_IFACE_INVALID;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void hostio_reset(void) {
    f_last_sent_module_mask = 0; /* после сброса хост считает, что модулей нет */
    f_read_ofs = 0;
    data_restart(); /* очистить буферы преобразования данных и сбросить связь с ПЛИС */

    if (f_state != ST_CLOSED)
        switch (f_active_iface) {
        case CONF_IFACE_USB:
            usbbulk_reset();
            /* Для USB дополнительно требуется посылка FPGA info. Для TCP она не нужна */
            f_state = ST_SEND_FPGA_INFO;
            break;
        case CONF_IFACE_NET:
            tcpserver_data_reset();
            f_state = ST_RUNNING;
            break;
        default:
            break;
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void hostio_worker(void) {
#if TEST_HOSTIO_LOOPBACK
    static uint8_t __attribute__ ((section(".sdram"))) buf[8192];
    static size_t len = 0;
    if (f_active_iface != CONF_IFACE_INVALID)
        {
        if (!len)
            len = iface_recv(buf, sizeof(buf));
        if (len)
            if (iface_send(buf, len, 0)) len = 0;
        iface_worker();
        }
#else
    if (f_state != ST_CLOSED){ /* Initialized state implies that iface_...() are not NULL */
        if (ST_RUNNING == f_state) { /* рабочий режим, обмен данными с модулем */
            uint8_t module_mask = g_cpld_module_mask;
            if (module_mask != f_last_sent_module_mask) {
                if (iface_send((const uint8_t*)f_module_update_signal, sizeof(f_module_update_signal), 0)) {
                    f_last_sent_module_mask = module_mask;
                }
            }
            f_host_read();
            f_host_write();
        } else if (ST_SEND_FPGA_INFO == f_state) { /* начальная посылка FPGA INFO */
            if (iface_send((const uint8_t*)g_ctlfunc_fpga_info, sizeof(g_ctlfunc_fpga_info), 1)) {
                f_state = ST_RUNNING;
            }
        }
        iface_worker();
    }
#endif
}
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
