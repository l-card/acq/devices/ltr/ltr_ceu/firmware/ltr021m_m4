@echo off
set HEX_IN=build\ltr021m-noboot.hex

set OCDPATH=c:\openocd-0.7.0
set NCSCRIPT=nc.tmp
set OCDPORT=14444
set OCDCFG="telnet_port %OCDPORT%"
set OCDIFACE=olimex-arm-usb-ocd
rem set OCDIFACE=jlink

if not exist %HEX_IN% goto nofile

start "openocd" %OCDPATH%\bin\openocd -c %OCDCFG% -s %OCDPATH% -f interface/%OCDIFACE%.cfg -f lpc43xx_flash.cfg

rem start "openocd" ocd.bat

echo. > %NCSCRIPT%
echo init >> %NCSCRIPT%
echo reset init >> %NCSCRIPT%
echo sleep 500 >> %NCSCRIPT%
echo halt >> %NCSCRIPT%
echo wait_halt 100 >> %NCSCRIPT%
echo flash banks >> %NCSCRIPT%
    rem echo flash info 0 >> %NCSCRIPT%
    rem echo flash erase_sector 0 0 last >> %NCSCRIPT%
echo flash write_image erase {%HEX_IN%} 0 ihex >> %NCSCRIPT%
    rem echo dump_image test.bin 0x1A000000 0x100 >> %NCSCRIPT%
    rem echo reset run >> %NCSCRIPT%
echo core_reset >> %NCSCRIPT%
echo sleep 10 >> %NCSCRIPT%
echo shutdown >> %NCSCRIPT%

nc localhost %OCDPORT% < %NCSCRIPT%
del %NCSCRIPT%

goto end

:nofile
echo FIle %HEX_IN% not found
:end
