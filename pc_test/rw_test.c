#include <windows.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <winioctl.h>

/*================================================================================================*/
#define DIOC_SEND_COMMAND   CTL_CODE(FILE_DEVICE_UNKNOWN, 15, METHOD_OUT_DIRECT, FILE_ANY_ACCESS)

#define TIMEOUT_MS      4000
#define BENCH_TIME      5000
#define BUF_LEN         65536

#define DATA_WORD(i)    (0x8080 | ((i) << 16))
//#define DATA_WORD(i)    0x55AA8080

typedef DWORD data_t;

enum en_usb_ioctl_vendor_request
    {
    USB_CTL_O_RESET_FPGA        = 0,
    USB_CTL_O_PUT_ARRAY         = 1,
    USB_CTL_I_GET_ARRAY         = 2,
    USB_CTL_O_INIT_FPGA         = 3,
    USB_CTL_O_INIT_DMA          = 4,
    USB_CTL_I_GET_MODULE_NAME   = 11,
    USB_CTL_O_CALL_APPLICATION  = 15
    };

typedef struct
    {
    WORD in, req, value, index;
    }
    ioctl_req_t;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
HANDLE open_device(void)
    {
    char devname[64];
    HANDLE h = INVALID_HANDLE_VALUE;
    int slot;

    for (slot = 0; (INVALID_HANDLE_VALUE == h) && (slot < 127); slot++)
        {
        snprintf(devname, sizeof(devname)/sizeof(devname[0]) - 1, "\\\\.\\LDev%u", slot);
        devname[sizeof(devname)/sizeof(devname[0]) - 1] = 0;
        h = CreateFile(devname, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING,
            FILE_FLAG_OVERLAPPED, NULL);
        }
    return h;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t do_ioctl_request(HANDLE hdev, ioctl_req_t* prq, void* buf, size_t len)
    {
    DWORD nbytes = 0;
    if (!DeviceIoControl(hdev, DIOC_SEND_COMMAND, prq, sizeof(*prq), buf, len, &nbytes, NULL))
        return 0;
    return nbytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t do_read(HANDLE hdev, void* buf, size_t len)
    {
    OVERLAPPED ov;
    DWORD nbytes = 0;
    ZeroMemory(&ov, sizeof(ov));
    ov.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (ReadFile(hdev, buf, len, NULL, &ov) || (ERROR_IO_PENDING == GetLastError()))
        {
        if (WAIT_OBJECT_0 != WaitForSingleObject(ov.hEvent, 5000))
            CancelIo(hdev);
        GetOverlappedResult(hdev, &ov, &nbytes, TRUE);
        }
    CloseHandle(ov.hEvent);
    return nbytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int reset_fpga(HANDLE hdev)
    {
    ioctl_req_t rq =
        { .in = 0, .req = USB_CTL_O_RESET_FPGA,
          .value = 0, .index = 0 };
    WORD dummy[255];
    DWORD mod_signal[2];

    do_ioctl_request(hdev, &rq, NULL, 0);
#if 1 // support module
    if (do_read(hdev, dummy, sizeof(dummy)) != sizeof(dummy)) return 0;
    if (do_read(hdev, mod_signal, sizeof(mod_signal)) != sizeof(mod_signal)) return 0;
    printf("First 2 words = %08lX %08lX\n", mod_signal[0], mod_signal[1]);
    return (0xFFFFFFFF == mod_signal[0]) && (0xFFFFAA01 == mod_signal[1]);
#else
    return 1;
#endif
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int get_array(HANDLE hdev, DWORD addr, void* buf, size_t len)
    {
    ioctl_req_t rq =
        { .in = 1, .req = USB_CTL_I_GET_ARRAY,
          .value = (WORD)(addr & 0xFFFF), .index = (WORD)(addr >> 16) };
    return (do_ioctl_request(hdev, &rq, buf, len) == len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int put_array(HANDLE hdev, DWORD addr, void* buf, size_t len)
    {
    ioctl_req_t rq =
        { .in = 0, .req = USB_CTL_O_PUT_ARRAY,
          .value = (WORD)(addr & 0xFFFF), .index = (WORD)(addr >> 16) };
    return (do_ioctl_request(hdev, &rq, buf, len) == len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int check_data(const data_t* buf, size_t nbytes, uint64_t start_pos)
    {
    int result = 1;
    size_t nwords, i, ofs = (size_t)(start_pos % BUF_LEN);
    if (nbytes % sizeof(data_t))
        {
        printf("\rData size not word-aligned (%u)\n", nbytes);
        return 0;
        }
    nwords = nbytes / sizeof(data_t);
    for (i = 0; i < nwords; i++)
        {
        data_t x = *buf++;
        if (x != (data_t)DATA_WORD(ofs))
            {
            printf("\rData mismatch: %08lX, expected %08lX at %"PRIu64"\n",
                x, (data_t)DATA_WORD(ofs), start_pos + i);
            result = 0;
            //continue;
            }
        ofs = (ofs + 1) % BUF_LEN;
        }
    return result;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int main(int argc, char* argv[])
    {
    HANDLE hdev;
    OVERLAPPED ov_read[2], ov_write[2];
    data_t rbuf[2][BUF_LEN], wbuf[2][BUF_LEN];
    int ridx, widx;
    uint64_t total_sent, total_recv;
    uint32_t bench_sent, bench_recv;
    DWORD start_time;
    int do_test = 1;

    if ((argc > 1) && !stricmp(argv[1], "-n"))
        do_test = 0;

    hdev = open_device();
    if (INVALID_HANDLE_VALUE == hdev) return puts("Cannot open device"), 1;

    /* init fpga */
    if (!reset_fpga(hdev))
        return puts("Cannot reset FPGA");

    /* set high speed */
    uint16_t speed_mask = 0x0001;
    puts("Setting speed");
    put_array(hdev, 0x82000000 + 0x8006, &speed_mask, sizeof(speed_mask));
    
    /* enable ring mode */
    uint16_t ring_on = 0x0001;
    puts("Enabling ring mode");
    put_array(hdev, 0x86000000 + 0x0001, &ring_on, sizeof(ring_on));

    puts("Testing...");

    for (widx = 0; widx < 2; widx++)
        {
        unsigned int i;
        for (i = 0; i < BUF_LEN; i++)
            {
            wbuf[widx][i] = DATA_WORD(i);
            }
        }

    ZeroMemory(&ov_write, sizeof(ov_write));
    ov_write[0].hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    ov_write[1].hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    ZeroMemory(&ov_read, sizeof(ov_read));
    ov_read[0].hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    ov_read[1].hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    total_sent = bench_sent = 0;
    total_recv = bench_recv = 0;
    start_time = GetTickCount();

    ReadFile(hdev, rbuf[0], sizeof(rbuf[0]), NULL, &ov_read[0]);
    ReadFile(hdev, rbuf[1], sizeof(rbuf[1]), NULL, &ov_read[1]);
    ridx = 0;

    WriteFile(hdev, wbuf[0], sizeof(wbuf[0]), NULL, &ov_write[0]);
    WriteFile(hdev, wbuf[1], sizeof(wbuf[1]), NULL, &ov_write[1]);
    widx = 0;

    while (1)
        {
        HANDLE elist[2];
        DWORD nbytes, now, t;

        elist[0] = ov_read[ridx].hEvent;
        elist[1] = ov_write[widx].hEvent;

        switch (WaitForMultipleObjects(2, elist, FALSE, INFINITE))
            {
            case WAIT_OBJECT_0: /* read */
                if (GetOverlappedResult(hdev, &ov_read[ridx], &nbytes, TRUE))
                    {
                    if (do_test)
                        check_data(rbuf[ridx], nbytes, total_recv / sizeof(data_t));
                    total_recv += nbytes;
                    bench_recv += nbytes;
                    }
                else
                    {
                    puts("\nread failed");
                    return 1;
                    }
                ReadFile(hdev, rbuf[ridx], sizeof(rbuf[ridx]), NULL, &ov_read[ridx]);
                ridx ^= 1;
                break;

            case WAIT_OBJECT_0 + 1: /* write */
                if (GetOverlappedResult(hdev, &ov_write[widx], &nbytes, TRUE))
                    {
                    total_sent += nbytes;
        			bench_sent += nbytes;
                    }
                else
                    {
                    puts("\nwrite failed");
                    return 1;
                    }
                WriteFile(hdev, wbuf[widx], sizeof(wbuf[widx]), NULL, &ov_write[widx]);
                widx ^= 1;
                break;
            }

        now = GetTickCount();
        t = now - start_time;
        if (t >= BENCH_TIME)
            {
            printf("\rTX: %"PRIu64" (%u b/s)\tRX: %"PRIu64" (%u b/s)\t",
                total_sent, (t) ? (unsigned int)(1000.0 * bench_sent / t) : 0,
                total_recv, (t) ? (unsigned int)(1000.0 * bench_recv / t) : 0);
            start_time = now;
            bench_recv = bench_sent = 0;
            }
        }

    CloseHandle(hdev);
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
