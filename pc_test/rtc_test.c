#include <windows.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <winioctl.h>
#include <getopt.h>
#include <stdint.h>

/*================================================================================================*/
#define DIOC_SEND_COMMAND   CTL_CODE(FILE_DEVICE_UNKNOWN, 15, METHOD_OUT_DIRECT, FILE_ANY_ACCESS)
#define RTC_CALIB_UNCHANGED INT32_MIN   /* �������� rtc_time_t.calib, ���������� "�� ������" */

enum en_usb_ioctl_vendor_request
    {
    USB_CTL_O_RESET_FPGA        = 0,
    USB_CTL_O_PUT_ARRAY         = 1,
    USB_CTL_I_GET_ARRAY         = 2,
    USB_CTL_O_INIT_FPGA         = 3,
    USB_CTL_O_INIT_DMA          = 4,
    USB_CTL_I_GET_MODULE_NAME   = 11,
    USB_CTL_O_CALL_APPLICATION  = 15
    };

#define RTC_ADDR_PREP       0x90004000
#define RTC_ADDR            0x90004002
#define RTC_PREP_CMD        0x2359

typedef struct
    {
    WORD in, req, value, index;
    }
    ioctl_req_t;

const char* const dow_str[] =
    {
    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };

typedef struct __attribute__ ((packed))
    {
    uint16_t year;      /* ��� (4-�������) */
    uint8_t month;      /* �����, 1..12 */
    uint8_t dow;        /* ���� ������ (0 = ��, 6 = ��) */
    uint8_t day;        /* ���� ������, 1..31 */
    uint8_t hour;       /* ���, 0..23 */
    uint8_t minute;     /* ������, 0..59 */
    uint8_t second;     /* �������, 0..59 */
    int32_t calib;
    }
    rtc_time_t;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
int open_device(HANDLE* ph)
    {
    char devname[64];
    int slot;

    if (!ph) return 0;
    *ph = INVALID_HANDLE_VALUE;

    for (slot = 0; (INVALID_HANDLE_VALUE == *ph) && (slot < 127); slot++)
        {
        snprintf(devname, sizeof(devname)/sizeof(devname[0]) - 1, "\\\\.\\LDev%u", slot);
        devname[sizeof(devname)/sizeof(devname[0]) - 1] = 0;
        *ph = CreateFile(devname, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING,
            FILE_FLAG_OVERLAPPED, NULL);
        }
    if (INVALID_HANDLE_VALUE == ph)
        {
        fputs("Error: cannot open device\n", stderr);
        return 0;
        }
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t do_ioctl_request(HANDLE hdev, ioctl_req_t* prq, void* buf, size_t len)
    {
    DWORD nbytes = 0;
    DWORD t = GetTickCount();
    if (!DeviceIoControl(hdev, DIOC_SEND_COMMAND, prq, sizeof(*prq), buf, len, &nbytes, NULL))
        return 0;
    t = GetTickCount() - t;
    if (t > 50)
        printf("Warning: IOCTL time = %lu ms\n", t);
    return nbytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int get_array(HANDLE hdev, DWORD addr, void* buf, size_t len)
    {
    ioctl_req_t rq =
        { .in = 1, .req = USB_CTL_I_GET_ARRAY,
          .value = (WORD)(addr & 0xFFFF), .index = (WORD)(addr >> 16) };
    return (do_ioctl_request(hdev, &rq, buf, len) == len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int put_array(HANDLE hdev, DWORD addr, const void* buf, size_t len)
    {
    ioctl_req_t rq =
        { .in = 0, .req = USB_CTL_O_PUT_ARRAY,
          .value = (WORD)(addr & 0xFFFF), .index = (WORD)(addr >> 16) };
    return (do_ioctl_request(hdev, &rq, (void*)buf, len) == len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void sys_time_to_rtc(rtc_time_t* pt, const SYSTEMTIME* pst)
    {
    pt->year = (uint16_t)pst->wYear;
    pt->month = (uint8_t)pst->wMonth;
    pt->dow = (uint8_t)pst->wDayOfWeek;
    pt->day = (uint8_t)pst->wDay;
    pt->hour = (uint8_t)pst->wHour;
    pt->minute = (uint8_t)pst->wMinute;
    pt->second = (uint8_t)pst->wSecond;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void rtc_to_sys_time(SYSTEMTIME* pst, const rtc_time_t* pt)
    {
    pst->wYear = pt->year;
    pst->wMonth = pt->month;
    pst->wDayOfWeek = pt->dow;
    pst->wDay = pt->day;
    pst->wHour = pt->hour;
    pst->wMinute = pt->minute;
    pst->wSecond = pt->second;
    pst->wMilliseconds = 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int time_diff(const SYSTEMTIME* a, const SYSTEMTIME* b)
    {
    union { int64_t ll; FILETIME ft; } ta, tb;
    SystemTimeToFileTime(a, &ta.ft);
    SystemTimeToFileTime(b, &tb.ft);
    return (int)((ta.ll - tb.ll) / 10000LL);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void time_add_ms(SYSTEMTIME* pst, int ms)
    {
    union { int64_t ll; FILETIME ft; } t;
    SystemTimeToFileTime(pst, &t.ft);
    t.ll += ms * 10000LL;
    FileTimeToSystemTime(&t.ft, pst);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
const char* rtc_time_str(const rtc_time_t* pt)
    {
    static char str_buf[32];
    snprintf(str_buf, sizeof(str_buf), "%s %02d.%02d.%04d %02d:%02d:%02d", 
        (pt->dow < 7) ? dow_str[pt->dow] : "???",
        pt->day, pt->month, pt->year,
        pt->hour, pt->minute, pt->second);
    return str_buf;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
const char* sys_time_str(const SYSTEMTIME* pst)
    {
    static char str_buf[48];
    snprintf(str_buf, sizeof(str_buf), "%s %02d.%02d.%04d %02d:%02d:%02d.%03d", 
        (pst->wDayOfWeek < 7) ? dow_str[pst->wDayOfWeek] : "???",
        pst->wDay, pst->wMonth, pst->wYear,
        pst->wHour, pst->wMinute, pst->wSecond, pst->wMilliseconds);
    return str_buf;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int main(int argc, char* argv[])
    {
    HANDLE hdev;
    int opt;
    int do_get = 0, do_set = 0;
    int new_calib = RTC_CALIB_UNCHANGED;

    const char help[] = "Usage: rtc_test options...\n"
        "-r      Read RTC time and calibration register\n"
        "-R      Like -r, but wait for SECONDS to change and compare against PC clock\n"
        "-w      Set RTC time to current time by computer's clock\n"
        "-W      Like -w, but wait for SECONDS to change and set at that point\n"
        "-c n    Set RTC calibration (use together with -w)\n"
        "        n = [-131072..-2] : slow down RTC by k = 1 + 1/n\n"
        "        n = [2..131072]   : speed up RTC by k = 1 + 1/n\n"
        "        n n [-1..1]       : no adjustment\n";

    if (argc < 2) return puts(help), 0;

    while (0 < (opt = getopt(argc, argv, "rRwWc:h")))
    switch (opt)
        {
        case 'r':
            do_get = 1;
            break;
        case 'R':
            do_get = 2;
            break;
        case 'w':
            do_set = 1;
            break;
        case 'W':
            do_set = 2;
            break;
        case 'c':
            {
            char* peol;
            new_calib = strtol(optarg, &peol, 0);
            if (*peol)
                {
                new_calib = RTC_CALIB_UNCHANGED;
                fprintf(stderr, "Invalid calibration value %s\n", optarg);
                }
            }
            break;
        case 'h':
            puts(help);
            break;
        }

    if (!do_get && !do_set)
        return 0;

    if (!open_device(&hdev))
        return 1;

    if (do_get)
        {
        rtc_time_t rtc;
        SYSTEMTIME st;
        if (!get_array(hdev, RTC_ADDR, &rtc, sizeof(rtc)))
            return fputs("Error: cannot read RTC\n", stderr), 2;
        if (2 == do_get)
            {
            uint8_t last_sec = rtc.second;
            DWORD t = GetTickCount();
            do
                {
                if (GetTickCount() - t > 4000)
                    return fputs("Error: timed out (RTC is stopped?)\n", stderr), 4;
                if (!get_array(hdev, RTC_ADDR, &rtc, sizeof(rtc)))
                    return fputs("Error: cannot read RTC\n", stderr), 2;
                }
            while (rtc.second == last_sec);
            }
        GetLocalTime(&st);
        printf("RTC time is %s\n", rtc_time_str(&rtc));
        printf("PC time is  %s\n", sys_time_str(&st));
        if (2 == do_get)
            {
            SYSTEMTIME t;
            rtc_to_sys_time(&t, &rtc);
            printf("Error = ~ %d ms\n", time_diff(&t, &st));
            }
        printf("RTC calibration value = %d\n", rtc.calib);
        }

    if (do_set)
        {
        rtc_time_t rtc;
        SYSTEMTIME st;
        uint16_t prep_cmd = RTC_PREP_CMD;
        uint8_t dummy = 0;

        if (!put_array(hdev, RTC_ADDR_PREP, &prep_cmd, sizeof(prep_cmd)))
            return fputs("Error: cannot set RTC (phase 1)\n", stderr), 3;

        DWORD t = GetTickCount();
        do
            {
            if (GetTickCount() - t > 4000)
                return fputs("Error: timed out while waiting for RTC ready\n", stderr), 4;
            if (!get_array(hdev, RTC_ADDR_PREP, &dummy, sizeof(dummy)))
                return fputs("Error: cannot read RTC set-ready state\n", stderr), 4;
            }
        while (!dummy);

        do { GetLocalTime(&st); } while ((2 == do_set) && (st.wMilliseconds >= 100));
        time_add_ms(&st, 1000); /* �������� 1� */
        sys_time_to_rtc(&rtc, &st);
        rtc.calib = new_calib;
        printf("Setting RTC time to %s\n", rtc_time_str(&rtc));
        if (!put_array(hdev, RTC_ADDR, &rtc, sizeof(rtc)))
            return fputs("Error: cannot set RTC (phase 2)\n", stderr), 5;
        }

    CloseHandle(hdev);
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
