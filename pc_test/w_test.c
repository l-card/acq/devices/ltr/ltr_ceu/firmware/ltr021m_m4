#include <windows.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

/*================================================================================================*/
#define TIMEOUT_MS      4000
#define BENCH_TIME      2000
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
HANDLE open_device(void)
    {
    char devname[64];
    HANDLE h = INVALID_HANDLE_VALUE;
    int slot;

    for (slot = 0; (INVALID_HANDLE_VALUE == h) && (slot < 127); slot++)
        {
        snprintf(devname, sizeof(devname)/sizeof(devname[0]) - 1, "\\\\.\\LDev%u", slot);
        devname[sizeof(devname)/sizeof(devname[0]) - 1] = 0;
        h = CreateFile(devname, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING,
            FILE_FLAG_OVERLAPPED, NULL);
        }
    return h;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int main(void)
    {
    HANDLE hdev;
    OVERLAPPED ov_write[2];
    typedef DWORD data_t;
    data_t wbuf[2][65536];
    int widx;
    uint64_t total_sent;
    uint32_t bench_sent;
    DWORD start_time;

    hdev = open_device();
    if (INVALID_HANDLE_VALUE == hdev) return puts("Cannot open device"), 1;

    for (widx = 0; widx < 2; widx++)
        {
        unsigned int i;
        for (i = 0; i < sizeof(wbuf[widx]) / sizeof(wbuf[widx][0]); i++)
            {
            wbuf[widx][i] = 0x01018080; /* LTR RESET, slot 0 */
            }
        }

    ZeroMemory(&ov_write, sizeof(ov_write));
    ov_write[0].hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    ov_write[1].hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    total_sent = bench_sent = 0;
    start_time = GetTickCount();

    WriteFile(hdev, wbuf[0], sizeof(wbuf[0]), NULL, &ov_write[0]);
    widx = 1;

    while (1)
        {
        DWORD nbytes, now, t;

        WriteFile(hdev, wbuf[widx], sizeof(wbuf[widx]), NULL, &ov_write[widx]);
        widx ^= 1;

        if (WaitForSingleObject(ov_write[widx].hEvent, INFINITE) != WAIT_OBJECT_0)
            break;

        if (GetOverlappedResult(hdev, &ov_write[widx], &nbytes, TRUE))
            {
            total_sent += nbytes;
			bench_sent += nbytes;
            }
        else
            {
            puts("write failed");
            }

        now = GetTickCount();
        t = now - start_time;
        if (t >= BENCH_TIME)
            {
            printf("\rTX: %"PRIu64" (%u b/s)",
                total_sent, (t) ? (unsigned int)(1000.0 * bench_sent / t) : 0);
            start_time = now;
            bench_sent = 0;
            }

        }

    CloseHandle(hdev);
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
