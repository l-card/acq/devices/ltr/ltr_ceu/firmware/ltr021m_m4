#include <windows.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <winioctl.h>

/*================================================================================================*/
#define USE_OVERLAPPED          0
#define DATA_DIAG_TRACE_LEN     21

#define DIOC_SEND_COMMAND   CTL_CODE(FILE_DEVICE_UNKNOWN, 15, METHOD_OUT_DIRECT, FILE_ANY_ACCESS)

enum en_usb_ioctl_vendor_request
    {
    USB_CTL_O_RESET_FPGA        = 0,
    USB_CTL_O_PUT_ARRAY         = 1,
    USB_CTL_I_GET_ARRAY         = 2,
    USB_CTL_O_INIT_FPGA         = 3,
    USB_CTL_O_INIT_DMA          = 4,
    USB_CTL_I_GET_MODULE_NAME   = 11,
    USB_CTL_O_CALL_APPLICATION  = 15
    };

typedef struct
    {
    WORD in, req, value, index;
    }
    ioctl_req_t;

typedef struct
    {
    uint32_t in_word_count;         /* ���������� �������� � ������������ ���� */
    uint32_t in_word_count_irq;     /* � �.�. ���������� ����, ������������ � ���������� */
    uint32_t in_ovf_count;          /* ������� ������������ ��������� */
    uint32_t out_word_count;        /* ���������� ��������� � ������ ���� */
    uint32_t max_in_words_bgnd;     /* ���������� ������ ������, ������������ �� ���� ����� worker */
    uint32_t max_in_words_irq;      /* ���������� ������ ������, ������������ � ����� ���������� */
    }
    data_diag_t;

#if DATA_DIAG_TRACE_LEN > 0
typedef struct __attribute__ ((packed))
    {
    uint16_t in_pos;
    uint16_t out_pos;
    uint16_t in[DATA_DIAG_TRACE_LEN];
    uint16_t out[DATA_DIAG_TRACE_LEN];
    }
    data_diag_trace_t;
#endif
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
HANDLE open_device(void)
    {
    char devname[64];
    HANDLE h = INVALID_HANDLE_VALUE;
    int slot;

    for (slot = 0; (INVALID_HANDLE_VALUE == h) && (slot < 127); slot++)
        {
        snprintf(devname, sizeof(devname)/sizeof(devname[0]) - 1, "\\\\.\\LDev%u", slot);
        devname[sizeof(devname)/sizeof(devname[0]) - 1] = 0;
        h = CreateFile(devname, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING,
            FILE_FLAG_OVERLAPPED, NULL);
        }
    return h;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t do_ioctl_request(HANDLE hdev, ioctl_req_t* prq, void* buf, size_t len)
    {
    DWORD nbytes = 0;
#if !USE_OVERLAPPED
    if (!DeviceIoControl(hdev, DIOC_SEND_COMMAND, prq, sizeof(*prq), buf, len, &nbytes, NULL))
        return 0;
#else
    OVERLAPPED ov;
    ZeroMemory(&ov, sizeof(ov));
    ov.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (!DeviceIoControl(hdev, DIOC_SEND_COMMAND, prq, sizeof(*prq), buf, len, &nbytes, &ov))
        {
        if (ERROR_IO_PENDING != GetLastError()) return 0;
        if (WAIT_OBJECT_0 != WaitForSingleObject(ov.hEvent, 4000))
            CancelIo(hdev);
        if (!GetOverlappedResult(hdev, &ov, &nbytes, TRUE)) return 0;
        }
#endif
    return nbytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int get_module_name(HANDLE hdev, void* buf, size_t len)
    {
    ioctl_req_t rq =
        { .in = 1, .req = USB_CTL_I_GET_MODULE_NAME,
          .value = 0, .index = 0 };
    return (do_ioctl_request(hdev, &rq, buf, len) == len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int get_array(HANDLE hdev, DWORD addr, void* buf, size_t len)
    {
    ioctl_req_t rq =
        { .in = 1, .req = USB_CTL_I_GET_ARRAY,
          .value = (WORD)(addr & 0xFFFF), .index = (WORD)(addr >> 16) };
    return (do_ioctl_request(hdev, &rq, buf, len) == len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int main(void)
    {
    HANDLE hdev;
    data_diag_t dd;
    char name[16];

    hdev = open_device();
    if (INVALID_HANDLE_VALUE == hdev) return puts("Cannot open device"), 1;

    if (get_module_name(hdev, name, sizeof(name)))
        {
        printf("module name = %.16s\n", name);
        }
    else
        {
        puts("getmodulename failed");
        }
    
    if (get_array(hdev, 0x90004200, &dd, sizeof(dd)))
        {
        printf("in_word_count      = %u\n", dd.in_word_count);
        printf("in_word_count_irq  = %u\n", dd.in_word_count_irq);
        printf("in_ovf_count       = %u\n", dd.in_ovf_count);
        printf("out_word_count     = %u\n", dd.out_word_count);
        printf("max_in_words_bgnd  = %u\n", dd.max_in_words_bgnd);
        printf("max_in_words_irq   = %u\n", dd.max_in_words_irq);
        }
    else
        {
        puts("getarray failed.");
        }
    
#if DATA_DIAG_TRACE_LEN > 0
    data_diag_trace_t tr;
    if (get_array(hdev, 0x90004300, &tr, sizeof(tr)))
        {
        unsigned int i;
        puts("OUT     IN");
        for (i = 0; i < DATA_DIAG_TRACE_LEN; i++)
            {
            printf("%04X%c   %04X%c\n",
                tr.out[i], (tr.out_pos == i) ? '<' : ' ',
                tr.in[i], (tr.in_pos == i) ? '<' : ' ');
            }
        }
    else
        {
        puts("getarray failed for trace.");
        }
#endif

    CloseHandle(hdev);
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
