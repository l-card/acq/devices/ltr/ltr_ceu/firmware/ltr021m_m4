#include <windows.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

/*================================================================================================*/
#define TIMEOUT_MS      4000
#define BENCH_TIME      5000
#define BUF_LEN         16384

#define DATA_NWORDS     3
typedef uint16_t data_t;

const data_t test_data[DATA_NWORDS] = { 0x180, 0x55, 0xAA };
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
HANDLE open_device(void)
    {
    char devname[64];
    HANDLE h = INVALID_HANDLE_VALUE;
    int slot;

    for (slot = 0; (INVALID_HANDLE_VALUE == h) && (slot < 127); slot++)
        {
        snprintf(devname, sizeof(devname)/sizeof(devname[0]) - 1, "\\\\.\\LDev%u", slot);
        devname[sizeof(devname)/sizeof(devname[0]) - 1] = 0;
        h = CreateFile(devname, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING,
            FILE_FLAG_OVERLAPPED, NULL);
        }
    return h;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int check_data(const data_t* buf, size_t nbytes, uint64_t start_pos)
    {
    int result = 1;
    size_t nwords, i, ofs = (size_t)(start_pos % DATA_NWORDS);
    if (nbytes % sizeof(data_t))
        {
        printf("\rData size not word-aligned (%u)\n", nbytes);
        return 0;
        }
    nwords = nbytes / sizeof(data_t);
    for (i = 0; i < nwords; i++)
        {
        data_t x = *buf++;
        if (x != test_data[ofs])
            {
            printf("\rData mismatch: %04X, expected %04X at %"PRIu64"\n",
                x, test_data[ofs], start_pos + i);
            result = 0;
            //continue;
            }
        ofs = (ofs + 1) % DATA_NWORDS;
        }
    return result;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int main(int argc, char* argv[])
    {
    HANDLE hdev;
    OVERLAPPED ov_read[2], ov_write[2];
    data_t rbuf[2][DATA_NWORDS * BUF_LEN], wbuf[2][DATA_NWORDS * BUF_LEN];
    int ridx, widx;
    uint64_t total_sent, total_recv;
    uint32_t bench_sent, bench_recv;
    DWORD start_time;
    int do_test = 1;

    if ((argc > 1) && !stricmp(argv[1], "-n"))
        do_test = 0;

    hdev = open_device();
    if (INVALID_HANDLE_VALUE == hdev) return puts("Cannot open device"), 1;

    for (widx = 0; widx < 2; widx++)
        {
        unsigned int i;
        for (i = 0; i < BUF_LEN; i++)
            {
            memcpy(&wbuf[widx][i * DATA_NWORDS], test_data, sizeof(data_t) * DATA_NWORDS);
            }
        }

    ZeroMemory(&ov_write, sizeof(ov_write));
    ov_write[0].hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    ov_write[1].hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    ZeroMemory(&ov_read, sizeof(ov_read));
    ov_read[0].hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    ov_read[1].hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    total_sent = bench_sent = 0;
    total_recv = bench_recv = 0;
    start_time = GetTickCount();

    ReadFile(hdev, rbuf[0], sizeof(rbuf[0]), NULL, &ov_read[0]);
    ReadFile(hdev, rbuf[1], sizeof(rbuf[1]), NULL, &ov_read[1]);
    ridx = 0;

    WriteFile(hdev, wbuf[0], sizeof(wbuf[0]), NULL, &ov_write[0]);
    WriteFile(hdev, wbuf[1], sizeof(wbuf[1]), NULL, &ov_write[1]);
    widx = 0;

    while (1)
        {
        HANDLE elist[2];
        DWORD nbytes, now, t;

        elist[0] = ov_read[ridx].hEvent;
        elist[1] = ov_write[widx].hEvent;

        switch (WaitForMultipleObjects(2, elist, FALSE, INFINITE))
            {
            case WAIT_OBJECT_0: /* read */
                if (GetOverlappedResult(hdev, &ov_read[ridx], &nbytes, TRUE))
                    {
                    if (do_test)
                        check_data(rbuf[ridx], nbytes, total_recv / sizeof(data_t));
                    total_recv += nbytes;
                    bench_recv += nbytes;
                    }
                else
                    {
                    puts("\nread failed");
                    return 1;
                    }
                ReadFile(hdev, rbuf[ridx], sizeof(rbuf[ridx]), NULL, &ov_read[ridx]);
                ridx ^= 1;
                break;

            case WAIT_OBJECT_0 + 1: /* write */
                if (GetOverlappedResult(hdev, &ov_write[widx], &nbytes, TRUE))
                    {
                    total_sent += nbytes;
        			bench_sent += nbytes;
                    }
                else
                    {
                    puts("\nwrite failed");
                    return 1;
                    }
                WriteFile(hdev, wbuf[widx], sizeof(wbuf[widx]), NULL, &ov_write[widx]);
                widx ^= 1;
                break;
            }

        now = GetTickCount();
        t = now - start_time;
        if (t >= BENCH_TIME)
            {
            printf("\rTX: %"PRIu64" (%u b/s)\tRX: %"PRIu64" (%u b/s)\t",
                total_sent, (t) ? (unsigned int)(1000.0 * bench_sent / t) : 0,
                total_recv, (t) ? (unsigned int)(1000.0 * bench_recv / t) : 0);
            start_time = now;
            bench_recv = bench_sent = 0;
            }
        }

    CloseHandle(hdev);
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
