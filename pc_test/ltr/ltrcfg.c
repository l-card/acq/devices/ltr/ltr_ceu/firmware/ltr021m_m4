#include <stdio.h>
#include <windows.h>
#include <getopt.h>
#include "ltrapi.h"

/*================================================================================================*/
typedef struct { const char* text; int val; } vallist_t;

const vallist_t vals_user[] =
    {
    { "digin1", LTR_USERIO_DIGIN1 },
    { "digin2", LTR_USERIO_DIGIN2 },
    { "digout", LTR_USERIO_DIGOUT },
    { NULL, 0 }
    };

const vallist_t vals_digout[] =
    {
    { "const0", LTR_DIGOUT_CONST0 },
    { "const1", LTR_DIGOUT_CONST1 },
    { "user0",  LTR_DIGOUT_USERIO0 },
    { "user1",  LTR_DIGOUT_USERIO1 },
    { "digin1", LTR_DIGOUT_DIGIN1 },
    { "digin2", LTR_DIGOUT_DIGIN2 },
    { "start",  LTR_DIGOUT_START },
    { "second", LTR_DIGOUT_SECOND },
    { "irig",   LTR_DIGOUT_IRIG },
    { NULL, 0 }
    };

const vallist_t vals_digouten[] =
    {
    { "0", FALSE },
    { "1", TRUE },
    { NULL, 0 }
    };
    
const vallist_t vals_start[] =
    {
    { "off",            LTR_MARK_OFF },
    { "digin1_rise",    LTR_MARK_EXT_DIGIN1_RISE },
    { "digin1_fall",    LTR_MARK_EXT_DIGIN1_FALL },
    { "digin2_rise",    LTR_MARK_EXT_DIGIN2_RISE },
    { "digin2_fall",    LTR_MARK_EXT_DIGIN2_FALL },
    { "soft",           LTR_MARK_INTERNAL },
    { NULL, 0 }
    };

const vallist_t vals_second[] =
    {
    { "off",            LTR_MARK_OFF },
    { "digin1_rise",    LTR_MARK_EXT_DIGIN1_RISE },
    { "digin1_fall",    LTR_MARK_EXT_DIGIN1_FALL },
    { "digin2_rise",    LTR_MARK_EXT_DIGIN2_RISE },
    { "digin2_fall",    LTR_MARK_EXT_DIGIN2_FALL },
    { "pulse",          LTR_MARK_INTERNAL },
    { "soft",           6 },
    { NULL, 0 }
    };

enum { OP_USER0, OP_USER1, OP_USER2, OP_USER3, OP_DIGOUT1, OP_DIGOUT2, OP_DIGOUTEN,
       OP_START, OP_SECOND };
const struct option op_long[] =
    {
    { "user0",      required_argument, NULL, OP_USER0 },
    { "user1",      required_argument, NULL, OP_USER1 },
    { "user2",      required_argument, NULL, OP_USER2 },
    { "user3",      required_argument, NULL, OP_USER3 },
    { "digout1",    required_argument, NULL, OP_DIGOUT1 },
    { "digout2",    required_argument, NULL, OP_DIGOUT2 },
    { "digout_en",  required_argument, NULL, OP_DIGOUTEN },
    { "start",      required_argument, NULL, OP_START },
    { "second",     required_argument, NULL, OP_SECOND },
    { NULL, 0, NULL, 0 }
    };

const struct { int opt; const vallist_t* vals; } opt_list[] =
    {
    { OP_USER0, vals_user }, { OP_USER1, vals_user },
    { OP_USER2, vals_user }, { OP_USER3, vals_user },
    { OP_DIGOUT1, vals_digout }, { OP_DIGOUT2, vals_digout },
    { OP_DIGOUTEN, vals_digouten },
    { OP_START, vals_start }, { OP_SECOND, vals_second },
    { 0, NULL }
    };

TLTR_CONFIG conf =
    {
    .userio = { LTR_USERIO_DEFAULT, LTR_USERIO_DEFAULT, LTR_USERIO_DEFAULT, LTR_USERIO_DEFAULT },
    .digout = { LTR_DIGOUT_CONST0, LTR_DIGOUT_CONST0 },
    .digout_en = FALSE
    };

int start_reg_val, sec_reg_val;

int do_conf = 0, do_start = 0, do_sec = 0;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void list_vals(const char* opt_name, const vallist_t* lst)
    {
    fprintf(stderr, "-%s = { ", opt_name);
    for (; lst->text; lst++)
        {
        fprintf(stderr, "%s %s", lst->text, (lst[1].text) ? "| " : "}\n");
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int lookup_val(const vallist_t* lst, const char* str, int* out_val)
    {
    for (; lst->text; lst++)
        {
        if (!stricmp(str, lst->text)) { *out_val = lst->val; return 1; }
        }
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
const vallist_t* opt_vallist(int opt)
    {
    unsigned int i;
    for (i = 0; opt_list[i].vals; i++)
        {
        if (opt_list[i].opt == opt)
            return opt_list[i].vals;
        }
    return NULL;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void help(void)
    {
    unsigned int i;
    fprintf(stderr, "Options:\n");
    for (i = 0; op_long[i].name; i++)
        {
        const vallist_t* op_vals = opt_vallist(op_long[i].val);
        if (op_vals) list_vals(op_long[i].name, op_vals);
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int parse_params(int argc, char* argv[])
    {
    int opt, oidx;
    while (0 < (opt = getopt_long_only(argc, argv, "", op_long, &oidx)))
        {
        int val;
        const vallist_t* op_vals = opt_vallist(opt);
        if (op_vals)
            {
            if (!lookup_val(op_vals, optarg, &val))
                return list_vals(op_long[oidx].name, op_vals), 0;
            }
        else
            {
            opt = -1;
            }

        switch (opt)
            {
            case OP_USER0:
            case OP_USER1:
            case OP_USER2:
            case OP_USER3:
                conf.userio[opt - OP_USER0] = val;
                do_conf = 1;
                break;
            case OP_DIGOUT1:
            case OP_DIGOUT2:
                conf.digout[opt - OP_DIGOUT1] = val;
                do_conf = 1;
                break;
            case OP_DIGOUTEN:
                conf.digout_en = val;
                do_conf = 1;
                break;
            case OP_START:
                start_reg_val = val;
                do_start = 1;
                break;
            case OP_SECOND:
                sec_reg_val = val;
                do_sec = 1;
                break;
            default:
                help();
                return 0;
            }
        }
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int write_fpga_reg(TLTR* pltr, int addr, int val)
    {
    struct __attribute__((packed))
        { DWORD hdr[2]; DWORD addr; DWORD size; WORD val; }
        request =
        {
        .hdr = { CONTROL_COMMAND_START, CONTROL_COMMAND_PUT_ARRAY },
        .addr = SEL_FPGA_DATA + addr,
        .size = sizeof(WORD),
        .val = val
        };
    return LTR__GenericCtlFunc(pltr, &request, sizeof(request), NULL, 0, LTR_ERROR_EXECUTE, 0);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
    {
    TLTR ltr;
    int ltr_result;
    TCRATE_INFO ci;

    if (!parse_params(argc, argv))
        return 1;

    if (!(do_conf || do_start || do_sec))
        {
        help();
        return 0;
        }

    if ((ltr_result = LTR_Init(&ltr)) != LTR_OK)
        return fprintf(stderr, "Cannot init TTLR: %s\n", LTR_GetErrorString(ltr_result)), 2;

    /* ������������ � ������������ ������ ������ */
    printf("Connecting to LTR... ");
    ltr.cc = CC_CONTROL;
    if ((ltr_result = LTR_Open(&ltr)) != LTR_OK)
        {
        return printf("error: %s\n", LTR_GetErrorString(ltr_result)), 3;
        }
    puts("ok");

    /* ������� ���������� � ������ */
    printf("LTR serial number: %s\n", ltr.csn);
    ZeroMemory(&ci, sizeof(ci));
    if ((ltr_result = LTR_GetCrateInfo(&ltr, &ci)) != LTR_OK)
        {
        LTR_Close(&ltr);
        return printf("Cannot get LTR info: %s\n", LTR_GetErrorString(ltr_result)), 4;
        }

    printf("Crate type: ");
    switch (ci.CrateType)
        {
        case CRATE_TYPE_LTR010:     printf("LTR010"); break;
        case CRATE_TYPE_LTR021:     printf("LTR021"); break;
        case CRATE_TYPE_LTR030:     printf("LTR030"); break;
        case CRATE_TYPE_LTR031:     printf("LTR031"); break;
        case CRATE_TYPE_LTR_CU_1:   printf("LTR-CU-1"); break;
        case CRATE_TYPE_LTR_CEU_1:  printf("LTR-CEU-1"); break;
        default:                    printf("(unknown %d)", ci.CrateType); break;
        }
    puts("");

    printf("Interface: ");
    switch (ci.CrateInterface)
        {
        case CRATE_IFACE_USB:       printf("USB");    break;
        case CRATE_IFACE_TCPIP:     printf("TCP/IP"); break;
        default:                    printf("(unknown %d)", ci.CrateInterface); break;
        }
    puts("");
    
    if (do_conf)
        {
        printf("Writing LTR configuration... ");
        if ((ltr_result = LTR_Config(&ltr, &conf)) == LTR_OK)
            puts("Ok");
        else
            printf("error: %s\n", LTR_GetErrorString(ltr_result));
        }

    if (do_start)
        {
        printf("Setting up START marks... ");
        if ((ltr_result = write_fpga_reg(&ltr, 0x0E, start_reg_val)) == LTR_OK)
            puts("Ok");
        else
            printf("error: %s\n", LTR_GetErrorString(ltr_result));
        }

    if (do_sec)
        {
        printf("Setting up SECOND marks... ");
        if ((ltr_result = write_fpga_reg(&ltr, 0x0F, sec_reg_val)) == LTR_OK)
            puts("Ok");
        else
            printf("error: %s\n", LTR_GetErrorString(ltr_result));
        }

    LTR_Close(&ltr);
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
