@echo off
set INCOPT=-I \prj\ltr\include
set LIBS=ltrapi.lib ltr34api.lib
gcc -O2 -static-libgcc -W -Wall %INCOPT% -o %~n1.exe %1 %LIBS%
strip %~n1.exe
