#include <stdio.h>
#include <windows.h>
#include <getopt.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>
#include "ltrapi.h"
#include "ltr34api.h"

/*================================================================================================*/
#define LTR34_SLOT  1
#define F_CONST     2.0E6
#define FDIV_MIN    4
#define FDIV_MAX    64
#define SIGNAL_FREQ 100.0
#define SIGNAL_AMP  0x4000
#define BUF_LEN     32768
#define N_PRELOAD   (2000000 / BUF_LEN)
#define TIMEOUT     4000

#if N_PRELOAD < 1
#error "Must preload at least 1 buffer"
#endif

double wave[BUF_LEN];
DWORD buf[BUF_LEN], echo_buf[BUF_LEN], tmark[BUF_LEN];

volatile int exit_request = 0;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void help(void)
    {
    puts("Usage: ltr34loadtest samplerate_hz");
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL WINAPI ctrl_c(DWORD signal)
    {
    if (CTRL_C_EVENT == signal)
        {
        exit_request = 1;
        return TRUE;
        }
    return FALSE;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
    {
    TLTR34 ltr34;
    int ltr_result;
    double fs;
    int i, fdiv, nsamples;
    uint16_t tm_sec = 0;
    DWORD last_mark_ti = 0, first_mark_ti = 0, tidx = 0;
    uint64_t mark_count;

    if (argc < 2) return help(), 0;
    fs = strtod(argv[1], NULL);
    if (fs < 1) return help(), 0;

    fdiv = (unsigned int)(F_CONST / fs + 0.5);
    if ((fdiv < FDIV_MIN) || (fdiv > FDIV_MAX))
        {
        printf("Invalid frequency, must be %.0f .. %.0f\n",
            F_CONST / FDIV_MAX,
            F_CONST / FDIV_MIN);
        return 1;
        }

    printf("Fs = %.1f\n", F_CONST / fdiv);

    puts("Connecting to LTR... ");
    if ((ltr_result = LTR34_Init(&ltr34)) != LTR_OK)
        return fprintf(stderr, "Cannot init TTLR34 (%d)\n", ltr_result), 2;

    if ((ltr_result = LTR34_Open(&ltr34, SADDR_DEFAULT, SPORT_DEFAULT, "", LTR34_SLOT)) != LTR_OK)
        return fprintf(stderr, "Cannot open LTR34 (%d)\n", ltr_result), 2;

    puts("Resetting module...");
    if ((ltr_result = LTR34_Reset(&ltr34)) != LTR_OK)
        return fprintf(stderr, "Cannot reset LTR34 (%d)\n", ltr_result), 2;

    ltr34.FrequencyDivisor = 64 - fdiv;
    ltr34.ChannelQnt = 1;
    ltr34.UseClb = 1;               /* ������������ ��������� ���������� */
    ltr34.AcknowledgeType = 1;      /* echo */
    ltr34.ExternalStart = 0;
    ltr34.RingMode = 0;
    ltr34.ExternalStart = 0;          /* ����������� ���� */
    ltr34.LChTbl[0] = LTR34_CreateLChannel(1, 0);

    puts("Configuring DAC...");
    if ((ltr_result = LTR34_Config(&ltr34)) != LTR_OK)
        return fprintf(stderr, "Cannot configure LTR34 (%d)\n", ltr_result), 2;

    /* prepare data */
    puts("Preparing data...");
    nsamples = BUF_LEN - BUF_LEN % (unsigned int)(ltr34.FrequencyDAC / SIGNAL_FREQ);
    for (i = 0; i < nsamples; i++)
        {
        wave[i] = SIGNAL_AMP * sin(2 * M_PI * SIGNAL_FREQ / ltr34.FrequencyDAC * i);
        }
    if ((ltr_result = LTR34_ProcessData(&ltr34, wave, buf, nsamples, 0)) != LTR_OK)
        return fprintf(stderr, "ProcessData failed (%d)\n", ltr_result), 2;

    /* preload N buffers */
    puts("Preloading buffers...");
    for (i = 0; i < N_PRELOAD; i++)
        {
        if ((ltr_result = LTR34_Send(&ltr34, buf, nsamples, TIMEOUT)) != nsamples)
            return fprintf(stderr, "Send failed (%d)\n", ltr_result), 2;
        }

    mark_count = 0;

    SetConsoleCtrlHandler(ctrl_c, TRUE);

    puts("Starting generation, Ctrl-C to quit.");
    if ((ltr_result = LTR34_DACStart(&ltr34)) != LTR_OK)
        return fprintf(stderr, "Cannot start DAC (%d)\n", ltr_result), 2;

    /* generation loop */
    while (!exit_request)
        {
        int err_count = 0;
        if ((ltr_result = LTR34_Recv(&ltr34, echo_buf, tmark, nsamples, TIMEOUT)) != nsamples)
            {
            fprintf(stderr, "Recv failed (%d)\n", ltr_result);
            break;
            }
        if (exit_request) break;

        if (!mark_count)
            tm_sec = tmark[0] & 0xFFFF;

        for (i = 0; i < nsamples; i++)
            {
            uint16_t tm_sec_new = tmark[i] & 0xFFFF;
            tidx++; /* time index (sample number) */
            if ((echo_buf[i] ^ buf[i]) & 0xFFFF0000)
                {
                if (++err_count <= 8)
                    fprintf(stderr, "ERROR at %i: send = %08lX recv = %08lX\n",
                        i, buf[i], echo_buf[i]);
                }
            if (tm_sec_new != tm_sec)
                {
                if (mark_count++)
                    {
                    DWORD dt = tidx - last_mark_ti;
                    if (tm_sec_new != ((tm_sec + 1) & 0xFFFF))
                        fprintf(stderr, "More than 1 second mark at once\n");
                    if ((dt < fs * 0.9) || (dt > fs * 1.1))
                        fprintf(stderr, "Warning: time mark %"PRIu64" dt = %lu (1s = %.0f)\n",
                        mark_count, dt, fs);
                    }
                else
                    {
                    first_mark_ti = tidx;
                    }
                last_mark_ti = tidx;
                tm_sec = tm_sec_new;
                }
            }
        if (err_count)
            fprintf(stderr, "ERRORS: %d / %d\n", err_count, nsamples);

        if ((ltr_result = LTR34_Send(&ltr34, buf, nsamples, TIMEOUT)) != nsamples)
            {
            fprintf(stderr, "Send failed (%d)\n", ltr_result);
            break;
            }
        }

    puts("Stopping DAC...");
    if ((ltr_result = LTR34_DACStop(&ltr34)) != LTR_OK)
        fprintf(stderr, "Cannot stop DAC (%d)\n", ltr_result);

    if (mark_count)
        {
        printf("Time marks: %"PRIu64" in %.1f s\n",
            mark_count - 1, (last_mark_ti - first_mark_ti) / fs);
        }

    LTR34_Close(&ltr34);
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
