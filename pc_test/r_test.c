#include <windows.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

/*================================================================================================*/
#define TIMEOUT_MS      4000
#define BENCH_TIME      2000
#define DUMP_COUNT      0
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
HANDLE open_device(void)
    {
    char devname[64];
    HANDLE h = INVALID_HANDLE_VALUE;
    int slot;

    for (slot = 0; (INVALID_HANDLE_VALUE == h) && (slot < 127); slot++)
        {
        snprintf(devname, sizeof(devname)/sizeof(devname[0]) - 1, "\\\\.\\LDev%u", slot);
        devname[sizeof(devname)/sizeof(devname[0]) - 1] = 0;
        h = CreateFile(devname, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING,
            FILE_FLAG_OVERLAPPED, NULL);
        }
    return h;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void dump(const uint8_t* buf, size_t len)
    {
    if (len > DUMP_COUNT) len = DUMP_COUNT;
    while (len)
        {
        size_t line_len = (len > 16) ? 16 : len;
        len -= line_len;
        while (line_len--)
            {
            printf("%02X ", *buf++);
            }
        puts("");
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int main(void)
    {
    HANDLE hdev;
    OVERLAPPED ov_read[2];
    typedef DWORD data_t;
    data_t rbuf[2][65536];
    int ridx;
    uint64_t total_recv;
    uint32_t bench_recv;
    DWORD start_time;

    hdev = open_device();
    if (INVALID_HANDLE_VALUE == hdev) return puts("Cannot open device"), 1;

    ZeroMemory(&ov_read, sizeof(ov_read));
    ov_read[0].hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    ov_read[1].hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    total_recv = bench_recv = 0;
    start_time = GetTickCount();

    ReadFile(hdev, rbuf[0], sizeof(rbuf[0]), NULL, &ov_read[0]);
    ridx = 1;

    while (1)
        {
        DWORD nbytes, now, t;

        ReadFile(hdev, rbuf[ridx], sizeof(rbuf[ridx]), NULL, &ov_read[ridx]);
        ridx ^= 1;

        if (WaitForSingleObject(ov_read[ridx].hEvent, INFINITE) != WAIT_OBJECT_0)
            break;

        if (GetOverlappedResult(hdev, &ov_read[ridx], &nbytes, TRUE))
            {
            total_recv += nbytes;
            bench_recv += nbytes;
#if DUMP_COUNT > 0
            puts("");
            dump((const uint8_t*)rbuf[ridx], nbytes);
#endif
            }
        else
            {
            puts("read failed");
            }

        now = GetTickCount();
        t = now - start_time;
        if (t >= BENCH_TIME)
            {
            printf("\rRX: %"PRIu64" (%u b/s)",
                total_recv, (t) ? (unsigned int)(1000.0 * bench_recv / t) : 0);
            start_time = now;
            bench_recv = 0;
            }

        }

    CloseHandle(hdev);
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
