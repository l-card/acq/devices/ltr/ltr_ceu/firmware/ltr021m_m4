#include <windows.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <winioctl.h>

/*================================================================================================*/
#define DIOC_SEND_COMMAND   CTL_CODE(FILE_DEVICE_UNKNOWN, 15, METHOD_OUT_DIRECT, FILE_ANY_ACCESS)

enum en_usb_ioctl_vendor_request
    {
    USB_CTL_O_RESET_FPGA        = 0,
    USB_CTL_O_PUT_ARRAY         = 1,
    USB_CTL_I_GET_ARRAY         = 2,
    USB_CTL_O_INIT_FPGA         = 3,
    USB_CTL_O_INIT_DMA          = 4,
    USB_CTL_I_GET_MODULE_NAME   = 11,
    USB_CTL_O_CALL_APPLICATION  = 15
    };

typedef struct
    {
    WORD in, req, value, index;
    }
    ioctl_req_t;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
HANDLE open_device(void)
    {
    char devname[64];
    HANDLE h = INVALID_HANDLE_VALUE;
    int slot;

    for (slot = 0; (INVALID_HANDLE_VALUE == h) && (slot < 127); slot++)
        {
        snprintf(devname, sizeof(devname)/sizeof(devname[0]) - 1, "\\\\.\\LDev%u", slot);
        devname[sizeof(devname)/sizeof(devname[0]) - 1] = 0;
        h = CreateFile(devname, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING,
            FILE_FLAG_OVERLAPPED, NULL);
        }
    return h;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t do_ioctl_request(HANDLE hdev, ioctl_req_t* prq, void* buf, size_t len, DWORD timeout)
    {
    DWORD nbytes = 0;
    OVERLAPPED ov;
    ZeroMemory(&ov, sizeof(ov));
    ov.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (!DeviceIoControl(hdev, DIOC_SEND_COMMAND, prq, sizeof(*prq), buf, len, &nbytes, &ov))
        {
        if (ERROR_IO_PENDING != GetLastError()) return 0;
        if (WAIT_OBJECT_0 != WaitForSingleObject(ov.hEvent, timeout))
            CancelIo(hdev);
        if (!GetOverlappedResult(hdev, &ov, &nbytes, TRUE)) return 0;
        }
    return nbytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int get_module_name(HANDLE hdev, void* buf, size_t len)
    {
    ioctl_req_t rq =
        { .in = 1, .req = USB_CTL_I_GET_MODULE_NAME,
          .value = 0, .index = 0 };
    return (do_ioctl_request(hdev, &rq, buf, len, 4000) == len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int get_array(HANDLE hdev, DWORD addr, void* buf, size_t len, DWORD timeout)
    {
    ioctl_req_t rq =
        { .in = 1, .req = USB_CTL_I_GET_ARRAY,
          .value = (WORD)(addr & 0xFFFF), .index = (WORD)(addr >> 16) };
    return (do_ioctl_request(hdev, &rq, buf, len, timeout) == len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int put_array(HANDLE hdev, DWORD addr, const void* buf, size_t len, DWORD timeout)
    {
    ioctl_req_t rq =
        { .in = 0, .req = USB_CTL_O_PUT_ARRAY,
          .value = (WORD)(addr & 0xFFFF), .index = (WORD)(addr >> 16) };
    return (do_ioctl_request(hdev, &rq, (void*)buf, len, timeout) == len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int do_test(HANDLE hdev)
    {
    char name[16];
    uint32_t cmd = 0xCE117E57;
    uint32_t test_result;

    if (!get_module_name(hdev, name, sizeof(name)))
        {
        puts("getmodulename failed.");
        return 2;
        }

    printf("module name = %.16s\n", name);

    puts("Testing memory, please wait (up to 1 min)");
    if (!put_array(hdev, 0x90005000, &cmd, sizeof(cmd), 60000))
        {
        puts("putarray failed.");
        return 3;
        }

    if (!get_array(hdev, 0x90005008, &test_result, sizeof(test_result), 4000))
        {
        puts("getarray failed.");
        return 3;
        }

    if (!test_result)
        {
        puts("Test passed.");
        }
    else if (UINT32_MAX == test_result)
        {
        puts("Test could not start (Maybe LTR is in Ethernet mode?)");
        }
    else
        {
        printf("Test failed at 0x%08X\n", test_result);
        }
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int main(void)
    {
    HANDLE hdev;
    int ret_code;

    hdev = open_device();
    if (hdev != INVALID_HANDLE_VALUE)
        {
        ret_code = do_test(hdev);
        }
    else
        {
        puts("Cannot open device");
        return 1;
        }
    
    CloseHandle(hdev);
    return ret_code;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
