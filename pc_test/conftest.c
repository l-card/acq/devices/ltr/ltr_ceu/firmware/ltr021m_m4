#include <windows.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <winioctl.h>
#include <getopt.h>
#include <stdint.h>

/*================================================================================================*/
#define DIOC_SEND_COMMAND   CTL_CODE(FILE_DEVICE_UNKNOWN, 15, METHOD_OUT_DIRECT, FILE_ANY_ACCESS)

enum en_usb_ioctl_vendor_request
    {
    USB_CTL_O_RESET_FPGA        = 0,
    USB_CTL_O_PUT_ARRAY         = 1,
    USB_CTL_I_GET_ARRAY         = 2,
    USB_CTL_O_INIT_FPGA         = 3,
    USB_CTL_O_INIT_DMA          = 4,
    USB_CTL_I_GET_MODULE_NAME   = 11,
    USB_CTL_O_CALL_APPLICATION  = 15
    };

typedef struct
    {
    WORD in, req, value, index;
    }
    ioctl_req_t;
/*================================================================================================*/

/*================================================================================================*/
#define CONF_ADDR_FACT      0x90005000
#define CONF_ADDR_USER      0x90005200

#define CONF_SERIAL_SIZE        16
#define CONF_PASSWD_SIZE        16
#define CONF_MAC_SIZE           6
#define CONF_IP_SIZE            4

typedef struct
    {
    uint32_t signature;
    uint32_t size;
    char passwd[CONF_PASSWD_SIZE];
    char serial[CONF_SERIAL_SIZE];
    uint8_t mac_addr[CONF_MAC_SIZE];
    uint8_t reserved[2];
    }
    conf_fact_t;

typedef struct
    {
    uint32_t signature;
    uint32_t size;
    char passwd[CONF_PASSWD_SIZE];
    uint8_t ip_enable;                  /* ����� ����������: 1 = TCP/IP, 0 = USB */
    uint8_t mac_override;               /* 1 = ������������ user mac_addr, 0 = factory mac_addr */
    uint8_t mac_addr[CONF_MAC_SIZE];    /* ������������, ���� mac_override == 1 */
    uint8_t ip_addr[CONF_IP_SIZE];
    uint8_t ip_netmask[CONF_IP_SIZE];   /* ���� netmask == 0, �� ������������ DHCP */
    uint8_t ip_gateway[CONF_IP_SIZE];
    }
    conf_user_t;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
int open_device(HANDLE* ph)
    {
    char devname[64];
    int slot;

    if (!ph) return 0;
    *ph = INVALID_HANDLE_VALUE;

    for (slot = 0; (INVALID_HANDLE_VALUE == *ph) && (slot < 127); slot++)
        {
        snprintf(devname, sizeof(devname)/sizeof(devname[0]) - 1, "\\\\.\\LDev%u", slot);
        devname[sizeof(devname)/sizeof(devname[0]) - 1] = 0;
        *ph = CreateFile(devname, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING,
            FILE_FLAG_OVERLAPPED, NULL);
        }
    if (INVALID_HANDLE_VALUE == ph)
        {
        fputs("Error: cannot open device\n", stderr);
        return 0;
        }
    return 1;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
size_t do_ioctl_request(HANDLE hdev, ioctl_req_t* prq, void* buf, size_t len)
    {
    DWORD nbytes = 0;
    DWORD t = GetTickCount();
    if (!DeviceIoControl(hdev, DIOC_SEND_COMMAND, prq, sizeof(*prq), buf, len, &nbytes, NULL))
        return 0;
    t = GetTickCount() - t;
    if (t > 50)
        printf("Warning: IOCTL time = %lu ms\n", t);
    return nbytes;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int get_array(HANDLE hdev, DWORD addr, void* buf, size_t len)
    {
    ioctl_req_t rq =
        { .in = 1, .req = USB_CTL_I_GET_ARRAY,
          .value = (WORD)(addr & 0xFFFF), .index = (WORD)(addr >> 16) };
    return (do_ioctl_request(hdev, &rq, buf, len) == len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int put_array(HANDLE hdev, DWORD addr, const void* buf, size_t len)
    {
    ioctl_req_t rq =
        { .in = 0, .req = USB_CTL_O_PUT_ARRAY,
          .value = (WORD)(addr & 0xFFFF), .index = (WORD)(addr >> 16) };
    return (do_ioctl_request(hdev, &rq, (void*)buf, len) == len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
const char* dump(const uint8_t* buf, size_t len)
    {
    static char txt[4096];
    char* p = txt;
    if (len > (sizeof(txt) - 1) / 3) len = (sizeof(txt) - 1) / 3;
    while (len--)
        {
        snprintf(p, (size_t)(txt + sizeof(txt) - p), "%02X ", *buf++);
        p += 3;
        }
    return txt;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
const char* ip_str(const uint8_t* ip)
    {
    static char txt[32];
    snprintf(txt, sizeof(txt), "%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
    return txt;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int read_conf(HANDLE hdev, uint32_t addr, void* buf, size_t len)
    {
    return get_array(hdev, addr, buf, len);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int write_conf(HANDLE hdev, uint32_t addr, const void* buf, size_t len, const char* new_passwd)
    {
    int ret_val = 0;
    if (new_passwd)
        {
        char* tmp = malloc(len + CONF_PASSWD_SIZE);
        if (tmp)
            {
            memcpy(tmp, buf, len);
            strncpy(tmp + len, new_passwd, CONF_PASSWD_SIZE);
            ret_val = put_array(hdev, addr, tmp, len + CONF_PASSWD_SIZE);
            free(tmp);
            }
        }
    else
        {
        ret_val = put_array(hdev, addr, buf, len);
        }
    return ret_val;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
int main(int argc, char* argv[])
    {
    HANDLE hdev;
    conf_fact_t cf;
    conf_user_t cu;

    if (!open_device(&hdev))
        return 1;

#if 1
    puts("Read factory config");
    if (!read_conf(hdev, CONF_ADDR_FACT, &cf, sizeof(cf)))
        return fputs("IOCTL failed\n", stderr), 1;

    printf(".signature    = %08X\n", cf.signature);
    printf(".size         = %u (expected %u)\n", cf.size, sizeof(cf));
    printf(".passwd       = %s\n", dump((uint8_t*)cf.passwd, sizeof(cf.passwd)));
    printf(".serial       = %.16s\n", cf.serial);
    printf(".mac_addr     = %s\n", dump(cf.mac_addr, sizeof(cf.mac_addr)));
#endif

#if 1
    puts("Read user config");
    if (!read_conf(hdev, CONF_ADDR_USER, &cu, sizeof(cu)))
        return fputs("IOCTL failed\n", stderr), 1;

    printf(".signature    = %08X\n", cu.signature);
    printf(".size         = %u (expected %u)\n", cu.size, sizeof(cu));
    printf(".passwd       = %s\n", dump((uint8_t*)cu.passwd, sizeof(cf.passwd)));
    printf(".ip_enable    = %d\n", cu.ip_enable);
    printf(".mac_override = %d\n", cu.mac_override);
    printf(".mac_addr     = %s\n", dump(cu.mac_addr, sizeof(cu.mac_addr)));
    printf(".ip_addr      = %s\n", ip_str(cu.ip_addr));
    printf(".ip_netmask   = %s\n", ip_str(cu.ip_netmask));
    printf(".ip_gateway   = %s\n", ip_str(cu.ip_gateway));
#endif

#if 1
    printf("Write user config: %s\n",
        (write_conf(hdev, CONF_ADDR_USER, &cu, sizeof(cu), NULL)) ? "Ok" : "Failed");
#endif

#if 1
//    strncpy(cf.passwd, "secret", sizeof(cf.passwd));
    printf("Write factory config: %s\n",
        (write_conf(hdev, CONF_ADDR_FACT, &cf, sizeof(cf), NULL)) ? "Ok" : "Failed");
#endif

    CloseHandle(hdev);
    return 0;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
